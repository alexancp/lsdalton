#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > geoopt_constrain.info <<'%EOF%'
   geoopt_constrain
   ------------------
   Molecule:         C2H6
   Wave Function:    SCF / STO-3G
   Test Purpose:     Shows a constrained optimization of ethane, keeping the
                     C-C bond distance fixed (coordinate #1 as determined
                     from geoopt_constrain)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > geoopt_constrain.mol <<'%EOF%'
BASIS
STO-3G
Constrained geometry optimization of ethane

Atomtypes=2
Charge=6.0 Atoms=2
C     0.0000000000        0.0000000000        1.7547876282             *
C     0.0000000000        0.0000000000       -1.7547876282             *
Charge=1.0 Atoms=6
H     1.6808445513        0.9704360937        2.4445545504             *
H    -1.6808445513        0.9704360937        2.4445545504             *
H     0.0000000000       -1.9408721894        2.4445545504             *
H     1.6808445513       -0.9704360937       -2.4445545504             *
H    -1.6808445513       -0.9704360937       -2.4445545504             *
H     0.0000000000        1.9408721894       -2.4445545504             *
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > geoopt_constrain.dal <<'%EOF%'
**OPTIMIZE
.CONSTR
1
1
.NOHOPE
**WAVE FUNCTIONS
.HF
*DENSOPT
.CONVTHR
5.0d-6
*END OF INPUT
%EOF%
#######################################################################



#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >geoopt_constrain.check
cat >>geoopt_constrain.check <<'%EOF%'
#!/bin/ksh
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Geometry optimization
CRIT1=`$GREP "Default 1st order method will be used\: * BFGS update\." $log | wc -l`
CRIT2=`$GREP "Optimization will be performed in redundant internal coordinates\." $log | wc -l`
CRIT3=`$GREP "Model Hessian will be used as initial Hessian\." $log | wc -l`
CRIT4=`$GREP "Trust region method will be used to control step \(default\)\." $log | wc -l`
CRIT5=`$GREP "Constrained optimization has been requested\." $log | wc -l`
CRIT6=`$GREP "The following coordinate numbers will be held fixed during the optimization\:" $log | wc -l`
CRIT7=`$GREP "Coordinate \# * 1" $log | wc -l`
TEST[1]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7`
CTRL[1]=7
ERROR[1]="CONSTRAINED GEOMETRY OPTIMIZATION NOT SET UP CORRECTLY"

# Geometry
CRIT1=`$GREP "Total number of coordinates\: * 24" $log | wc -l`
CRIT2=`$GREP "^ * .[0-9] * C * x * \-*0*\.0000000000" $log | wc -l`
CRIT3=`$GREP "^ * .[0-9] * H * x * \-*1\.68084455" $log | wc -l`
CRIT4=`$GREP "^ * [0-9][0-9] * y * \-*0*\.0000000000" $log | wc -l`
CRIT5=`$GREP "^ * [0-9][0-9] * y * \-*0*\.97043609" $log | wc -l`
CRIT6=`$GREP "^ * [0-9][0-9] * y * \-*1\.94087218" $log | wc -l`
CRIT7=`$GREP "^ * [0-9][0-9] * z * \-*1\.75478762" $log | wc -l`
CRIT8=`$GREP "^ * [0-9][0-9] * z * \-*2\.44455455" $log | wc -l`
TEST[3]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7 \+ $CRIT8`
CTRL[3]=25
ERROR[3]="GEOMETRY NOT READ CORRECTLY"

# Initial energy
CRIT1=`$GREP "Final * HF energy\: * \-78\.2565905[67]" $log | wc -l`
TEST[4]=`expr	$CRIT1`
CTRL[4]=1
ERROR[4]="INITIAL ENERGY NOT CORRECT"

# Initial gradient
CRIT1=`$GREP -l " * C *  .0\.00000000[0-9][0-9] *   0\.00000002[0-9][0-9] *   0\.12144743[0-9][0-9]" $log | wc -l`
CRIT2=`$GREP -l " * C *  .0\.00000000[0-9][0-9] * \-0\.00000002[0-9][0-9] * \-0\.12144743[0-9][0-9]" $log | wc -l`
CRIT3=`$GREP -l " * H * \-0\.00150041[0-9][0-9] *   0\.00086626[0-9][0-9] *   0\.00477138[0-9][0-9]" $log | wc -l`
CRIT4=`$GREP -l " * H *   0\.00150041[0-9][0-9] *   0\.00086626[0-9][0-9] *   0\.00477138[0-9][0-9]" $log | wc -l`
CRIT5=`$GREP -l " * H * \-0\.00150041[0-9][0-9] * \-0\.00086626[0-9][0-9] * \-0\.00477138[0-9][0-9]" $log | wc -l`
CRIT6=`$GREP -l " * H *   0\.00150041[0-9][0-9] * \-0\.00086626[0-9][0-9] * \-0\.00477138[0-9][0-9]" $log | wc -l`
CRIT7=`$GREP -l " * H *  .0\.00000000[0-9][0-9] * \-0\.00173255[0-9][0-9] *   0\.00477138[0-9][0-9]" $log | wc -l`
CRIT8=`$GREP -l " * H *  .0\.00000000[0-9][0-9] *   0\.00173255[0-9][0-9] * \-0\.00477138[0-9][0-9]" $log | wc -l`
TEST[5]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
                $CRIT7 \+ $CRIT8`
CTRL[5]=8
ERROR[5]="INITIAL GRADIENT NOT CORRECT"

# Initial step
CRIT01=`$GREP -l  "^   1 * C * x *   0\.0000000[0-9][0-9][0-9]$" $log | wc -l`
CRIT02=`$GREP -l  "^   2 *   * y *  .0\.0000000[0-9][0-9][0-9]$" $log | wc -l`
CRIT03=`$GREP -l  "^   3 *   * z *   1\.7547876[0-9][0-9][0-9]$" $log | wc -l`
CRIT04=`$GREP -l  "^   4 * C * x *  .0\.0000000[0-9][0-9][0-9]$" $log | wc -l`
CRIT05=`$GREP -l  "^   5 *   * y *   0\.0000000[0-9][0-9][0-9]$" $log | wc -l`
CRIT06=`$GREP -l  "^   6 *   * z * \-1\.7547876[0-9][0-9][0-9]$" $log | wc -l`
CRIT07=`$GREP -l  "^   7 * H * x *   1\.6969200[0-9][0-9][0-9]$" $log | wc -l`
CRIT08=`$GREP -l  "^   8 *   * y *   0\.9797172[0-9][0-9][0-9]$" $log | wc -l`
CRIT09=`$GREP -l  "^   9 *   * z *   2\.3722573[0-9][0-9][0-9]$" $log | wc -l`
CRIT10=`$GREP -l  "^  10 * H * x * \-1\.6969200[0-9][0-9][0-9]$" $log | wc -l`
CRIT11=`$GREP -l  "^  11 *   * y *   0\.9797172[0-9][0-9][0-9]$" $log | wc -l`
CRIT12=`$GREP -l  "^  12 *   * z *   2\.3722573[0-9][0-9][0-9]$" $log | wc -l`
CRIT13=`$GREP -l  "^  13 * H * x *  .0\.0000000[0-9][0-9][0-9]$" $log | wc -l`
CRIT14=`$GREP -l  "^  14 *   * y * \-1\.9594345[0-9][0-9][0-9]$" $log | wc -l`
CRIT15=`$GREP -l  "^  15 *   * z *   2\.3722573[0-9][0-9][0-9]$" $log | wc -l`
CRIT16=`$GREP -l  "^  16 * H * x *   1\.6969200[0-9][0-9][0-9]$" $log | wc -l`
CRIT17=`$GREP -l  "^  17 *   * y * \-0\.9797172[0-9][0-9][0-9]$" $log | wc -l`
CRIT18=`$GREP -l  "^  18 *   * z * \-2\.3722573[0-9][0-9][0-9]$" $log | wc -l`
CRIT19=`$GREP -l  "^  19 * H * x * \-1\.6969200[0-9][0-9][0-9]$" $log | wc -l`
CRIT20=`$GREP -l  "^  20 *   * y * \-0\.9797172[0-9][0-9][0-9]$" $log | wc -l`
CRIT21=`$GREP -l  "^  21 *   * z * \-2\.3722573[0-9][0-9][0-9]$" $log | wc -l`
CRIT22=`$GREP -l  "^  22 * H * x *  .0\.0000000[0-9][0-9][0-9]$" $log | wc -l`
CRIT23=`$GREP -l  "^  23 *   * y *   1\.9594345[0-9][0-9][0-9]$" $log | wc -l`
CRIT24=`$GREP -l  "^  24 *   * z * \-2\.3722573[0-9][0-9][0-9]$" $log | wc -l`
TEST[6]=`expr	$CRIT01 \+ $CRIT02 \+ $CRIT03 \+ $CRIT04 \+ $CRIT05 \+ \
                $CRIT06 \+ $CRIT07 \+ $CRIT08 \+ $CRIT09 \+ $CRIT10 \+ \
                $CRIT11 \+ $CRIT12 \+ $CRIT13 \+ $CRIT14 \+ $CRIT15 \+ \
                $CRIT16 \+ $CRIT17 \+ $CRIT18 \+ $CRIT19 \+ $CRIT20 \+ \
                $CRIT21 \+ $CRIT22 \+ $CRIT23 \+ $CRIT24`
CTRL[6]=24
ERROR[6]="INITIAL STEP NOT CORRECT"

# Second iteration
CRIT1=`$GREP "Energy at this geometry is * \: * \-78\.25722[0-9]" $log | wc -l`
CRIT2=`$GREP "Norm of gradient * \: * (0| )\.18778[0-9]" $log | wc -l`
CRIT3=`$GREP "Norm of step * \: * (0| )\.00148[0-9]" $log | wc -l`
TEST[7]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[7]=3
ERROR[7]="SECOND ITERATION NOT CORRECT"

# Third iteration
CRIT1=`$GREP "Norm of gradient * \: * (0| )\.18805[0-9]" $log | wc -l`
CRIT2=`$GREP "Norm of step * \: * (0| )\.00001[6-9]" $log | wc -l`
TEST[8]=`expr	$CRIT1 \+ $CRIT2`
CTRL[8]=2
ERROR[8]="THIRD ITERATION NOT CORRECT"

# Final geometry
CRIT01=`$GREP -l  "^   1 * C * x *  .0\.0000000[0-9][0-9][0-9]$" $log |wc -l`
CRIT02=`$GREP -l  "^   2 *   * y *  .0\.0000000[0-9][0-9][0-9]$" $log |wc -l`
CRIT03=`$GREP -l  "^   3 *   * z *   1\.7547876[0-9][0-9][0-9]$" $log |wc -l`
CRIT04=`$GREP -l  "^   4 * C * x *   0\.0000000[0-9][0-9][0-9]$" $log |wc -l`
CRIT05=`$GREP -l  "^   5 *   * y *   0\.0000000[0-9][0-9][0-9]$" $log |wc -l`
CRIT06=`$GREP -l  "^   6 *   * z * \-1\.7547876[0-9][0-9][0-9]$" $log |wc -l`
CRIT07=`$GREP -l  "^   7 * H * x *   1\.6873556[0-9][0-9][0-9]$" $log |wc -l`
CRIT08=`$GREP -l  "^   8 *   * y *   0\.9741952[0-9][0-9][0-9]$" $log |wc -l`
CRIT09=`$GREP -l  "^   9 *   * z *   2\.3971499[0-9][0-9][0-9]$" $log |wc -l`
CRIT10=`$GREP -l  "^  10 * H * x * \-1\.6873556[0-9][0-9][0-9]$" $log |wc -l`
CRIT11=`$GREP -l  "^  11 *   * y *   0\.9741952[0-9][0-9][0-9]$" $log |wc -l`
CRIT12=`$GREP -l  "^  12 *   * z *   2\.3971499[0-9][0-9][0-9]$" $log |wc -l`
CRIT13=`$GREP -l  "^  13 * H * x *  .0\.0000000[0-9][0-9][0-9]$" $log |wc -l`
CRIT14=`$GREP -l  "^  14 *   * y * \-1\.9483904[0-9][0-9][0-9]$" $log |wc -l`
CRIT15=`$GREP -l  "^  15 *   * z *   2\.3971499[0-9][0-9][0-9]$" $log |wc -l`
CRIT16=`$GREP -l  "^  16 * H * x *   1\.6873556[0-9][0-9][0-9]$" $log |wc -l`
CRIT17=`$GREP -l  "^  17 *   * y * \-0\.9741952[0-9][0-9][0-9]$" $log |wc -l`
CRIT18=`$GREP -l  "^  18 *   * z * \-2\.3971499[0-9][0-9][0-9]$" $log |wc -l`
CRIT19=`$GREP -l  "^  19 * H * x * \-1\.6873556[0-9][0-9][0-9]$" $log |wc -l`
CRIT20=`$GREP -l  "^  20 *   * y * \-0\.9741952[0-9][0-9][0-9]$" $log |wc -l`
CRIT21=`$GREP -l  "^  21 *   * z * \-2\.3971499[0-9][0-9][0-9]$" $log |wc -l`
CRIT22=`$GREP -l  "^  22 * H * x *   0\.0000000[0-9][0-9][0-9]$" $log |wc -l`
CRIT23=`$GREP -l  "^  23 *   * y *   1\.9483904[0-9][0-9][0-9]$" $log |wc -l`
CRIT24=`$GREP -l  "^  24 *   * z * \-2\.3971499[0-9][0-9][0-9]$" $log |wc -l`
TEST[9]=`expr	$CRIT01 \+ $CRIT02 \+ $CRIT03 \+ $CRIT04 \+ $CRIT05 \+ \
                $CRIT06 \+ $CRIT07 \+ $CRIT08 \+ $CRIT09 \+ $CRIT10 \+ \
                $CRIT11 \+ $CRIT12 \+ $CRIT13 \+ $CRIT14 \+ $CRIT15 \+ \
                $CRIT16 \+ $CRIT17 \+ $CRIT18 \+ $CRIT19 \+ $CRIT20 \+ \
                $CRIT21 \+ $CRIT22 \+ $CRIT23 \+ $CRIT24`
CTRL[9]=24
ERROR[9]="FINAL GEOMETRY NOT CORRECT"

# Geometry convergence
CRIT1=`$GREP "Constrained optimization converged in * 4( |  )iterations\!" $log | wc -l`
CRIT2=`$GREP "Energy at final geometry is * \: * \-78\.25722[0-9] a\.u\." $log | wc -l`
CRIT3=`$GREP "Energy change during optimization \: * (\-0|\-)\.00063[0-9] a\.u\." $log | wc -l`
TEST[10]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[10]=3
ERROR[10]="GEOMETRY OPTIMIZATION NOT CONVERGED"

PASSED=1
for i in 1 3 4 5 6 7 8 9 10
do
   if [ ${TEST[i]} -lt ${CTRL[i]} ]; then
     echo "${ERROR[i]} : ${TEST[i]} .ne. ${CTRL[i]} ; "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
