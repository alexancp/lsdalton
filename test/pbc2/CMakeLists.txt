add_lsdalton_runtest_v1(NAME pbc2_h2fock      LABELS "pbc")
add_lsdalton_runtest_v1(NAME pbc2_h2SCF_iter1 LABELS "pbc")
add_lsdalton_runtest_v1(NAME pbc2_h2STO6G     LABELS "pbc")
add_lsdalton_runtest_v1(NAME pbc2_h2mol       LABELS "pbc")
