#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > decmp2_StressTest.info <<'%EOF%'
   DEC-MP2 energies for full DEC
   -----------------------------
   Molecule:         HCOOH
   Wave Function:    MP2 / 6-31G
   Test Purpose:     Test StressTest Keyword (Thomas K)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > decmp2_StressTest.mol <<'%EOF%'
BASIS
3-21G


Atomtypes=3 Nosymmetry Angstrom
Charge=6.0 Atoms=1
C          0.41977       -0.05971        0.16752
Charge=1.0 Atoms=2
H          0.88996       -0.11071        1.15515
H         -1.30534        0.18370       -0.62443
Charge=8.0 Atoms=2
O          0.90317       -0.15439       -0.93920
O         -0.90756        0.14113        0.24096
%EOF%
#######################################################################
#  DALTON INPUT
#######################################################################
cat > decmp2_StressTest.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH DAVID
.START
TRILEVEL
.CONVDYN
SLOPPY
.LCM
**INFO
.DEBUG_MPI_MEM
**LOCALIZE ORBITALS
.PIPEKMEZEY
.NO LEVEL2 LOCALIZATION
**DEC
.STRESSTEST
.MP2
.FOT
1.0e-6
.FROZENCORE
.MEMORY
2.0
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >decmp2_StressTest.check
cat >>decmp2_StressTest.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# HF ENERGY 
CRIT1=`$GREP "Final HF energy: * \-187\.3420346" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Total correlation energy
CRIT1=`$GREP "Correlation energy  \: * \-0.2965" $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="Total correlation energy is not correct"

# Memory test for total memory                                             
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`         
TEST[3]=`expr  $CRIT1`                                                    
CTRL[3]=1                                                                 
ERROR[3]="Memory leak -"
                                                                          
# Memory test for array4                                                  
CRIT1=`$GREP "Memory in use for array4 * \: * 0.000 * GB" $log | wc -l`   
TEST[4]=`expr  $CRIT1`                                                    
CTRL[4]=1                                                                 
ERROR[4]="Memory leak for array4 -" 

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=0
ERROR[5]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%




