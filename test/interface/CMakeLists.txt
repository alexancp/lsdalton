# Conditionally available tests
if(ENABLE_RSP)
  add_lsdalton_runtest_v1(NAME chemshell_energy_gradient LABELS "chemshell")
endif()
