#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > IchorThermiteTesting6.info <<'%EOF%'
   IchorThermiteTesting6
   -------------
   Test Purpose:     Test Ichor Screening Integrals agains Thermite
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > IchorThermiteTesting6.mol <<'%EOF%'
BASIS
3-21G
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   1     0         *
        2.    1
He     0.00000   0.00000   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > IchorThermiteTesting6.dal <<'%EOF%'
**GENERAL
.TIME
.NOGCBASIS
**INTEGRALS
.DEBUGICHOR
6
**WAVE FUNCTIONS
.HF
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >IchorThermiteTesting6.check
cat >> IchorThermiteTesting6.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Integral test

CRIT1=`$GREP "Ichor Integrals tested against Thermite\: SUCCESSFUL" $log | wc -l`
TEST[ 1]=`expr  $CRIT1`
CTRL[ 1]=1
ERROR[ 1]="NOT ALL ICHOR INTEGRALS CORRECT -"

PASSED=1
for((i=1;i<=1;i++))
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then

      if [ $PASSED -eq 1 ]
      then
         echo -e ${ERROR[i]} > newfile.txt
      else
         echo -e ${ERROR[i]} >> newfile.txt
      fi
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   cat newfile.txt -n
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
