#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_fmm_df_gradient.info <<'%EOF%'
   LSDALTON_fmm_df_gradient
   ------------
   Molecule:         Four H2O molecules (two clusters separated by long dist)
   Model       :     DFT (LDA) / 6-31G / df-def2
   Test Purpose:     Check the FMM gradients with density fitting
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_fmm_df_gradient.mol <<'%EOF%'
BASIS
6-31G Aux=df-def2
Two + two nearby H2O
---------
    2              C
        1.    8
H                  8.675000    0.000000  -16.640000
H                  9.579936    0.000000  -17.920455
H                  7.203213    0.037757  -17.673336
H                  8.108149    0.037757  -18.953791
H                  9.241330    0.571501   -2.493601
H                 10.146266    0.571501   -3.774055
H                  7.134485    0.659897   -2.982496
H                  8.039421    0.659897   -4.262951
        8.    4
O                  8.675000    0.000000  -17.600000
O                  7.203213    0.037757  -18.633336
O                  9.241330    0.571501   -3.453601
O                  7.134485    0.659897   -3.942496
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_fmm_df_gradient.dal <<'%EOF%'
!**OPTIMI
**INTEGRAL
.DENSFIT
.RUNMM
*FMM
!.NOONE
!.NOMMBU
.SCREEN
 1.e-10
.LMAX
 10
.TLMAX
 24
**WAVE FUNCTIONS
.DFT
 LDA
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
**RESPONS
*MOLGRA
*END OF INPUT
%EOF%
#######################################################################

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_fmm_df_gradient.check
cat >>LSDALTON_fmm_df_gradient.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-293\.95568980[1-2]..." $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Nuclear repulsion:
CRIT1=`$GREP "Nuclear repulsion: *198\.01334251935." $log | wc -l`
TEST[3]=$CRIT1
CTRL[3]=1
ERROR[3]="NUCLEAR REPULSION NOT CORRECT"

# Electronic energy:
CRIT1=`$GREP "Electronic energy: *\-491\.96903232[0-2]..." $log | wc -l`
TEST[4]=$CRIT1
CTRL[4]=1
ERROR[4]="ELECTRONIC ENERGY NOT CORRECT"


# LDA:
CRIT1=`$GREP "This is a DFT calculation of type: *LDA" $log | wc -l`
TEST[5]=$CRIT1
CTRL[5]=1
ERROR[5]="NOT AN LDA CALCULATION"

# GRADIENT:
CRIT1=`$GREP "H     *\-0\.0516079...   *0\.0034653...     *\-3\.1716048..."  $log | wc -l`
CRIT2=`$GREP "H     *\-3\.0060028...   *0\.0034280...     *1\.0123044..."    $log | wc -l`
CRIT3=`$GREP "H     *1\.0415082...     *\-0\.0297016...   *\-3\.1198378..."  $log | wc -l`
CRIT4=`$GREP "H     *\-2\.5876041...   *\-0\.0300709...   *2\.0310254..."    $log | wc -l`
CRIT5=`$GREP "H     *\-0\.0165407...   *0\.0052838...     *\-3\.1314987..."  $log | wc -l`
CRIT6=`$GREP "H     *\-2\.9545372...   *0\.007219[6-7]... *1\.0332036..."    $log | wc -l`
CRIT7=`$GREP "H     *0\.2059919...     *\-0\.0072536...    *\-3\.0379220..." $log | wc -l`
CRIT8=`$GREP "H     *\-2\.1481159...   *\-0\.0500674...    *1\.5236386..."   $log | wc -l`
CRIT9=`$GREP "O     *0\.6496013...     *0\.071279[8-9]...  *0\.4611865..."   $log | wc -l`
CRIT10=`$GREP "O    *3\.9543509...     *\-0\.0183967...    *2\.7871426..."   $log | wc -l`
CRIT11=`$GREP "O    *0\.5849334...     *0\.1060216...      *1\.3089639..."   $log | wc -l`
CRIT12=`$GREP "O    *4\.3286683...     *\-0\.0612080...    *2\.3038208..."   $log | wc -l`
TEST[6]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12`
CTRL[6]=12
ERROR[6]="MOLECULAR GRADIENT NOT CORRECT"

PASSED=1
for i in 1 2 3 4 5 6 
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
