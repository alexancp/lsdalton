#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_magderiv2.info <<'%EOF%'
   LSDALTON_magderiv2
   -------------
   Molecule:         water/6-31G**
   Wave Function:    HF
   Test Purpose:     Check magnetic derivative 2e int integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_magderiv2.mol <<'%EOF%'
ATOMBASIS
Water
Output is in Bohr
Atomtypes=2    Charge=0   Nosymmetry
Charge=8.00   Atoms=1     Basis=6-31G**
O     0.0000000000000000   -0.2249058930  0.00000000
Charge=1.00   Atoms=2     Basis=6-31G**
H     1.45235              0.899623      0.00000000 
H    -1.45235              0.899623      0.00000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_magderiv2.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRAL
.DEBUGMAGDERIV
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.RH
.DIIS
.START
H1DIAG
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_magderiv2.check
cat >> LSDALTON_magderiv2.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
# the others should be zero
CRIT1=`$GREP "QQQ FxFx magderiv * \-190.99886" $log | wc -l`
CRIT2=`$GREP "QQQ FyFy magderiv * \-337.60403" $log | wc -l`
CRIT3=`$GREP "QQQ FzFz magderiv * \-677.61188" $log | wc -l`
TEST[1]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=3
ERROR[1]="MAGDERIV 2INT TEST1 NOT CORRECT -"

CRIT1=`$GREP "QQQ KxKx magderiv * \-0.027714" $log | wc -l`
CRIT2=`$GREP "QQQ KyKy magderiv * \-0.046992" $log | wc -l`
CRIT3=`$GREP "QQQ KzKz magderiv * \-0.101169" $log | wc -l`
TEST[2]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[2]=3
ERROR[2]="MAGDERIV 2INT TEST2 NOT CORRECT -"

CRIT1=`$GREP "QQQ JxJx magderiv * \-194.31260" $log | wc -l`
CRIT2=`$GREP "QQQ JyJy magderiv * \-343.39214" $log | wc -l`
CRIT3=`$GREP "QQQ JzJz magderiv * \-688.89640" $log | wc -l`
TEST[3]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[3]=3
ERROR[3]="MAGDERIV 2INT TEST3 NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=0
ERROR[5]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
