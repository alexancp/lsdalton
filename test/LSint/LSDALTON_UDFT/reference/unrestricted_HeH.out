  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2014.0
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Sonia Coriani,           University of Trieste,     Italy     (response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Trygve Helgaker,         University of Oslo,        Norway    (supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (orbital localization, SCF optimization)
      Robert Izsak,            University of Oslo,        Norway    (ADMM)
      Branislav Jansik,        Aarhus University,         Denmark   (trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (response, integrals)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (ADMM)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (supervision)
      Simen Reine,             University of Oslo,        Norway    (integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 Who compiled             | simensr
 Host                     | 1x-193-157-199-133.uio.no
 System                   | Darwin-14.0.0
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /usr/local/bin/gfortran
 Fortran compiler version | GNU Fortran (GCC) 5.0.0 20141012 (experimental)
 C compiler               | /usr/local/bin/gcc
 C compiler version       | gcc (GCC) 5.0.0 20141012 (experimental)
 C++ compiler             | /usr/local/bin/g++
 C++ compiler version     | g++ (GCC) 5.0.0 20141012 (experimental)
 BLAS                     | /usr/lib/libblas.dylib
 LAPACK                   | /usr/lib/liblapack.dylib
 Static linking           | OFF
 Last Git revision        | 6912f763cad369377e4f1999ea67e2a2d27473fe
 Git branch               | XCfun-dev
 Configuration time       | 2014-12-09 17:18:56.079323
 The Functional chosen is a GGA type functional
 The Functional chosen contains an exact exchange contribution
 with the weight:  0.20000000000000001     

Start simulation
     Date and time (Linux)  : Tue Dec  9 17:22:56 2014
     Host name              : 1x-193-157-199-133.uio.no               
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  6-31G                                   
  HeH, CCSD( T)/cc-pVQZ opt. geometry     
  Test open shell response                
  Atomtypes=2 Charge=0 Nosymmetry Angstrom                    
  Charge=1.0 Atoms=1                                          
  H     0.0000000000   0.0000000000   0.0000000000            
  Charge=2.0 Atoms=1                                          
  He    0.0000000000   0.0000000000   3.9218840000            
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **WAVE FUNCTIONS                                                                                    
  .DFT                                                                                                
  B3LYP                                                                                               
  *DFT INPUT                                                                                          
  .FINE                                                                                               
  .GRID TYPE                                                                                          
   BECKEORIG LMG                                                                                      
  *DENSOPT                                                                                            
  .RH                                                                                                 
  .DIIS                                                                                               
  **INFO                                                                                              
  .DEBUG_MPI_MEM                                                                                      
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   He       x      0.0000000000
   5            y      0.0000000000
   6            z      7.4112866890
 
WARNING:  No bonds - no atom pairs are within normal bonding distances
WARNING:  maybe coordinates were in Bohr, but program were told they were in Angstrom ?
 
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  6-31G                                             
  CHARGES:    1.0000    2.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE/Users/simensr/Dalton/2014/xcbuild-omp/basis/6-31G
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :        4
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :        8
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   1.000  6-31G           F              4           2
     2   2.000  6-31G           F              4           2
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  H           1        0.00000000        0.00000000        0.00000000
     2  He          1        0.00000000        0.00000000        7.41128669
                      
                      
Atoms and basis sets
  Total number of atoms        :      2
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 H      1.000 6-31G                      4        2 [4s|2s]                                      
      2 He     2.000 6-31G                      4        2 [4s|2s]                                      
---------------------------------------------------------------------
total          3                                8        4
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is an OpenMP calculation using   8 threads.
    This is a serial calculation (no MPI)
 The Functional chosen is a GGA type functional
 The Functional chosen contains a exact exchange contribution
 with the weight:  0.20000000000000001     
   
  The Exchange-Correlation Grid specifications:
  Radial Quadrature : LMG scheme
                      Radial grid as proposed by R. Lindh, P.A. Malmqvist
                      and L. Gagliardi. Theor. Chem. Acc. (2001) 106, 178
  Space partitioning: Becke partitioning scheme without atomic size correction
                      J. Chem. Phys. (1988) vol 88 page 2547

  We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.2154D-16
     DFT LSint integration order range     :   [  5:  47]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.2000D+00

Density subspace min. method    : DIIS                    
Density optimization : Diagonalization                    

 Maximum size of Fock/density queue in averaging:           7

 WARNING WARNING WARNING spin check commented out!!! /Stinne


 --------------------------
 <Unrestricted calculation>
 --------------------------
 ALPHA spin occupancy =     1
 BETA  spin occupancy =     2

 
Matrix type: mtype_unres_dense

Convergence threshold for gradient:   0.10E-03
 
We perform the calculation in the Grand Canonical basis
(see PCCP 2009, 11, 5805-5813)
To use the standard input basis use .NOGCBASIS
 
Since the input basis set is a segmented contracted basis we
perform the integral evaluation in the more efficient
standard input basis and then transform to the Grand 
Canonical basis, which is general contracted.
You can force the integral evaluation in Grand 
Canonical basis by using the keyword
.NOGCINTEGRALTRANSFORM
 
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
 The SCF Convergence Criteria is applied to the gradnorm in AO basis

 End of configuration!

 
Matrix type: mtype_dense
 
 A set of atomic calculations are performed in order to construct
 the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
 as well as JCTC 5, 1027 (2009)
 This is done in order to use the TRILEVEL starting guess and 
 perform orbital localization
 This is Level 1 of the TRILEVEL starting guess and is performed per default.
 The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
 under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
 or orbital localization.
 

 Level 1 atomic calculation on 6-31G Charge   1
 ================================================
 The Coulomb energy contribution   0.31435582677568180     
 The Exchange energy contribution    3.1435582677568179E-002
 The Fock energy contribution  -0.28292024409811367     
    This is an OpenMP Gridgeneration calculation using  8 threads.
Total Number of grid points:       31176

  Max allocated memory, Grid                    2.183 MB

  KS electrons/energy:    1.00000000000000   -0.22355801482455 rel.err:-0.38E-14
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1     -0.4388706815    0.00000000000    0.00      0.00000    0.00    0.0000000    8.35E-02  ###
 The Coulomb energy contribution   0.29994973971303562     
 The Exchange energy contribution    2.9994973971303567E-002
 The Fock energy contribution  -0.26995476574173205     
  KS electrons/energy:    1.00000000000000   -0.21356084698134 rel.err:-0.44E-15
  2     -0.4403059211   -0.00143523968    0.00      0.00000    0.00    0.0000000    7.00E-03  ###
 The Coulomb energy contribution   0.30102731304210523     
 The Exchange energy contribution    3.0102731304210519E-002
 The Fock energy contribution  -0.27092458173789469     
  KS electrons/energy:    1.00000000000000   -0.21431327793558 rel.err: 0.29E-14
  3     -0.4403161201   -0.00001019896   -1.00      0.00000    0.00    0.0000000    3.23E-05  ###

 Level 1 atomic calculation on 6-31G Charge   2
 ================================================
 The Coulomb energy contribution    2.4906574961730761     
 The Exchange energy contribution   0.24906574961730762     
 The Fock energy contribution   -2.2415917465557684     
    This is an OpenMP Gridgeneration calculation using  8 threads.
Total Number of grid points:       28836

  Max allocated memory, Grid                    2.041 MB

  KS electrons/energy:    2.00000000000000   -1.03642401460349 rel.err: 0.22E-15
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1     -2.7820678196    0.00000000000    0.00      0.00000    0.00    0.0000000    1.53E+00  ###
 The Coulomb energy contribution    1.9473813524160368     
 The Exchange energy contribution   0.19473813524160369     
 The Fock energy contribution   -1.7526432171744331     
  KS electrons/energy:    1.99999999999999   -0.82343713215126 rel.err:-0.28E-14
  2     -2.8917473556   -0.10967953602    0.00      0.00000    0.00    0.0000000    4.17E-01  ###
 The Coulomb energy contribution    2.0526198104551612     
 The Exchange energy contribution   0.20526198104551613     
 The Fock energy contribution   -1.8473578294096451     
  KS electrons/energy:    1.99999999999999   -0.86580817622356 rel.err:-0.48E-14
  3     -2.8999197256   -0.00817236997   -1.00      0.00000    0.00    0.0000000    3.67E-03  ###
 The Coulomb energy contribution    2.0526198104551612     
 The Exchange energy contribution   0.20526198104551613     
 The Fock energy contribution   -1.8473578294096451     
  KS electrons/energy:    1.99999999999999   -0.86580817622356 rel.err:-0.44E-14
  4     -2.8999197256    0.00000000000   -1.00      0.00000    0.00    0.0000000    3.67E-03  ###
 The Coulomb energy contribution    2.0535747036565342     
 The Exchange energy contribution   0.20535747036565338     
 The Fock energy contribution   -1.8482172332908806     
  KS electrons/energy:    1.99999999999999   -0.86618985192749 rel.err:-0.49E-14
  5     -2.8999203583   -0.00000063268   -1.00      0.00000    0.00    0.0000000    1.28E-06  ###
 
Matrix type: mtype_unres_dense

 First density: Atoms in molecule guess

 The Coulomb energy contribution    2.6244606292373969     
 The Exchange energy contribution  -0.47092068834769357     
 The Fock energy contribution   -4.7780005701271007     
    This is an OpenMP Gridgeneration calculation using  8 threads.
Total Number of grid points:       60006

  Max allocated memory, Grid                    4.044 MB

  KS electrons/energy:    2.99999719461620   -1.08048395059637 rel.err:-0.94E-06
  Iteration 0 energy:  -3.3402175029335597     
 FALLBACK: open shell currently works only with cholesky decomposition
 - Ask Dr. Jansik to fix it for Lowdin!
 Preparing to do cholesky decomposition...
  Relative convergence threshold for solver:   1.0000000000000000E-002
  SCF Convergence criteria for gradient norm:   9.9999997473787516E-005
** Get Fock matrix number   1
 The Coulomb energy contribution    2.6244669663478071     
 The Exchange energy contribution  -0.53112800679839756     
 The Fock energy contribution   -4.7178059258972160     
  KS electrons/energy:    2.99999719486609   -1.10539976698970 rel.err:-0.94E-06
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1     -3.3952333944    0.00000000000    0.00      0.00000    0.00    0.0000000    2.21E-01  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.41 seconds
 >>> wall Time used in SCF iteration is   0.07 seconds
** Get Fock matrix number   2
 The Coulomb energy contribution    2.6382030338336619     
 The Exchange energy contribution  -0.53662244090052202     
 The Fock energy contribution   -4.7397836267668012     
  KS electrons/energy:    2.99999747599625   -1.11662223027856 rel.err:-0.84E-06
  2     -3.3967696403   -0.00153624596    0.00      0.00000    0.00    0.0000000    7.76E-03  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.41 seconds
 >>> wall Time used in SCF iteration is   0.07 seconds
** Get Fock matrix number   3
 The Coulomb energy contribution    2.6387187215817258     
 The Exchange energy contribution  -0.53682895924305063     
 The Fock energy contribution   -4.7406084839204015     
  KS electrons/energy:    2.99999748601762   -1.11704194355491 rel.err:-0.84E-06
  3     -3.3967715309   -0.00000189063   -1.00      0.00000    0.00    0.0000000    3.82E-05  ###
SCF converged in      3 iterations
 >>>  CPU Time used in **ITER is   1.23 seconds
 >>> wall Time used in **ITER is   0.20 seconds

Total no. of matmuls in SCF optimization:         74

 Number of occupied alpha orbitals:           1
 Number of occupied beta orbitals:           2

 Number of virtual alpha orbitals:           3
 Number of virtual beta orbitals:           2

 Number of occupied alpha orbital energies to be found:           1
 Number of occupied beta orbital energies to be found:           1

 Number of virtual alpha orbital energies to be found:           1
 Number of virtual beta orbital energies to be found:           1


Calculation of alpha occupied orbital energies converged in     1 iterations!

Calculation of beta occupied orbital energies converged in     2 iterations!

Calculation of alpha virtual orbital energies converged in     3 iterations!

Calculation of beta virtual orbital energies converged in     2 iterations!

    E(LUMO), alpha:                         -0.015092 au
   -E(HOMO), alpha:                         -0.646215 au
   -------------------------------------
    "Alpha" HOMO-LUMO Gap (iteratively):     0.631123 au


    E(LUMO), beta:                           0.714266 au
   -E(HOMO), beta:                          -0.312620 au
   -------------------------------------
    "Beta" HOMO-LUMO Gap (iteratively):      1.026886 au


    E(LUMO):                                -0.015092 au
   -E(HOMO):                                -0.312620 au
   -------------------------------------
    "Overall" HOMO-LUMO Gap (iteratively):   0.297528 au


********************************************************
 it       dE(HF)          exit   RHshift    RHinfo 
********************************************************
  1    0.00000000000    0.0000    0.0000    0.0000000
  2   -0.00153624596    0.0000    0.0000    0.0000000
  3   -0.00000189063   -1.0000    0.0000    0.0000000

======================================================================
                   LINSCF ITERATIONS:
  It.nr.               Energy                 AO Gradient norm
======================================================================
    1            -3.39523339435480941972      0.220583602522034D+00
    2            -3.39676964031163830526      0.776099967986250D-02
    3            -3.39677153094109218756      0.382062484750357D-04

      SCF converged !!!! 
         >>> Final results from LSDALTON <<<


      Final DFT energy:                       -3.396771530941
      Nuclear repulsion:                       0.269858674198
      Electronic energy:                      -3.666630205140

 >>>  CPU Time used in *SCF is   1.59 seconds
 >>> wall Time used in *SCF is   0.56 seconds

Total no. of matmuls used:                        82
Total no. of Fock/KS matrix evaluations:           4
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                      0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                       0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):          0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMITEM):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECORBITAL):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECFRAG):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECAOBATCHINFO):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (PNOSpaceInfo):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lvec_data)):           0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lattice_cell)):        0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                         68.762 MB
  Max allocated memory, type(matrix)                   6.000 kB
  Max allocated memory, real(realk)                    2.240 MB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                      447.394 kB
  Max allocated memory, logical                      201.092 kB
  Max allocated memory, character                      2.448 kB
  Max allocated memory, AOBATCH                        4.032 kB
  Max allocated memory, DECORBITAL                     0.000 Byte
  Max allocated memory, DECFRAG                        0.000 Byte
  Max allocated memory, BATCHTOORB                     0.000 Byte
  Max allocated memory, DECAOBATCHINFO                 0.000 Byte
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                         66.400 MB
  Max allocated memory, PNOSpaceInfo                   0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        1.584 kB
  Max allocated memory, LSAOTENSOR                     2.432 kB
  Max allocated memory, SLSAOTENSOR                    2.576 kB
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                 153.056 kB
  Max allocated memory, ATOMITEM                       1.536 kB
  Max allocated memory, LSMATRIX                       0.784 kB
  Max allocated memory, OverlapT                     327.168 kB
  Max allocated memory, linkshell                      0.400 kB
  Max allocated memory, integrand                    516.096 kB
  Max allocated memory, integralitem                 368.640 kB
  Max allocated memory, IntWork                       83.136 kB
  Max allocated memory, Overlap                      597.184 kB
  Max allocated memory, ODitem                         1.152 kB
  Max allocated memory, LStensor                       2.496 kB
  Max allocated memory, FMM                            0.000 Byte
  Max allocated memory, XC                             0.000 Byte
  Max allocated memory, Lvec_data                      0.000 Byte
  Max allocated memory, Lattice_cell                   0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

  Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
  This is a non MPI calculation so naturally no memory is allocated on slaves!
 >>>  CPU Time used in LSDALTON is   3.19 seconds
 >>> wall Time used in LSDALTON is   0.88 seconds

End simulation
     Date and time (Linux)  : Tue Dec  9 17:22:57 2014
     Host name              : 1x-193-157-199-133.uio.no               
