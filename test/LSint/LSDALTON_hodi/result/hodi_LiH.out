     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton v2018.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,                    Norway   (Geometry optimizer)
    Ashleigh Barnes,        Oak Ridge National Laboratory,         USA      (ML-DEC)
    Radovan Bast,           UiT The Arctic University of Norway,   Norway   (CMake, Testing)
    Pablo Baudin,           Aarhus University,                     Denmark  (DEC,CCSD)
    Dmytro Bykov,           Oak Ridge National Laboratory,         USA      (ML-DEC)
    Sonia Coriani,          Technical University of Denmark,       Denmark  (Response)
    Roberto Di Remigio,     UiT The Arctic University of Norway,   Norway   (PCM)
    Karen Dundas,           UiT The Arctic University of Norway,   Norway   (OpenRSP)
    Patrick Ettenhuber,     Aarhus University,                     Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,                     Denmark  (CCSD(T), DEC)
    Luca Frediani,          UiT The Arctic University of Norway,   Norway   (PCM, Supervision)
    Daniel Henrik Friese,   Heinrich-Heine-Universitat Dusseldorf, Germany  (OpenRSP)
    Bin Gao,                UiT The Arctic University of Norway,   Norway   (OpenRSP, QcMatrix)
    Trygve Helgaker,        University of Oslo,                    Norway   (Supervision)
    Stinne Hoest,           Aarhus University,                     Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,                     Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,                    Norway   (ADMM)
    Branislav Jansik,       Aarhus University,                     Denmark  (Trilevel, orbital localization)
    Frank Jensen,           Aarhus University,                     Denmark  (ADMM basis sets)
    Poul Joergensen,        Aarhus University,                     Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,                     Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,                     Denmark  (RSP, INT, DEC, SCF, Input, MPI, 
                                                                             MAT)
    Andreas Krapp,          University of Oslo,                    Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,                     Denmark  (Response, DEC)
    Chandan Kumar,          University of Oslo,                    Norway   (Nuclei-selected NMR shielding)
    Patrick Merlot,         University of Oslo,                    Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,                     Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,                     Denmark  (Supervision)
    Elisa Rebolini,         University of Oslo,                    Norway   (MO-PARI-K)
    Simen Reine,            University of Oslo,                    Norway   (Integrals and approximations, 
                                                                             XC, MPI, OpenMP, geo.opt.)
    Magnus Ringholm,        UiT The Arctic University of Norway,   Norway   (OpenRSP)
    Kenneth Ruud,           UiT The Arctic University of Norway,   Norway   (OpenRSP, Supervision)
    Vladimir Rybkin,        University of Oslo,                    Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,                         Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham               England  (E-coefficients)
    Erik Tellgren,          University of Oslo,                    Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,                 Norway   (Response)
    Lea Thoegersen,         Aarhus University,                     Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,                    Norway   (FMM)
    Lukas Wirz,             University of Oslo,                    Norway   (NR-PARI)
    Marcin Ziolkowski,      Aarhus University,                     Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | simensr
     Host                     | Simens-MBP
     System                   | Darwin-17.7.0
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/local/bin/gfortran
     Fortran compiler version | GNU Fortran (Homebrew GCC 8.2.0) 8.2.0
     C compiler               | /usr/local/bin/gcc
     C compiler version       | gcc (Homebrew GCC 8.2.0) 8.2.0
     C++ compiler             | /usr/local/bin/g++
     C++ compiler version     | g++ (Homebrew GCC 8.2.0) 8.2.0
     BLAS                     | /usr/lib/libblas.dylib
     LAPACK                   | /usr/lib/liblapack.dylib
     Static linking           | OFF
     Last Git revision        | 2f51cdc3dabaf51c276dcd14446c358ed6ea009f
     Git branch               | release/2018
     Configuration time       | 2019-08-01 14:19:29.664831
  

         Start simulation
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    3-21G                                   
    LiH                                     
    -                                       
    Atomtypes=2 Nosymmetry Angstrom                                                                                         
    Charge=3.0 Atoms=1                                                                                                      
    Li  0.257  -0.363   0.000                                                                                               
    Charge=1.0 Atoms=1                                                                                                      
    H   0.257   0.727   0.000                                                                                               
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .NOGCBASIS
    .TESTHODI
    .TIME
    **WAVE FUNCTIONS
    .HF
    **INTEGRALS
    .NOFAMILY
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 Li     3.000 3-21G                     15        9 [6s3p|3s2p]                                  
          2 H      1.000 3-21G                      3        2 [3s|2s]                                      
    ---------------------------------------------------------------------
    total          4                               18       11
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       11
      Primitive Regular basisfunctions   :       18
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!

    >>>  CPU Time used in *INPUT is   0.00 seconds
    >>> wall Time used in *INPUT is   0.00 seconds
    >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
    >>>  CPU Time used in II_precalc_ScreenMat is   0.00 seconds
    >>> wall Time used in II_precalc_ScreenMat is   0.00 seconds
    >>>  CPU Time used in OVERLAP is   0.00 seconds
    >>> wall Time used in OVERLAP is   0.00 seconds
 ndim =          11
    >>>  CPU Time used in *S is   0.00 seconds
    >>> wall Time used in *S is   0.00 seconds
WARNING: Turning off MPI for HODI test
    >>>  CPU Time used in hodiC1 is   0.05 seconds
    >>> wall Time used in hodiC1 is   0.05 seconds
 HODI contract1 expectation values: first-order geometrical-derivative Coulomb matrix
 RMS value for first-order geometrical-derivative Coulomb matrix          8.96883016
 RMS value for first-order geometrical-derivative Coulomb matrix         35.87532064
    >>>  CPU Time used in hodiC1 is   0.05 seconds
    >>> wall Time used in hodiC1 is   0.05 seconds
 HODI contract1 expectation values: first-order geometrical-derivative exchange matrix
 RMS value for first-order geometrical-derivative exchange matrix          2.26478931
 RMS value for first-order geometrical-derivative exchange matrix          9.05915725
 RMS value for first-order dipole-moment integrals contribution          5.84392352
 RMS value for first-order dipole-moment integrals contribution         11.68784703
    >>>  CPU Time used in CS15_Point is   0.00 seconds
    >>> wall Time used in CS15_Point is   0.00 seconds
    >>>  CPU Time used in EP-int is   0.00 seconds
    >>> wall Time used in EP-int is   0.00 seconds
    >>>  CPU Time used in ep_mat is   0.00 seconds
    >>> wall Time used in ep_mat is   0.00 seconds
 RMS value for first-order geometrical-derivative of charge and ep-dipole ep-moments          0.71247676
 RMS value for first-order geometrical-derivative of charge and ep-dipole ep-moments          1.42495351
    >>>  CPU Time used in CS15_Point is   0.00 seconds
    >>> wall Time used in CS15_Point is   0.00 seconds
    >>>  CPU Time used in EP-int is   0.00 seconds
    >>> wall Time used in EP-int is   0.00 seconds
    >>>  CPU Time used in ep_mat is   0.00 seconds
    >>> wall Time used in ep_mat is   0.00 seconds
 RMS value for first-order geometrical-derivative of dipole ep-moments          0.79790755
 RMS value for first-order geometrical-derivative of dipole ep-moments          1.59581509
    >>>  CPU Time used in CS15_Point is   0.00 seconds
    >>> wall Time used in CS15_Point is   0.00 seconds
    >>>  CPU Time used in EP-int is   0.01 seconds
    >>> wall Time used in EP-int is   0.01 seconds
    >>>  CPU Time used in ep_mat is   0.00 seconds
    >>> wall Time used in ep_mat is   0.00 seconds
 RMS value for first-order geometrical-derivative of charge to quadropole ep-moments            Infinity
 RMS value for first-order geometrical-derivative of charge to quadropole ep-moments            Infinity
    >>>  CPU Time used in CS15_Point is   0.00 seconds
    >>> wall Time used in CS15_Point is   0.00 seconds
    >>>  CPU Time used in EP-int is   0.01 seconds
    >>> wall Time used in EP-int is   0.01 seconds
    >>>  CPU Time used in ep_mat is   0.00 seconds
    >>> wall Time used in ep_mat is   0.00 seconds
 RMS value for first-order geometrical-derivative of dipole to third-order ep-moments                 NaN
 RMS value for first-order geometrical-derivative of dipole to third-order ep-moments                 NaN
    >>>  CPU Time used in hodiC1 is   0.13 seconds
    >>> wall Time used in hodiC1 is   0.13 seconds
 HODI contract1 expectation values: second-order geometrical-derivative Coulomb matrix
 RMS value for second-order geometrical-derivative Coulomb matrix          9.45606603
 RMS value for second-order geometrical-derivative Coulomb matrix         37.82426410
    >>>  CPU Time used in hodiC1 is   0.48 seconds
    >>> wall Time used in hodiC1 is   0.48 seconds
 HODI contract1 expectation values: third-order geometrical-derivative Coulomb matrix
 RMS value for third-order geometrical-derivative Coulomb matrix          3.35443278
 RMS value for third-order geometrical-derivative Coulomb matrix         13.41773111
    >>>  CPU Time used in hodiC1 is   0.48 seconds
    >>> wall Time used in hodiC1 is   0.48 seconds
 HODI contract1 expectation values: third-order geometrical-derivative exchange matrix
 RMS value for third-order geometrical-derivative exchange matrix          0.64349631
 RMS value for third-order geometrical-derivative exchange matrix          2.57398524
    >>>  CPU Time used in hodiC2 is   0.61 seconds
    >>> wall Time used in hodiC2 is   0.61 seconds
 HODI contract1 expectation values: third-order geometrical-derivative Coulomb expectation
 RMS value for third-order geometrical-derivative Coulomb expectation          3.35443278
 RMS value for third-order geometrical-derivative Coulomb expectation         13.41773111
    >>>  CPU Time used in hodiC2 is   0.61 seconds
    >>> wall Time used in hodiC2 is   0.61 seconds
 HODI contract1 expectation values: third-order geometrical-derivative exchange expectation
 RMS value for third-order geometrical-derivative exchange expectation          0.64349631
 RMS value for third-order geometrical-derivative exchange expectation          2.57398524
 RMS value for third-order overlap derivative matrix          0.06684186
 RMS value for third-order overlap derivative matrix          0.13368372
    >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
 RMS value for third-order nuclear-electron repulsion derivative matrix          0.93668126
 RMS value for third-order nuclear-electron repulsion derivative matrix          1.87336253
 RMS value for third-order kinetic-integral derivative matrix          0.05647066
 RMS value for third-order kinetic-integral derivative matrix          0.11294132
 RMS value for third-order overlap derivative expectation          0.06684186
 RMS value for third-order overlap derivative expectation          0.13368372
    >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
 RMS value for third-order nuclear-electron repulsion derivative expectation          0.93668126
 RMS value for third-order nuclear-electron repulsion derivative expectation          1.87336253
 RMS value for third-order kinetic-integral derivative expectation          0.05647066
 RMS value for third-order kinetic-integral derivative expectation          0.11294132
    >>>  CPU Time used in CS15_Point is   0.00 seconds
    >>> wall Time used in CS15_Point is   0.00 seconds
    >>>  CPU Time used in EP-int is   0.03 seconds
    >>> wall Time used in EP-int is   0.03 seconds
    >>>  CPU Time used in ep_mat is   0.00 seconds
    >>> wall Time used in ep_mat is   0.00 seconds
 RMS value for third-order geometrical-derivative of dipole ep-moments          2.87285081
 RMS value for third-order geometrical-derivative of dipole ep-moments          5.74570162
    >>>  CPU Time used in hodiC1 is   1.73 seconds
    >>> wall Time used in hodiC1 is   1.73 seconds
 HODI contract1 expectation values: fourth-order geometrical-derivative Coulomb matrix
 RMS value for fourth-order geometrical-derivative Coulomb matrix          3.87301932
 RMS value for fourth-order geometrical-derivative Coulomb matrix         15.49207728
    >>>  CPU Time used in hodiC1 is   1.73 seconds
    >>> wall Time used in hodiC1 is   1.73 seconds
 HODI contract1 expectation values: fourth-order geometrical-derivative exchange matrix
 RMS value for fourth-order geometrical-derivative exchange matrix          1.52075609
 RMS value for fourth-order geometrical-derivative exchange matrix          6.08302437
    >>>  CPU Time used in hodiC2 is   1.99 seconds
    >>> wall Time used in hodiC2 is   2.00 seconds
 HODI contract1 expectation values: fourth-order geometrical-derivative Coulomb expectation
 RMS value for fourth-order geometrical-derivative Coulomb expectation          3.87301932
 RMS value for fourth-order geometrical-derivative Coulomb expectation         15.49207728
    >>>  CPU Time used in hodiC2 is   1.98 seconds
    >>> wall Time used in hodiC2 is   1.98 seconds
 HODI contract1 expectation values: fourth-order geometrical-derivative exchange expectation
 RMS value for fourth-order geometrical-derivative exchange expectation          1.52075609
 RMS value for fourth-order geometrical-derivative exchange expectation          6.08302437
 RMS value for fourth-order overlap derivative matrix          0.07786811
 RMS value for fourth-order overlap derivative matrix          0.15573621
    >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
 RMS value for fourth-order nuclear-electron repulsion derivative matrix          1.35188217
 RMS value for fourth-order nuclear-electron repulsion derivative matrix          2.70376435
 RMS value for fourth-order kinetic-integral derivative matrix          0.08327877
 RMS value for fourth-order kinetic-integral derivative matrix          0.16655754
 RMS value for fourth-order overlap derivative expectation          0.07786811
 RMS value for fourth-order overlap derivative expectation          0.15573621
    >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
 RMS value for fourth-order nuclear-electron repulsion derivative expectation          1.35188217
 RMS value for fourth-order nuclear-electron repulsion derivative expectation          2.70376435
 RMS value for fourth-order kinetic-integral derivative expectation          0.08327877
 RMS value for fourth-order kinetic-integral derivative expectation          0.16655754
    >>>  CPU Time used in CS15_Point is   0.00 seconds
    >>> wall Time used in CS15_Point is   0.00 seconds
    >>>  CPU Time used in EP-int is   0.07 seconds
    >>> wall Time used in EP-int is   0.07 seconds
    >>>  CPU Time used in ep_mat is   0.00 seconds
    >>> wall Time used in ep_mat is   0.00 seconds
 RMS value for fourth-order geometrical-derivative of dipole ep-moments         10.09994900
 RMS value for fourth-order geometrical-derivative of dipole ep-moments         20.19989800
    >>>  CPU Time used in D-HODI is  10.26 seconds
    >>> wall Time used in D-HODI is  10.27 seconds
    >>>  CPU Time used in NucElec is   0.00 seconds
    >>> wall Time used in NucElec is   0.00 seconds
    >>>  CPU Time used in Kinetic is   0.00 seconds
    >>> wall Time used in Kinetic is   0.00 seconds
    >>>  CPU Time used in *H1 is   0.00 seconds
    >>> wall Time used in *H1 is   0.00 seconds

    First density: Atoms in molecule guess

    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    Iteration 0 energy:       -7.318924279926
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    >>>  CPU Time used in ATOMS: DiagF is   0.00 seconds
    >>> wall Time used in ATOMS: DiagF is   0.00 seconds
    >>>  CPU Time used in *START is   0.00 seconds
    >>> wall Time used in *START is   0.00 seconds
    Preparing to do S^1/2 decomposition...
    >>>  CPU Time used in LWDIAG is   0.00 seconds
    >>> wall Time used in LWDIAG is   0.00 seconds
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    >>>  CPU Time used in INIT SCF is   0.00 seconds
    >>> wall Time used in INIT SCF is   0.00 seconds
    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    >>>  CPU Time used in FCK_FO is   0.01 seconds
    >>> wall Time used in FCK_FO is   0.01 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1     -7.8448491616    0.00000000000    0.00      0.00000    0.00    6.930E-02 ###
    >>>  CPU Time used in AVERAG is   0.00 seconds
    >>> wall Time used in AVERAG is   0.00 seconds
    >>>  CPU Time used in HOMO-LUMO gap is   0.00 seconds
    >>> wall Time used in HOMO-LUMO gap is   0.00 seconds
    >>>  CPU Time used in CROP solver is   0.00 seconds
    >>> wall Time used in CROP solver is   0.00 seconds
    >>>  CPU Time used in NEW D is   0.00 seconds
    >>> wall Time used in NEW D is   0.00 seconds
    >>>  CPU Time used in G_DENS is   0.00 seconds
    >>> wall Time used in G_DENS is   0.00 seconds
    >>>  CPU Time used in SCF iteration is   0.01 seconds
    >>> wall Time used in SCF iteration is   0.01 seconds
    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    >>>  CPU Time used in FCK_FO is   0.01 seconds
    >>> wall Time used in FCK_FO is   0.01 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
      2     -7.8561661410   -0.01131697938    0.00      0.00000   -0.00    2.683E-02 ###
    >>>  CPU Time used in AVERAG is   0.00 seconds
    >>> wall Time used in AVERAG is   0.00 seconds
    >>>  CPU Time used in HOMO-LUMO gap is   0.00 seconds
    >>> wall Time used in HOMO-LUMO gap is   0.00 seconds
    >>>  CPU Time used in CROP solver is   0.00 seconds
    >>> wall Time used in CROP solver is   0.00 seconds
    >>>  CPU Time used in NEW D is   0.00 seconds
    >>> wall Time used in NEW D is   0.00 seconds
    >>>  CPU Time used in G_DENS is   0.00 seconds
    >>> wall Time used in G_DENS is   0.00 seconds
    >>>  CPU Time used in SCF iteration is   0.01 seconds
    >>> wall Time used in SCF iteration is   0.01 seconds
    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    >>>  CPU Time used in FCK_FO is   0.01 seconds
    >>> wall Time used in FCK_FO is   0.01 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
      3     -7.8589515814   -0.00278544041    0.00      0.00000   -0.00    5.520E-03 ###
    >>>  CPU Time used in AVERAG is   0.00 seconds
    >>> wall Time used in AVERAG is   0.00 seconds
    >>>  CPU Time used in HOMO-LUMO gap is   0.00 seconds
    >>> wall Time used in HOMO-LUMO gap is   0.00 seconds
    >>>  CPU Time used in CROP solver is   0.00 seconds
    >>> wall Time used in CROP solver is   0.00 seconds
    >>>  CPU Time used in NEW D is   0.00 seconds
    >>> wall Time used in NEW D is   0.00 seconds
    >>>  CPU Time used in G_DENS is   0.00 seconds
    >>> wall Time used in G_DENS is   0.00 seconds
    >>>  CPU Time used in SCF iteration is   0.01 seconds
    >>> wall Time used in SCF iteration is   0.01 seconds
    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    >>>  CPU Time used in FCK_FO is   0.01 seconds
    >>> wall Time used in FCK_FO is   0.01 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
      4     -7.8590377970   -0.00008621564    0.00      0.00000   -0.00    8.476E-04 ###
    >>>  CPU Time used in AVERAG is   0.00 seconds
    >>> wall Time used in AVERAG is   0.00 seconds
    >>>  CPU Time used in HOMO-LUMO gap is   0.00 seconds
    >>> wall Time used in HOMO-LUMO gap is   0.00 seconds
    >>>  CPU Time used in CROP solver is   0.00 seconds
    >>> wall Time used in CROP solver is   0.00 seconds
    >>>  CPU Time used in NEW D is   0.00 seconds
    >>> wall Time used in NEW D is   0.00 seconds
    >>>  CPU Time used in G_DENS is   0.00 seconds
    >>> wall Time used in G_DENS is   0.00 seconds
    >>>  CPU Time used in SCF iteration is   0.01 seconds
    >>> wall Time used in SCF iteration is   0.01 seconds
    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    >>>  CPU Time used in FCK_FO is   0.01 seconds
    >>> wall Time used in FCK_FO is   0.01 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
      5     -7.8590391235   -0.00000132648    0.00      0.00000   -0.00    1.455E-04 ###
    >>>  CPU Time used in AVERAG is   0.00 seconds
    >>> wall Time used in AVERAG is   0.00 seconds
    >>>  CPU Time used in HOMO-LUMO gap is   0.00 seconds
    >>> wall Time used in HOMO-LUMO gap is   0.00 seconds
    >>>  CPU Time used in CROP solver is   0.00 seconds
    >>> wall Time used in CROP solver is   0.00 seconds
    >>>  CPU Time used in NEW D is   0.00 seconds
    >>> wall Time used in NEW D is   0.00 seconds
    >>>  CPU Time used in G_DENS is   0.00 seconds
    >>> wall Time used in G_DENS is   0.00 seconds
    >>>  CPU Time used in SCF iteration is   0.01 seconds
    >>> wall Time used in SCF iteration is   0.01 seconds
    >>>  CPU Time used in reg-Jengine is   0.00 seconds
    >>> wall Time used in reg-Jengine is   0.00 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.00 seconds
    >>> wall Time used in LINK-Kbuild is   0.00 seconds
    >>>  CPU Time used in FCK_FO is   0.01 seconds
    >>> wall Time used in FCK_FO is   0.01 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
      6     -7.8590391534   -0.00000002994    0.00      0.00000   -0.00    9.839E-06 ###
    SCF converged in      6 iterations
    >>>  CPU Time used in SCF iterations is   0.04 seconds
    >>> wall Time used in SCF iterations is   0.04 seconds

    Total no. of matmuls in SCF optimization:        487

    Number of occupied orbitals:       2
    Number of virtual orbitals:        9

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     2 iterations!

    Calculation of virtual orbital energies converged in     5 iterations!

     E(LUMO):                         0.011561 au
    -E(HOMO):                        -0.318913 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.330475 au

    >>>  CPU Time used in HL GAP is   0.37 seconds
    >>> wall Time used in HL GAP is   0.37 seconds

    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.01131697938    0.0000   -0.0000    0.0000000
      3   -0.00278544041    0.0000   -0.0000    0.0000000
      4   -0.00008621564    0.0000   -0.0000    0.0000000
      5   -0.00000132648    0.0000   -0.0000    0.0000000
      6   -0.00000002994    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1            -7.84484916160252332418      0.693041124739674D-01
        2            -7.85616614098427845647      0.268299983761972D-01
        3            -7.85895158139026861477      0.552007324683219D-02
        4            -7.85903779702850258104      0.847567206099377D-03
        5            -7.85903912351281164206      0.145476937297893D-03
        6            -7.85903915344869830051      0.983915038432950D-05

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                        -7.859039153449
          Nuclear repulsion:                       1.456451032018
          Electronic energy:                      -9.315490185467

    Total no. of matmuls used:                       503
    Total no. of Fock/KS matrix evaluations:           7
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                        358.583 MB
      Max allocated memory, type(matrix)                 369.112 kB
      Max allocated memory, real(realk)                  358.445 MB
      Max allocated memory, integer                       54.264 kB
      Max allocated memory, logical                        0.672 kB
      Max allocated memory, character                      2.384 kB
      Max allocated memory, AOBATCH                       27.040 kB
      Max allocated memory, ODBATCH                        4.928 kB
      Max allocated memory, LSAOTENSOR                     3.072 kB
      Max allocated memory, SLSAOTENSOR                    3.248 kB
      Max allocated memory, ATOMTYPEITEM                 100.528 kB
      Max allocated memory, ATOMITEM                       1.776 kB
      Max allocated memory, LSMATRIX                       6.336 kB
      Max allocated memory, OverlapT                     166.656 kB
      Max allocated memory, linkshell                      1.260 kB
      Max allocated memory, integrand                     64.512 kB
      Max allocated memory, integralitem                  73.665 MB
      Max allocated memory, IntWork                      142.283 MB
      Max allocated memory, Overlap                      388.008 kB
      Max allocated memory, ODitem                         3.584 kB
      Max allocated memory, LStensor                       5.975 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is  10.68 seconds
    >>> wall Time used in LSDALTON is  10.69 seconds

    End simulation
