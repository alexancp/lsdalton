#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > lsdalton_dft_ssfgrid.info <<'%EOF%'
   lsdalton_dft_ssfgrid
   ------------
   Molecule:         Two H2O molecules 
   Model       :     DFT (LDA) / 3-21G 
   Test Purpose:     Check the blocked SSF schmeme in the DFT code
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > lsdalton_dft_ssfgrid.mol <<'%EOF%'
BASIS
3-21G
Two H2O
---------
    2              
        1.    4
H                  8.675000    0.000000  -16.640000
H                  9.579936    0.000000  -17.920455
H                  9.241330    0.571501   -2.493601
H                 10.146266    0.571501   -3.774055
        8.    2
O                  8.675000    0.000000  -17.600000
O                  9.241330    0.571501   -3.453601
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > lsdalton_dft_ssfgrid.dal <<'%EOF%'
**WAVEFUNCTION
.DFT
LDA
*DFT INPUT
.GRID TYPE
 LMG BLOCKSSF
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
*END OF INPUT
%EOF%
#######################################################################
 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > lsdalton_dft_ssfgrid.check
cat >> lsdalton_dft_ssfgrid.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# DFT energy:
CRIT1=`$GREP "Final *DFT energy: *\-147\.8121472" $log | wc -l`
TEST[1]=$CRIT1
CTRL[1]=1
ERROR[1]="SCF ENERGY NOT CORRECT"

# DFT - partioning scheme
CRIT1=`$GREP "Space partitioning\: Stratmann-Scuseria-Frisch partitioning scheme" $log | wc -l`
CRIT2=`$GREP "Combined with a blockwise handling of grid points" $log | wc -l`
TEST[2]=`expr  $CRIT1 \+ $CRIT2`
CTRL[2]=2
ERROR[2]="Partitioning scheme not correct"

# DFT - number of grid points:
#CRIT1=`$GREP "Postprocessing compression: from 52784 to 47528" $log | wc -l`
#TEST[3]=$CRIT1
#CTRL[3]=1
#ERROR[3]="Number of grid points not correct"
   
# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"
                                                                             
PASSED=1
for i in 1 2 3
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
