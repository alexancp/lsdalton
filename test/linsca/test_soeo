#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > test_soeo.info <<'%EOF%'
   Test second order ensemble optimisation
   ------------------------------------------------------
   Molecule:         H2 
   Basis:            cc-pVDZ
   Test Purpose:     Test if total energy and resulting occupation 
                     numbers are correct
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > test_soeo.mol <<'%EOF%'
BASIS
cc-pVDZ


Atomtypes=1 Charge=0 Angstrom
Charge=1.0 Atoms=2
H     0.0000000000            0.0000000000            5.00000000000000000000
H     0.0000000000            0.0000000000           -5.00000000000000000000
%EOF%
#######################################################################
#  DALTON INPUT
#######################################################################
cat > test_soeo.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
 LDA
*DENSOPT
.TEST SOEO
.SOEO
.SOEOSPACE
0 2
.SOEOOCC
0
2
0.6 0.4
.ARH
.CONVDYN
 STANDARD
.PRINTFINALCMO
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >test_soeo.check
cat >>test_soeo.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy 
CRIT1=`$GREP "TEST\: Final SOEO energy * -0\.88766" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="Energy is not correct"

# Occupation numbers
CRIT1=`$GREP "TEST\: Final occupation 1 * 0\.499999" $log | wc -l`
CRIT2=`$GREP "TEST\: Final occupation 2 * 0\.500000" $log | wc -l`
CRIT3=`$GREP "TEST\: Final occupation 1 * 0\.500000" $log | wc -l`
CRIT4=`$GREP "TEST\: Final occupation 2 * 0\.499999" $log | wc -l`
TEST[2]=`expr   $CRIT1 + $CRIT2 + $CRIT3 + $CRIT4`
CTRL[2]=2
ERROR[2]="Occupation numbers are not correct"

# First gradient
#CRIT1=`$GREP "TEST\: First gradnorm \= * 0\.2513" $log | wc -l`
#TEST[3]=`expr   $CRIT1`
#CTRL[3]=2
#ERROR[3]="Gradient is not correct"

# Memory test for matrices
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
CRIT2=`$GREP "TEST\: Occu * 0\.5000" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak for matrices -"


PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%




