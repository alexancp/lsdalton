#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > HF_vib.info <<'%EOF%'
   HF_vib 
   -------------
   Molecule:         HF
   Wave Function:    HF / STO-3G

   Test Purpose:     Test direct dynamics of vibrating HF with
   time-reversible density propagation
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > HF_vib.mol <<'%EOF%'
BASIS                                                                           
STO-3G
Direct dynamics calculation of vibrating HF molecule
No rotation
    2    0
        9.    1                                                                 
F     0.1   -0.7    0.1
        1.    1                                                                 
H     1.7    0.3   -0.2
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > HF_vib.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
 DIIS
.RESTART
**DYNAMI
.TIMREV
2
.NUMTRA
1
.VERLET
.MAX ITER
20
.TIMESTEP
0.5D0
.VELOCI
0
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > HF_vib.check
cat >> HF_vib.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-98\.5719851" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Energy
CRIT1=`$GREP "Energy drift, mHartree/ps  :  \-3946\.234" $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="Energy drift -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
