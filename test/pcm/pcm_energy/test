#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))  # isort: skip

from runtest import version_info, get_filter, cli, run
from runtest_config import configure


assert version_info.major == 2

options = cli()

f_hf = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final HF energy:", rel_tolerance=1.0e-9),
]

ierr = 0
for inp in ["arh_energy_hf.dal", "energy_hf.dal", "energy_hf_separate.dal"]:
    for mol in ["H2O.mol", "CH4.mol"]:
        ierr += run(
            options,
            configure,
            input_files=[inp, mol, "pcmsolver.pcm"],
            filters={"out": f_hf},
        )

f_dft = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final DFT energy:", rel_tolerance=1.0e-9),
]

ierr = 0
for inp in ["arh_energy_lda.dal", "energy_lda.dal", "energy_lda_separate.dal"]:
    for mol in ["H2O.mol", "CH4.mol"]:
        ierr += run(
            options,
            configure,
            input_files=[inp, mol, "pcmsolver.pcm"],
            filters={"out": f_dft},
        )

sys.exit(ierr)
