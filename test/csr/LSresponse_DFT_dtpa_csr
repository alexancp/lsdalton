#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_DFT_dtpa_csr.info <<'%EOF%'
   LSresponse_DFT_dtpa_csr
   -------------------
   Molecule:         HF
   Wave Function:    DFT-B3LYP / STO-3G
   Test Purpose:     Test damped two-photon absorption in LSDALTON (Kasper K).
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_DFT_dtpa_csr.mol <<'%EOF%'
BASIS
STO-3G
-----------test----------

Atomtypes=2 Generators=0
Charge=1.0 Atoms=1 Basis=STO-3G
H 0.000000000 0.0000000000   0.000000000000000
Charge=9.0 Atoms=1 Basis=STO-3G
F 1.96000000  0.0000000000   0.000000000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_DFT_dtpa_csr.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.CSR
.ARH                                                                                       
.NVEC
8
.CONVTHR
1.0D-6
**RESPONSE
*DAMPED_TPA
.GAMMA
0.001
.OPFREQ
1
0.372258445
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_DFT_dtpa_csr.check
cat >> LSresponse_DFT_dtpa_csr.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-98.8868" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Isotropically averaged TPA
CRIT1=`$GREP "0.37225[0-9][0-9] * 1281\.5[0-9][0-9] * 528.92" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Error in damped TPA"


PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
