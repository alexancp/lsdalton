  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pbaudin
     Host                     | fe2
     System                   | Linux-2.6.32-504.16.2.el6.x86_64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | ON
     MPI                      | OFF
     Fortran compiler         | /com/intel/composer_xe_2013_sp1.2.144/bin/intel64/
                              | ifort
     Fortran compiler version | ifort (IFORT) 14.0.2 20140120
     C compiler               | /com/intel/composer_xe_2013_sp1.2.144/bin/intel64/
                              | icc
     C compiler version       | icc (ICC) 14.0.2 20140120
     C++ compiler             | /com/intel/composer_xe_2013_sp1.2.144/bin/intel64/
                              | icpc
     C++ compiler version     | icpc (ICC) 14.0.2 20140120
     BLAS                     | -Wl,--start-group;/com/intel/composer_xe_2013_sp1.
                              | 2.144/mkl/lib/intel64/libmkl_intel_ilp64.so;/com/i
                              | ntel/composer_xe_2013_sp1.2.144/mkl/lib/intel64/li
                              | bmkl_intel_thread.so;/com/intel/composer_xe_2013_s
                              | p1.2.144/mkl/lib/intel64/libmkl_core.so;/usr/lib64
                              | /libpthread.so;/usr/lib64/libm.so;-openmp;-Wl,--en
                              | d-group
     LAPACK                   | -Wl,--start-group;/com/intel/composer_xe_2013_sp1.
                              | 2.144/mkl/lib/intel64/libmkl_lapack95_ilp64.a;/com
                              | /intel/composer_xe_2013_sp1.2.144/mkl/lib/intel64/
                              | libmkl_intel_ilp64.so;-openmp;-Wl,--end-group
     Static linking           | OFF
     Last Git revision        | fd2ae77d6e553b97912a02cb967bdb8e72e55753
     Git branch               | master
     Configuration time       | 2016-05-12 15:44:40.007310
  
 Linear response calculations are carried out.

         Start simulation
     Date and time (Linux)  : Thu May 12 22:34:22 2016
     Host name              : fe2                                     
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G                                   
    ==============                          
    =============                           
    Atomtypes=2 Nosymmetry Angstrom                                                                                         
    Charge=6.0 Atoms=1                                                                                                      
    C   0.0000000   0.000000   0.000000                                                                                     
    Charge=8.0 Atoms=1                                                                                                      
    O   1.1280000   0.000000   0.000000                                                                                     
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RESTART
    .RH
    .DIIS
    .CONVTHR
    1.d-7
    .START
    ATOMS
    **RESPONSE
    *LINRSP
    .NEXCIT
    4
    *SOLVER
    .RESTEXC
    2
    .CONVTHR
    1.0D-5
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 C      6.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
    ---------------------------------------------------------------------
    total         14                               44       18
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       18
      Primitive Regular basisfunctions   :       44
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is an OpenMP calculation using  16 threads.
    This is a serial calculation (no MPI)
 
    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    
 
    Maximum size of Fock/density queue in averaging:   10
 
    Convergence threshold for gradient        :   0.10E-06
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Since the input basis set is a segmented contracted basis we
    perform the integral evaluation in the more efficient
    standard input basis and then transform to the Grand 
    Canonical basis, which is general contracted.
    You can force the integral evaluation in Grand 
    Canonical basis by using the keyword
    .NOGCINTEGRALTRANSFORM
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis
 
    End of configuration!
 
 Using 64-bit integers!
 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 
 
    Level 1 atomic calculation on 6-31G Charge   6
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -36.1261883331    0.00000000000    0.00      0.00000    0.00    0.0000000    1.811E+00 ###
      2    -37.2417446448   -1.11555631170    0.00      0.00000    0.00    0.0000000    5.280E-01 ###
      3    -37.3143365252   -0.07259188037   -1.00      0.00000    0.00    0.0000000    8.028E-02 ###
      4    -37.3153570603   -0.00102053510   -1.00      0.00000    0.00    0.0000000    1.000E-02 ###
      5    -37.3153753736   -0.00001831338   -1.00      0.00000    0.00    0.0000000    5.076E-04 ###
 
    Level 1 atomic calculation on 6-31G Charge   8
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -71.4946859143    0.00000000000    0.00      0.00000    0.00    0.0000000    4.531E+00 ###
      2    -73.8341599123   -2.33947399805    0.00      0.00000    0.00    0.0000000    2.663E+00 ###
      3    -74.2580432558   -0.42388334348   -1.00      0.00000    0.00    0.0000000    1.607E-01 ###
      4    -74.2598387062   -0.00179545041   -1.00      0.00000    0.00    0.0000000    3.161E-02 ###
      5    -74.2598920070   -0.00005330082   -1.00      0.00000    0.00    0.0000000    2.600E-03 ###
      6    -74.2598924085   -0.00000040150   -1.00      0.00000    0.00    0.0000000    1.479E-06 ###
 
    Matrix type: mtype_dense
 
    First density: Atoms in molecule guess
 
    Iteration 0 energy:     -112.924987413669
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  1.00000000E-07
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1   -112.6227652613    0.00000000000    0.00      0.00000    0.00    0.0000000    1.368E+00 ###
      2   -112.6338803058   -0.01111504455    0.00      0.00000    0.00    0.0000000    1.376E+00 ###
      3   -112.6669961781   -0.03311587231   -1.00      0.00000    0.00    0.0000000    6.716E-02 ###
      4   -112.6672019172   -0.00020573908   -1.00      0.00000    0.00    0.0000000    6.932E-03 ###
      5   -112.6672044333   -0.00000251607   -1.00      0.00000    0.00    0.0000000    1.709E-03 ###
      6   -112.6672045502   -0.00000011699   -1.00      0.00000    0.00    0.0000000    3.768E-04 ###
      7   -112.6672045587   -0.00000000843   -1.00      0.00000    0.00    0.0000000    7.720E-05 ###
      8   -112.6672045590   -0.00000000029   -1.00      0.00000    0.00    0.0000000    7.007E-06 ###
      9   -112.6672045590   -0.00000000000   -1.00      0.00000    0.00    0.0000000    5.430E-07 ###
     10   -112.6672045590    0.00000000000   -1.00      0.00000    0.00    0.0000000    5.467E-08 ###
    SCF converged in     10 iterations
    >>>  CPU Time used in SCF iterations is   0.65 seconds
    >>> wall Time used in SCF iterations is   0.04 seconds
 
    Total no. of matmuls in SCF optimization:        127
 Postponing calculation of HOMO-LUMO gap to response part...
 
    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.01111504455    0.0000    0.0000    0.0000000
      3   -0.03311587231   -1.0000    0.0000    0.0000000
      4   -0.00020573908   -1.0000    0.0000    0.0000000
      5   -0.00000251607   -1.0000    0.0000    0.0000000
      6   -0.00000011699   -1.0000    0.0000    0.0000000
      7   -0.00000000843   -1.0000    0.0000    0.0000000
      8   -0.00000000029   -1.0000    0.0000    0.0000000
      9   -0.00000000000   -1.0000    0.0000    0.0000000
     10    0.00000000000   -1.0000    0.0000    0.0000000
 
    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1          -112.62276526125012310331      0.136786721929618D+01
        2          -112.63388030579727683289      0.137620517311973D+01
        3          -112.66699617810314748567      0.671600151845621D-01
        4          -112.66720191718053456498      0.693206445558411D-02
        5          -112.66720443325027645187      0.170884868582034D-02
        6          -112.66720455023931890537      0.376785343205138D-03
        7          -112.66720455866521888311      0.771961156860336D-04
        8          -112.66720455895313079964      0.700677911863630D-05
        9          -112.66720455895446661998      0.542976101932907D-06
       10          -112.66720455895438135485      0.546711325491810D-07
 
          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<
 
 
          Final HF energy:                      -112.667204558954
          Nuclear repulsion:                      22.518179076596
          Electronic energy:                    -135.185383635550
 



 <<< EXCITATION ENERGIES AND TRANSITION MOMENT CALCULATION (LSTDHF) >>>


 Use MO orbital energy differences to find  first guess for eigenvectors/values
Restart from     2 vectors from the rsp_eigenvecs file.
  ** RSP_SOLVER MICROITERATION NUMBER                     1
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
 Response convergence threshold is  1.000000000000000E-005
Residual norm for vector   1 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   1 CONV = T
Residual norm for vector   2 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   1 CONV = T
Residual norm for vector   3 is:   0.216529E+00    and eigenvalue =   0.37547368   It =   1 CONV = F
 MO preconditioning
Residual norm for vector   4 is:   0.671798E+00    and eigenvalue =   0.54561594   It =   1 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER                     2
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
Residual norm for vector   1 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   2 CONV = T
Residual norm for vector   2 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   2 CONV = T
Residual norm for vector   3 is:   0.141405E+00    and eigenvalue =   0.35549376   It =   2 CONV = F
 MO preconditioning
Residual norm for vector   4 is:   0.270228E+00    and eigenvalue =   0.40593428   It =   2 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER                     3
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
Residual norm for vector   1 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   3 CONV = T
Residual norm for vector   2 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   3 CONV = T
Residual norm for vector   3 is:   0.543064E-01    and eigenvalue =   0.33853044   It =   3 CONV = F
 MO preconditioning
Residual norm for vector   4 is:   0.172547E+00    and eigenvalue =   0.37517863   It =   3 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER                     4
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
Residual norm for vector   1 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   4 CONV = T
Residual norm for vector   2 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   4 CONV = T
Residual norm for vector   3 is:   0.296796E-01    and eigenvalue =   0.33594359   It =   4 CONV = F
 MO preconditioning
Residual norm for vector   4 is:   0.768564E-01    and eigenvalue =   0.36330680   It =   4 CONV = F
 MO preconditioning
  ** RSP_SOLVER MICROITERATION NUMBER                     5
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
Residual norm for vector   1 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   5 CONV = T
Residual norm for vector   2 is:   0.958316E-05    and eigenvalue =   0.31106102   It =   5 CONV = T
Residual norm for vector   3 is:   0.195462E-14    and eigenvalue =   0.33569702   It =   5 CONV = T
Residual norm for vector   4 is:   0.925140E-14    and eigenvalue =   0.36199742   It =   5 CONV = T

 *** THE REQUESTED              4 SOLUTION VECTORS CONVERGED
 Write                      4  excitation vectors to disk
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
  * Start calculation of PCM contributions to Linear Response
  * Done calculation of PCM contributions to Linear Response
 
 The following excitation energies are used for the response calculation:
 
                     1  0.311061022289986     
                     2  0.311061022289987     
                     3  0.335697022512351     
                     4  0.361997416255176     
 Starting linear response function calculation for: << EL  ; EXCI>>
 
 Starting linear response function calculation for: << EL  ; EXCI>>
 
 Starting linear response function calculation for: << EL  ; EXCI>>
 
 Starting linear response function calculation for: << EL  ; EXCI>>
 
 
 
 
 
  ******************************************************************************
  *                   ONE-PHOTON ABSORPTION RESULTS (in a.u.)                  *
  ******************************************************************************
 
 
 
      Excitation              Transition Dipole Moments               Oscillator
       Energies            x               y               z           Strengths
 ===============================================================================
      0.31106102     -0.00000000     -0.06514163     -0.61495167      0.07930169
      0.31106102      0.00000000     -0.61495167      0.06514163      0.07930169
      0.33569702     -0.00000000     -0.00000000     -0.00000000      0.00000000
      0.36199742      0.00000000     -0.00000000     -0.00000000      0.00000000
 
 
 
 
  End of excitation energy calculation
    >>>  CPU Time used in LSDALTON RSP is   0.82 seconds
    >>> wall Time used in LSDALTON RSP is   0.06 seconds
 *****************************************************
 **     CPU-TIME USED IN LSDALTON RESPONSE:   0.822874000000000         **
 *****************************************************
    Total no. of matmuls used:                       667
    Total no. of Fock/KS matrix evaluations:          11
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                        123.478 MB
      Max allocated memory, type(matrix)                 217.984 kB
      Max allocated memory, real(realk)                  122.275 MB
      Max allocated memory, integer                        1.203 MB
      Max allocated memory, logical                        3.648 kB
      Max allocated memory, character                      2.496 kB
      Max allocated memory, AOBATCH                       42.240 kB
      Max allocated memory, ODBATCH                        5.040 kB
      Max allocated memory, LSAOTENSOR                     3.840 kB
      Max allocated memory, SLSAOTENSOR                    3.920 kB
      Max allocated memory, ATOMTYPEITEM                 233.440 kB
      Max allocated memory, ATOMITEM                       2.304 kB
      Max allocated memory, LSMATRIX                       8.448 kB
      Max allocated memory, OverlapT                       2.269 MB
      Max allocated memory, linkshell                      1.872 kB
      Max allocated memory, integrand                      4.129 MB
      Max allocated memory, integralitem                  29.491 MB
      Max allocated memory, IntWork                      881.536 kB
      Max allocated memory, Overlap                       87.369 MB
      Max allocated memory, ODitem                         3.528 kB
      Max allocated memory, LStensor                      49.956 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 
    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   2.93 seconds
    >>> wall Time used in LSDALTON is   0.23 seconds

    End simulation
     Date and time (Linux)  : Thu May 12 22:34:22 2016
     Host name              : fe2                                     
