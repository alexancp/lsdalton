list(APPEND LSDALTON_Fortran_FLAGS
  "-fpp"
  )

if(DEFINED MKL_FLAG)
  list(APPEND LSDALTON_Fortran_FLAGS
    ${MKL_FLAG}
    )
endif()

if(ENABLE_STATIC_LINKING)
  list(APPEND LSDALTON_Fortran_FLAGS
    "-static"
    "-static-libgcc"
    "-static-intel"
    )
endif()

list(APPEND LSDALTON_Fortran_FLAGS_DEBUG
  "-W1"
  "-O0"
  "-g"
  "-traceback"
  "-debug"
  )

list(APPEND LSDALTON_Fortran_FLAGS_RELEASE
  "-O3"
  "-ip"
  )
