list(APPEND LSDALTON_CXX_FLAGS
  "-g"
  "-wd981"
  "-wd279"
  "-wd383"
  "-wd1572"
  "-wd1777"
  "-fno-rtti"
  "-fno-exceptions"
  )

if(DEFINED MKL_FLAG)
  list(APPEND LSDALTON_CXX_FLAGS
    ${MKL_FLAG}
    )
endif()

list(APPEND LSDALTON_CXX_FLAGS_DEBUG
  "-Wall"
  "-O0"
  )

list(APPEND LSDALTON_CXX_FLAGS_RELEASE
  "-O3"
  "-ip"
  )
