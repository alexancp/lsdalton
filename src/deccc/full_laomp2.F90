!> @file full molecular calculation of Laplace Transformed AO based MP2 
!> Full calculation
!> 

module fulllaomp2
  use precision
  use typedeftype,only:lsitem
  use dec_typedef_module,only:fullmolecule
  public :: full_canonical_laomp2

  private

contains
  !> \brief Calculate canonical Laplace Transformed AO based MP2 energy for full molecular system
  !> \author Thomas Kjaergaard
  !> \date October 2015
  subroutine full_canonical_laomp2(MyMolecule,MyLsitem,laomp2_energy)
    implicit none
    !> Full molecule info
    type(fullmolecule), intent(inout) :: MyMolecule
    !> Lsitem structure
    type(lsitem), intent(inout) :: mylsitem
    !> Canonical MP2 correlation energy
    real(realk),intent(inout) :: laomp2_energy    

    !Empty subroutine 

  end subroutine full_canonical_laomp2

end module fulllaomp2

#ifdef VAR_MPI
subroutine full_canonical_laomp2_slave
  use fulllaomp2,only: full_canonical_laomp2
  use infpar_module !infpar
  use lsmpi_param,only:LSMPIBROADCAST,MPI_COMM_LSDALTON 
  use lsmpi_Buffer,only:ls_mpiInitBuffer,ls_mpiFinalizeBuffer
  use lsmpi_op,only: mpicopy_lsitem
  use precision
  use typedeftype,only:lsitem
  use lsparameters
  use decmpi_module, only: mpi_bcast_fullmolecule
  use DALTONINFO, only: ls_free
!  use typedef
  use dec_typedef_module,only:fullmolecule
  ! DEC DEPENDENCIES (within deccc directory)   
  ! *****************************************
!  use dec_fragment_utils
  use full_molecule, only: molecule_finalize
  implicit none
  !> Full molecule info
  type(fullmolecule) :: MyMolecule
  !> Lsitem structure
  type(lsitem) :: mylsitem
  !> Canonical MP2 correlation energy
  real(realk) :: rimp2_energy    
  
  ! Init MPI buffer
  ! ***************
  ! Main master:  Prepare for writing to buffer
  ! Local master: Receive buffer
!  call ls_mpiInitBuffer(infpar%master,LSMPIBROADCAST,MPI_COMM_LSDALTON)
  ! Integral lsitem
  ! ---------------
!  call mpicopy_lsitem(MyLsitem,MPI_COMM_LSDALTON)
  
!  call ls_mpiFinalizeBuffer(infpar%master,LSMPIBROADCAST,MPI_COMM_LSDALTON)
  ! Full molecule bcasting
  ! **********************
!  call mpi_bcast_fullmolecule(MyMolecule)
  
  ! Finalize MPI buffer
  ! *******************
  ! Main master:  Send stuff to local masters and deallocate temp. buffers
  ! Local master: Deallocate buffer etc.
!  call full_canonical_laomp2(MyMolecule,MyLsitem,rimp2_energy)
!
!  call ls_free(MyLsitem)
!  call molecule_finalize(MyMolecule,.false.)
  
end subroutine full_canonical_laomp2_slave
#endif

