# if BLAS and/or LAPACK not found, compile own implementation
list(APPEND _builtin_blas
  ${CMAKE_CURRENT_LIST_DIR}/blas_wrapper.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_dblas1.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_dblas2.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_dblas3.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_sblas.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_qblas.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_zblas.F
  )

list(APPEND _builtin_lapack
  ${CMAKE_CURRENT_LIST_DIR}/gp_dlapack.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_zlapack.F
  ${CMAKE_CURRENT_LIST_DIR}/gp_zgesvd.F
  )

add_library(pdpack
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/jacobi.F
    ${CMAKE_CURRENT_LIST_DIR}/eispack.F
    ${CMAKE_CURRENT_LIST_DIR}/linpack.F
    ${CMAKE_CURRENT_LIST_DIR}/cholesky.F
    ${CMAKE_CURRENT_LIST_DIR}/invroutines.F
    "$<$<BOOL:${USE_BUILTIN_BLAS}>:$<JOIN:${_builtin_blas},;>>"
    "$<$<BOOL:${USE_BUILTIN_LAPACK}>:$<JOIN:${_builtin_lapack},;>>"
  )

target_compile_options(pdpack
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(lsdalton
  PUBLIC
    pdpack
  )
