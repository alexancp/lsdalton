#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > PA_oligomer_5_pariK.info <<'%EOF%'
   PA_oligomer_5_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > PA_oligomer_5_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
EXCITATION DIAGNOSTIC
CAM-B3LYP/6-31G* GEOMETRY
Atomtypes=2  Nosymmetry
Charge=6.0 Atoms=8
C1        -0.0905307083           -0.0000000077           -0.1531167190
C2         0.0002575985           -0.0000000086            2.3679187777
C3         2.3225637383           -0.0000000069            3.8302523252
C4         2.4053161483           -0.0000000054            6.3680454809
C5         4.7136803039           -0.0000000023            7.8332464142
C6         4.7964327135           -0.0000000008           10.3710395700
C7         7.1187388519            0.0000000022           11.8333731196
C8         7.2095271545            0.0000000037           14.3544086163
Charge=1.0 Atoms=10
H1         1.6215676524            0.0000000026           -1.2873880284
H2        -1.7547207434           -0.0000000014            3.4433502615
H3         4.0859713979           -0.0000000076            2.7670102907
H4         0.6375379228           -0.0000000066            7.4244915872
H5        -1.8706119671            0.0000000300           -1.1691485555
H6         6.4814585292           -0.0000000012            6.7768003086
H7         3.0330250528           -0.0000000020           11.4342816032
H8         8.8737171937            0.0000000034           10.7579416367
H9         8.9896084094            0.0000000061           15.3704404579
H10        5.4974287907            0.0000000027           15.4886799210


%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > PA_oligomer_5_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >PA_oligomer_5_pariK.check
cat >> PA_oligomer_5_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
