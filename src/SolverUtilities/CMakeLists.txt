add_library(SolverUtilities
  OBJECT
   ${CMAKE_CURRENT_LIST_DIR}/arh_density.F90
   ${CMAKE_CURRENT_LIST_DIR}/dalton_interface.F90
   ${CMAKE_CURRENT_LIST_DIR}/dd_utilities.F90
   ${CMAKE_CURRENT_LIST_DIR}/dd_utilities_unres.F90
   ${CMAKE_CURRENT_LIST_DIR}/decomp.F90
   ${CMAKE_CURRENT_LIST_DIR}/lsdalton_modules.F90
   ${CMAKE_CURRENT_LIST_DIR}/queue-operations.F90
   ${CMAKE_CURRENT_LIST_DIR}/queue-typedef.F90
   ${CMAKE_CURRENT_LIST_DIR}/rsp_precond.F90
   ${CMAKE_CURRENT_LIST_DIR}/rsp_utilities.F90
  )

target_compile_options(SolverUtilities
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(SolverUtilities
  PUBLIC
    lsutil
    LSint
  )

target_link_libraries(lsdalton
  PUBLIC
    SolverUtilities
  )
