module wannier_types

use precision
implicit none

!> @brief Information about the periodic lattice. 
!> @author Karl Leikanger
!> @dat 2015
!> @var dims Number of dimensions that are periodic.
!> @var lvec Columns are primitive vectors.
!> @var dim_is_periodic Along which lattice vec(s) is the lattice periodic.
type, public :: bravais_lattice !rspace_lat_info_t
   integer :: dims
   logical :: dim_is_periodic(1:3) !is_active
   real(realk) :: lvec(1:3, 1:3) !avec 
end type

!> @brief Main derived type part of the configurationType initialized in the 
!> start of a simulation as config%wannier_config.
!> @author Karl Leikanger
!> @date 2015
!> @var do_wannier_scf Calc. a-priori wannier funcitons?
!> @var setup_bravais_lattice  Read bravais lattice from .mol - file.
!> @var blat The definition of the bravais lattice.
!> @var nf_cutoff Nearfield cutoff.
!> @var fmm_lmax Input with keyword .MAX_MULTIPOLE_MOMENT .
!> @var ao_cutoff Basis expanded in au_cutoff layers.
!> @var orthpot_strength The strength of the orthog. pot. (lambda)
!> @var debug Execute debug routines
!> @var debug_level Set print level for debug info 
!> @var n_bvk Number of k-points in Brillouin zone
!> @var scfthresh Convergence threshhold for sct routine.
!> @var maxiter Max number of scf iterations.
!> @var dump_dens Dump density matrix to disk
!> @var no_redundant Do not assume Hermiticity
!> @var orthonormalise Orthonormalise Wannier orbitals (initial and each iter)
!> @var unitary_scf Use unitary orbital rotations in SCF
!> @var scf_kspace
!> @var nbvk_scf_kspace if scf_kspace, define the size of the bvk lattice in layers
!> @var scf_shuklaAO turns on the AO version of Shukla's algorithm
!> @var scf_shuklaMO turns on the MO version of Shukla's algorithm
!> @var scf_original original version of the algorithm with orth. pot. on all orbitals
!> @var nocc number of occupied orbitals in the reference cell
!> @var localization turns on the localization
!> @var nbvk_scf_kspace if scf_kspace, define the size of the bvk lattice in layers
!> @var compute_bandstructure compute bandstructure for the same number of k -
!> points as the number of cells in the ao - cutoff TODOk - grid should be arbritrary.

type, public :: wannier_config
   logical :: do_wannier_scf
   logical :: setup_bravais_lattice
   type(bravais_lattice) :: blat 
   integer :: nf_cutoff(3)
   integer :: fmm_lmax
   real(realk) :: screening_thr
   integer :: ao_cutoff
   real(realk) :: orthpot_strength
   integer :: debug_level
   integer, dimension(3) :: n_bvk
   real(realk) :: scfthresh
   integer :: maxiter
   integer :: dump_dens
   logical :: no_redundant
   logical :: orthonormalise
   logical :: unitary_scf
   logical :: conv_start_guess
   logical :: dens_start
   logical :: scf_kspace 
   integer :: nbvk_scf_kspace
   logical :: scf_shuklaAO
   logical :: scf_shuklaMO
   logical :: scf_original
   integer :: nocc
   logical :: localization
   logical :: asymptotic_expansion
   logical :: compute_bandstructure
   integer, dimension(3) :: ff_cutoff
   logical :: crystal_init
end type wannier_config

end module
