module globals

  use, intrinsic :: iso_c_binding, only: c_char, &
       c_int, &
       c_null_char, &
       c_ptr, &
       c_funloc, &
       c_funptr, &
       c_f_pointer

  implicit none

  public :: dalton_version, install_basdir, binary_info

  private

  integer, save :: max_msg_len = 5000

contains

  subroutine host_writer(message, io_unit)
    character(kind=c_char, len=1), intent(in) :: message(*)
    integer(c_int), intent(in) :: io_unit
    integer(c_int) :: length, i

    length = 0
    do
       if (message(length + 1) == c_null_char) exit
       length = length + 1
    end do

    ! Let whoever sends a message decide its newlines
    write (unit=io_unit, fmt='(1000A)', advance="no") (message(i), i=1, length)
  end subroutine

  function dalton_version() result(text)
    interface
      function dalton_version_ptr() result(str) bind(C, name="dalton_version")
        import
        type(c_ptr) :: str
      end function
    end interface

    character(kind=c_char, len=max_msg_len) :: text
    text = c_f_string(dalton_version_ptr())
  end function

  function install_basdir() result(text)
    interface
      function install_basdir_ptr() result(str) bind(C, name="install_basdir")
        import
        type(c_ptr) :: str
      end function
    end interface

    character(kind=c_char, len=max_msg_len) :: text
    text = c_f_string(install_basdir_ptr())
  end function

  subroutine binary_info(io)
    interface
      subroutine print_info(writer, io_unit) bind(C, name="print_info")
        import
        type(c_funptr), intent(in), value :: writer
        integer(c_int), intent(in) :: io_unit
      end subroutine
    end interface

    integer, intent(in), optional :: io

    integer :: io_unit

    if (present(io)) then
      io_unit = io
    else
      io_unit = 6
    end if

    write(io_unit,'(1X,64("="))')
    flush(io_unit)

    call print_info(c_funloc(host_writer), int(io_unit, kind=c_int))
  end subroutine

  function c_f_string(cstring) result(text)
    type(c_ptr) :: cstring

    character(kind=c_char, len=max_msg_len) :: text
    character(kind=c_char), pointer, dimension(:) :: msg_array
    character(kind=c_char, len=max_msg_len+1) :: msg
    integer(c_int) :: msg_length
    integer :: i

    call c_f_pointer(cstring, msg_array, [ max_msg_len ])

    do i = 1, max_msg_len
       msg(i:i+1) = msg_array(i)
    end do

    msg_length = len_trim(msg(1:index(msg, c_null_char)))

    text = msg(1:msg_length-1)
  end function

end module
