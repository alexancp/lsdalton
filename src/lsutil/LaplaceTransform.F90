!> @file
!> contains the main routines to calculate laplace quadrature point
!> \author Jon Austad
!> \date 2015
MODULE LaplaceTransformMOD
  use precision
  use memory_handling
  use LaplaceTransformDatexpMod
  use LaplaceTransformBestapMod

  public :: get_laplace_quadrature_points, get_minimax_laplace_quadrature_points, &
       & get_minimax_fixed_laplace_quadrature_points
  private

contains
  !> \brief main routine to calculate laplace quadrature point
  !> \author Jon Austad
  !> \date 2015-10-06
  !> \param nlines number of orbital energies
  !> \param eps The list of orbital energies
  !> \param omega The desired number of laplace quadrature point
  !> \param nocc the number of occupied orbitals
  !> \param nvirt the number of virtual orbitals
  !> \param w_out The laplace quadrature weights
  !> \param t_out The laplace quadrature point exponents
  subroutine get_laplace_quadrature_points(nlines, eps, omega, nocc, nvirt, w_out, t_out,opt)
    implicit none
    integer, intent(in) :: nlines,opt
    real(realk), intent(in) :: eps(nlines)
    integer, intent(in) :: omega
    integer, intent(in) :: nocc
    integer, intent(in) :: nvirt
    real(realk), intent(inout) :: t_out(omega), w_out(omega)
    !local variables
    integer :: ix, jx, ax, bx, s, N_tot, N_virt, N_occ, alpha, iterations
    integer :: xsize,xsize2,xsize3
    integer(kind=long) :: xsize8
    integer :: xsize_max = 2048 !8192 !262144
    real(realk),pointer :: x(:)  
    real(realk) :: xmin, xmax, lsq_q1, lsq_q2, eps_aij, step
    real(realk) :: t(omega), w(omega)
    logical :: ACCEPTED
    integer :: i,InitialGuessOption,offset

    do I=1,40
       xsize_max = I*1024 !2048 !8192 !262144
       if(xsize_max.GT.nocc+nvirt)EXIT
    enddo

    N_tot = nlines
    N_occ = nocc
    N_virt = nvirt
    !without a 64 bit integer this could cause an integer overflow.     
    xsize8 = (nocc**2)*1_long*(nvirt**2) 
    xmin = 2.0E0_realk*(eps(N_occ+1) - eps(N_occ))
    xmax = 2.0E0_realk*(eps(N_tot) - eps(1))

!    print*,'xsize_max',xsize_max
!    print*,'xmin',xmin
!    print*,'xmax',xmax
!    print*,'eps(N_occ+1)',eps(N_occ+1)
!    print*,'eps(N_occ)  ',eps(N_occ)
!    print*,'eps(N_tot)  ',eps(N_tot)
!    print*,'eps(1)      ',eps(1)

    if (xsize8 .LT. xsize_max) then
       xsize = nocc**2*nvirt**2
       call mem_alloc(x,xsize,'LT:xA')
       call buildX(N_occ,N_virt,N_tot,eps,x)
    else
       xsize = xsize_max
       call mem_alloc(x,xsize,'LT:xB')
!       call buildXRed(N_occ,N_virt,N_tot,eps,x,xsize,xsize3)
!       xsize2 = xsize_max-(xsize3)
!       offset = xsize3
       xsize2 = xsize_max
       offset = 0

       ! check for fenceposts!
       step = (xmax - xmin)/(xsize2-1)
       x(offset+1) = xmin
       x(xsize) = xmax
       !$OMP PARALLEL DO DEFAULT(NONE) PRIVATE(s) SHARED(xmin,step,x,xsize2,offset)
       do s = 1,xsize2-2
          x(offset+s+1) = (xmin + s*step)
       end do
       !$OMP END PARALLEL DO
    end if

    InitialGuessOption = opt
    call alternate(w,t,omega,iterations,xmin,xmax,x,xsize,InitialGuessOption)
    call q_error(lsq_q1, lsq_q2, w, t, omega, x, xsize)
!
!    WRITE(*,*)'lsq_q1',lsq_q1,'lsq_q2',lsq_q2
!    WRITE(51,*)'lsq_q1',lsq_q1,'lsq_q2',lsq_q2

    ACCEPTED = .TRUE.
    do i=1,omega
!       WRITE(*,*)'t = ',t 
!       WRITE(*,*)'w = ',w 
!       WRITE(51,*)'t = ',t 
!       WRITE(51,*)'w = ',w 
       IF(ABS(t(i)).GT.2000.0E0_realk)ACCEPTED=.FALSE.
       IF(w(i).LT.1.0E-20_realk)ACCEPTED=.FALSE.
    enddo
    IF(ACCEPTED)THEN
!       WRITE(*,*)'ACCEPTED'       
!       WRITE(51,*)'ACCEPTED'       
       w_out(1:omega) =  w(1:omega)
       t_out(1:omega) =  t(1:omega)
    ELSE
       call lsquit('Error Laplace quadrature grid',-1)       
    ENDIF
  end subroutine get_laplace_quadrature_points

  subroutine buildX(N_occ,N_virt,N_tot,eps,x)
    implicit none
    integer,intent(in) :: N_occ,N_virt,N_tot
    real(realk),intent(in) :: eps(N_tot)
    real(realk),intent(inout) :: x(N_virt,N_virt,N_occ,N_occ)
    !local variables
    integer :: ix,jx,ax,bx
    real(realk) :: eps_aij
    !$OMP PARALLEL DO COLLAPSE(3) DEFAULT(NONE) PRIVATE(ix,jx,ax,eps_aij,bx) SHARED(N_occ,N_virt,eps,x)
    do ix = 1, N_occ
       do jx = 1, N_occ
          do ax = 1, N_virt
             eps_aij = (eps(N_occ+ax) - eps(ix) - eps(jx))
             do bx = 1, N_virt
                x(bx,ax,jx,ix) = eps_aij + eps(N_occ+bx)
             end do
          end do
       end do
    end do
    !$OMP END PARALLEL DO
  end subroutine buildX

  subroutine buildXRed(N_occ,N_virt,N_tot,eps,x,xsize,xsize3)
    implicit none
    integer,intent(in) :: N_occ,N_virt,N_tot,xsize
    real(realk),intent(in) :: eps(N_tot)
    real(realk),intent(inout) :: x(xsize)
    integer,intent(inout) :: xsize3
    !local variables
    integer :: ix,ax
    !HOMO and all occ 
    do ix = 1, N_occ
       x(ix) = 2.0E0_realk*eps(N_occ+1) - 2.0E0_realk*eps(ix)
    enddo
    !LUMO and all virt 
    do ax = 2, N_virt
       x(ax+N_occ-1) = 2.0E0_realk*eps(N_occ+ax) - 2.0E0_realk*eps(N_occ)
    enddo
    xsize3 = N_occ + N_virt-1
  end subroutine buildXRed

  ! ============================================================================ !
  ! exponents
  ! ============================================================================ !
  subroutine exponents(t, omega, xmin, xmax)
    implicit none

    integer, intent(in) :: omega
    real(realk), intent(in) :: xmin
    real(realk), intent(in) :: xmax
    real(realk), intent(inout) :: t(omega)

    integer :: ix
    integer :: step(omega)
    real(realk) :: tmin, tmax, lt0, ltn, ltincr

    do ix = 1, omega
       step(ix) = ix-1
    end do

    tmin = 1.0E0_realk/xmax
    tmax = 1.0E0_realk/xmin
    lt0 = -log(xmin)
    ltn = -log(xmax)
    ltincr = (lt0 - ltn)/(omega - 1.0E0_realk - (omega/3))
    t(:) = exp(ltn + (step(:))*ltincr)
  end subroutine exponents

  ! ========================================================================== !
  ! Iterative optimization of w and t
  ! ========================================================================== !
  subroutine alternate(w_out, t_out, omega, iterations, xmin, xmax, x, xsize,InitialGuessOption)
    implicit none

    integer, intent(in) :: omega
    integer, intent(in) :: xsize
    integer, intent(inout) :: iterations,InitialGuessOption
    real(realk), intent(in) :: xmin, xmax
    real(realk), intent(in) :: x(xsize)
    real(realk), intent(inout) :: w_out(omega)
    real(realk), intent(inout) :: t_out(omega)

    integer :: itr, s, ix, jx, ax, bx, alpha,i
    integer, parameter :: maxitr = 512
    real(realk) :: w_err, t_err, q1_old, q2_old, q1, q2
    real(realk), parameter :: tolerance = 1.0e-008_realk
    real(realk), parameter :: hard_tolerance = 1.0e-012_realk
    real(realk), parameter :: minsum_w = 1.0e-5_realk
    real(realk) :: w_new(omega), t_new(omega), w_old(omega), t_old(omega), w_storage(omega), t_storage(omega)
    logical :: kosher_p, converged_p,ACCEPTED

!    call exponents(t_out, omega, xmin, xmax)
!    call get_w0(w_out, t_out, omega, x, xsize)
!    call q_error(q1_old, q2_old, w_old, t_old, omega, x, xsize)
    IF(InitialGuessOption.EQ.1)THEN
       call exponents(t_old, omega, xmin, xmax)
       call get_w0(w_old, t_old, omega, x, xsize)
       call q_error(q1_old, q2_old, w_old, t_old, omega, x, xsize)
!       WRITE(*,*)'INITIAL q = ',q1_old, q2_old
!       WRITE(*,*)'INITIAL t = ',t_old
!       WRITE(*,*)'INITIAL w = ',w_old 
!       WRITE(51,*)'INITIAL q = ',q1_old, q2_old
!       WRITE(51,*)'INITIAL t = ',t_old 
!       WRITE(51,*)'INITIAL w = ',w_old 
       ACCEPTED=.TRUE.
       do i=1,omega
          IF(ABS(t_old(i)).GT.2000.0E0_realk)ACCEPTED=.FALSE.
          IF(w_old(i).LT.1.0E-20_realk)ACCEPTED=.FALSE.
       enddo
       IF(.NOT.ACCEPTED)InitialGuessOption = 2
    ENDIF

    IF(InitialGuessOption.EQ.2)THEN
       call get_minimax_fixed_laplace_quadrature_points(xmin,xmax, omega, w_old, t_old)
       call q_error(q1_old, q2_old, w_old, t_old, omega, x, xsize)

!       WRITE(*,*)'INITIAL2 q = ',q1_old, q2_old
!       WRITE(*,*)'INITIAL2 t = ',t_old
!       WRITE(*,*)'INITIAL2 w = ',w_old 
!       WRITE(51,*)'INITIAL2 q = ',q1_old, q2_old
!       WRITE(51,*)'INITIAL2 t = ',t_old 
!       WRITE(51,*)'INITIAL2 w = ',w_old 
       ACCEPTED=.TRUE.
       do i=1,omega
          IF(ABS(t_old(i)).GT.2000.0E0_realk)ACCEPTED=.FALSE.
          IF(w_old(i).LT.1.0E-20_realk)ACCEPTED=.FALSE.
       enddo
       IF(.NOT.ACCEPTED)THEN
          call lsquit('Error no good starting guess',-1)
       ENDIF
    ENDIF
    w_out(:) = w_old(:)
    t_out(:) = t_old(:)
    w_storage(:) = w_old(:)
    t_storage(:) = t_old(:)
    ! write (*,*) 'w0', w_storage
    ! write (*,*) 't0', t_storage
    ! write (*,*) 'q0', q1_old, q2_old
    if (q1_old > hard_tolerance) then
       itr = 1
       converged_p = .false.
       kosher_p = .true.
       do while (itr <= maxitr .and. kosher_p .and. .not. converged_p)
          ! w_old(:) = 1.
          ! t_old(:) = 1.
          call lmqt(w_old, t_new, t_old, omega, x, xsize)
          call lmqw(w_new, w_old, t_new, omega, x, xsize)
          ! call get_w0(w_new, t_new, omega, x, xsize)
          call q_error(q1, q2, w_new, t_new, omega, x, xsize)
          ! write (*,*) 'itr', itr
          ! write (*,*) 'w', w_new
          ! write (*,*) 't', t_new
          ! write (*,*) 'q', q1, q2
          ! stop
          do alpha = 1, omega
             if (w_new(alpha) /= w_new(alpha) .or. t_new(alpha) /= t_new(alpha)) then
                kosher_p = .false.
             end if
          end do
          if (kosher_p) then
             w_err = sum((w_old(:) - w_new(:))**2)/omega
             t_err = sum((t_old(:) - t_new(:))**2)/omega
             if (sqrt(sum(w_new(:)**2))/omega < minsum_w) then
                kosher_p = .false.
             else if ((w_err < tolerance) .and. (t_err < tolerance)) then
                ! write (*,*) 'alternate converged'
                converged_p = .true.
             else
                itr = itr + 1
             end if
             w_old(:) = w_new(:)
             t_old(:) = t_new(:)
          else
             ! write (*,*) 'alternatre not kosher'
          end if
       end do
       iterations = itr
       if (.not. kosher_p) then
          w_out(:) = w_storage(:)
          t_out(:) = t_storage(:)
       else if (converged_p) then
          if ((q1 < q1_old) .or. (q2 < q2_old)) then
             w_out(:) = w_old(:)
             t_out(:) = t_old(:)
          else
             w_out(:) = w_storage(:)
             t_out(:) = t_storage(:)
          end if
       else
          if ((q1 < q1_old) .or. (q2 < q2_old)) then
             w_out(:) = w_old(:)
             t_out(:) = t_old(:)
          else
             w_out(:) = w_storage(:)
             t_out(:) = t_storage(:)
          end if
       end if
    else
       w_out(:) = w_storage(:)
       t_out(:) = t_storage(:)
       iterations = 0
    end if
  end subroutine alternate


  ! ========================================================================== !
  ! Get w0
  ! ========================================================================== !
  subroutine get_w0(w, t, omega, x, xsize)
    implicit none

    ! This subroutine calculates first approximations to weigths.
    ! It does not require the existence of previous weigths.
    ! seems legit

    integer, intent(in) :: omega
    integer, intent(in) :: xsize
    real(realk), intent(in) :: t(omega)
    real(realk), intent(in) :: x(xsize)
    real(realk), intent(inout) :: w(omega)

    integer :: k, l, m, info0, info1, s,ix
    real(realk) :: x0,tmpg
    real(realk),pointer :: g(:) !xsize
    real(realk) :: work(5*omega)
    real(realk) :: gradient(omega), eigvals(omega), tmp(omega)
    real(realk) :: hessian(omega, omega), U(omega, omega)
    real(realk), parameter :: epsilon = 1.0e-006_realk
    logical :: kosher_input_p
    call mem_alloc(g,xsize,'LT:g')
    kosher_input_p = .true.

    do k = 1, omega
       if (t(k) /= t(k)) then
          kosher_input_p = .false.
       end if
    end do

    if (kosher_input_p) then
       work(:) = 0.0E0_realk
       g(:) = x(:)**2

       do l = 1, omega     
          !gradient(l) = 2.0E0_realk*sum(g(:)*exp(-t(l)*x(:))/x(:))
          TMPg = 0.0E0_realk
          !$OMP PARALLEL DO DEFAULT(none) PRIVATE(ix) SHARED(xsize,&
          !$OMP g,t,x,l) REDUCTION(+:TMPg)
          do ix = 1,xsize
             TMPg = TMPg + g(ix)*exp(-t(l)*x(ix))/x(ix)
          enddo
          !$OMP END PARALLEL DO
          gradient(l) = 2.0E0_realk*TMPg
          eigvals(l) = 0.0E0_realk
          do m = l, omega
             !hessian(l, m) = 2.0E0_realk*sum(g(:)*exp(-x*(t(l) + t(m))))
             TMPg = 0.0E0_realk
             !$OMP PARALLEL DO DEFAULT(none) PRIVATE(ix) SHARED(xsize,&
             !$OMP g,t,x,l,m) REDUCTION(+:TMPg)
             do ix = 1,xsize
                TMPg = TMPg + g(ix)*exp(-x(ix)*(t(l) + t(m)))
             enddo
             !$OMP END PARALLEL DO
             hessian(l, m) = 2.0E0_realk*TMPg
             hessian(m, l) = hessian(l, m)
          end do
       end do
       info0 = 0
       info1 = 5*omega
       U(:, :) = Hessian(:, :)

       call dsyev('v', 'u', omega, U, omega, eigvals, work, info1, info0)
       tmp = matmul(-gradient, U)
       do l = 1, omega
          x0 = eigvals(l)
          if (abs(x0) > epsilon) then
             x0 = 1.0E0_realk/x0
          else
             x0 = 0.0E0_realk
          end if
          tmp(l) = tmp(l)*x0
       end do

       w = -matmul(U, tmp)
    end if
    call mem_dealloc(g)
  end subroutine get_w0

  ! ========================================================================== !
  ! Get w for a given set of t by means of Levenberg-Marquart
  ! ========================================================================== !
  subroutine lmqw(w_out, w_in, t, omega, x, xsize)
    implicit none

    integer, intent(in) :: omega
    integer, intent(in) :: xsize
    real(realk), intent(in) :: w_in(omega)
    real(realk), intent(in) :: t(omega)
    real(realk), intent(in) :: x(xsize)
    real(realk), intent(inout) :: w_out(omega)

    integer :: k, l, m, s, info0, info1, itr
    real(realk) :: x0, q1m, q2m, q1p, q2p, q1_prev, q2_prev, j_ks, j_ls, lambda
    real(realk) :: work(5*omega)
    real(realk) :: eigvals(omega), k1(omega), k2(omega)
    real(realk) :: w(omega), delta(omega), w_tmp_p(omega), w_tmp_m(omega), b(omega)
    real(realk) :: U(omega, omega), eig_inv(omega, omega)
    real(realk) :: A(omega, omega), A_p(omega, omega)
    real(realk), parameter :: tolerance = 1.0e-008_realk
    real(realk), parameter :: lambda_tolerance = 1.0e-012_realk
    integer, parameter :: maxitr = 512
    logical :: converged_p, kosher_input_p
    integer :: rejections

    kosher_input_p = .true.
    w(:) = w_in(:)
    itr = 1
    converged_p = .false.
    lambda = 1.0E0_realk/(2.0E0_realk**10)
    rejections = 0
    b(:) = 0.0E0_realk
    A(:, :) = 0.0E0_realk
    call q_error(q1_prev, q2_prev, w, t, omega, x, xsize)
    do while ((.not. converged_p) .and. (itr < maxitr) .and. (kosher_input_p))
       do k = 1, omega
          if (w(k) /= w(k)) then
             kosher_input_p = .false.
          end if
       end do

       if (kosher_input_p) then
          !$OMP PARALLEL DO DEFAULT(none) PRIVATE(s,j_ks,k,l,j_ls) SHARED(xsize,&
          !$OMP omega,t,x,w) REDUCTION(+:A,b)
          do s = 1, xsize
             do k = 1, omega
                j_ks = exp(-t(k)*x(s))
                b(k) = b(k) + j_ks*(1.0E0_realk/x(s) - sum(w(:)*exp(-t(:)*x(s))))
                do l = k, omega
                   j_ls = exp(-t(l)*x(s))
                   A(k, l) = A(k, l) + j_ks*j_ls
                   A(l, k) = A(k, l)
                end do
             end do
          end do
          !$OMP END PARALLEL DO
          A_p(:, :) = A(:, :)
          do k = 1, omega
             A_p(k, k) = A_p(k, k) + lambda*A_p(k, k)
          end do

          info0 = 0
          info1 = 5*omega
          U(:, :) = A_p(:, :)

          call dsyev('v', 'u', omega, U, omega, eigvals, work, info1, info0)
          k1 = matmul(b, U)
          eig_inv(:, :) = 0.0E0_realk
          do k = 1, omega
             x0 = eigvals(k)
             if (abs(x0) > tolerance) then
                x0 = 1.0E0_realk/x0
             else
                x0 = 0.0E0_realk
             end if
             eig_inv(k, k) = x0
          end do
          k2(:) = 0.0E0_realk
          k2 = matmul(k1, eig_inv)
          delta(:) = 0.0E0_realk
          delta = matmul(U, k2)
          ! if (sum(delta(:)**2) < tolerance) then
          !    converged_p = .true.
          ! end if

          w_tmp_p(:) = w(:) + delta(:)
          w_tmp_m(:) = w(:) - delta(:)

          call q_error(q1m, q2m, w_tmp_m, t, omega, x, xsize)
          call q_error(q1p, q2p, w_tmp_p, t, omega, x, xsize)
          ! if ((q1m < q1_prev) .or. (q2m < q2_prev)) then
          if (q1m < q1_prev) then
             w(:) = w_tmp_m(:)
             q1_prev = q1m
             q2_prev = q2m
             lambda = lambda/2
             if (lambda < lambda_tolerance) then
                ! write (*,*) 'lambda too small'
                converged_p = .true.
                ! or kosher_input_p = .false. ?
             end if
             ! write (*,*) '- accepted w', itr, q1_prev, q2_prev!, lambda, 'delta, t', delta, t
             rejections = 0 
          ! else if ((q1p < q1_prev) .or. (q2p < q2_prev)) then
          else if (q1p < q1_prev) then
             w(:) = w_tmp_p(:)
             q1_prev = q1p
             q2_prev = q2p
             lambda = lambda/2
             if (lambda < lambda_tolerance) then
                ! write (*,*) 'lambda too small'
                converged_p = .true.
                ! or kosher_input_p = .false. ?
             end if
             ! write (*,*) '+ accepted w', itr, q1_prev, q2_prev!, lambda, 'delta, t', delta, t
             rejections = 0 
          else
             lambda = lambda*2
             if (lambda > 1.0E12_realk) then
                ! write (*,*) 'lambda too large'
                ! kosher_input_p = .false.
                converged_p = .true.
             end if
             ! write (*,*) 'rejected  ', itr, q1_prev, q2_prev, lambda, 'delta, t', delta, t
             rejections = rejections + 1
             IF(rejections.GT.10)converged_p = .true.
          end if
          if ((q1_prev < tolerance) .and. (q2_prev < tolerance)) then
             converged_p = .true.
          end if
          ! if ((q1 < q1_prev) .or. (q2 < q2_prev)) then
          !    w(:) = w_tmp(:)
          !    q1_prev = q1
          !    q2_prev = q2
          !    lambda = lambda/2
          !    if (lambda < lambda_tolerance) then
          !       ! write (*,*) 'lambda too small'
          !       converged_p = .true.
          !       ! or kosher_input_p = .false. ?
          !    end if
          ! else
          !    lambda = lambda*2
          !    if (lambda > 1.0E12_realk) then
          !       ! write (*,*) 'lambda too large'
          !       kosher_input_p = .false.
          !    end if
          ! end if
          ! if ((q1 < tolerance) .and. (q2 < tolerance)) then
          !    converged_p = .true.
          ! end if
          ! write (*,*) 'lmqw', itr, q1, q2, lambda, 'delta', delta
          itr = itr + 1
       end if
    end do

    ! Final sanity check
    if (kosher_input_p) then
       w_out(:) = w(:)
    else
       w_out(:) = w_in(:)
    end if
  end subroutine lmqw


  ! subroutine brute_force(w_out, w_in, t_out, t_in, omega, x, xsize)
  !   implicit none

  !   integer, intent(in) :: omega
  !   integer, intent(in) :: xsize
  !   real(realk), intent(in) :: x(xsize)
  !   real(realk), intent(in) :: w_in(omega)
  !   real(realk), intent(in) :: t_in(omega)
  !   real(realk), intent(inout) :: w_out(omega)
  !   real(realk), intent(inout) :: t_out(omega)

  !   real(realk) :: t(omega), w(omega), t_store(omega), w_store(omega), t_try(omega)
  !   integer :: n, k, l, m, a, b, c
  !   real(realk) :: f, g, t_change, q1, q2

  !   ! six digits
  !   w_out(:) = w_in(:)
  !   t_try(:) = t_in(:)
  !   t(:) = t_in(:)
  !   do n = 1, 8
  !      do k = 1, omega
  !         if (n == 1) then
  !            g = 1.0E9_realk
  !            do while (g > t(k)) then
  !               g = g/10.
  !            end do
  !         else
  !            g = f
  !         end if
  !         f = g/10.
  !         t(k) = t(k) - g
  !         do a = 1, 11
  !            t(k) = t(k) + f
  !            call q_error(q1_i, q2_i, w_in, t, omega, x, xsize)
  !         end do
  !      end do
  !   end do

  ! end subroutine brute_force

  ! ========================================================================== !
  ! Get t for a given set of w by means of Levenberg-Marquart
  ! ========================================================================== !
  subroutine lmqt(w, t_out, t_in, omega, x, xsize)
    implicit none

    integer, intent(in) :: omega
    integer, intent(in) :: xsize
    real(realk), intent(in) :: w(omega)
    real(realk), intent(in) :: t_in(omega)
    real(realk), intent(in) :: x(xsize)
    real(realk), intent(inout) :: t_out(omega)

    integer :: k, l, m, s, info0, info1, itr
    real(realk) :: x0, q1, q2, q1p, q2p, q1m, q2m, q1_prev, q2_prev, j_ks, j_ls, lambda
    real(realk) :: work(5*omega)
    real(realk) :: eigvals(omega), k1(omega), k2(omega)
    real(realk) :: t(omega), delta(omega), t_tmp_p(omega), t_tmp_m(omega), b(omega)
    real(realk) :: U(omega, omega), eig_inv(omega, omega)
    real(realk) :: A(omega, omega), A_p(omega, omega)
    real(realk), parameter :: tolerance = 1.0e-008_realk
    real(realk), parameter :: lambda_tolerance = 1.0e-012_realk
    integer, parameter :: maxitr = 512
    logical :: converged_p, kosher_input_p
    integer :: rejections
    kosher_input_p = .true.
    t(:) = t_in(:)
    ! t(:) = 1.                   !REMOVE!
    itr = 1
    converged_p = .false.
    lambda = 1.0E0_realk/(2.0E0_realk**10)
    rejections = 0
    b(:) = 0.0E0_realk
    A(:, :) = 0.0E0_realk
    call q_error(q1_prev, q2_prev, w, t, omega, x, xsize)
    ! write (*,*) ''
    ! write (*,*) 'w0', w
    ! write (*,*) 't0', t
    ! write (*,*) 'q0', q1_prev, q2_prev

    do while ((.not. converged_p) .and. (itr < maxitr) .and. (kosher_input_p))
       do k = 1, omega
          if (t(k) /= t(k)) then
             kosher_input_p = .false.
          end if
       end do

       if (kosher_input_p) then
          !$OMP PARALLEL DO DEFAULT(none) PRIVATE(s,j_ks,k,l,j_ls) SHARED(xsize,&
          !$OMP omega,t,x,w) REDUCTION(+:A,b)
          do s = 1, xsize
             do k = 1, omega
                j_ks = -x(s)*w(k)*exp(-t(k)*x(s))
                b(k) = b(k) + j_ks*(1.0E0_realk/x(s) - sum(w(:)*exp(-t(:)*x(s))))
                do l = k, omega
                   j_ls = -x(s)*w(l)*exp(-t(l)*x(s))
                   A(k, l) = A(k, l) + j_ks*j_ls
                   A(l, k) = A(k, l)
                end do
             end do
          end do
          !$OMP END PARALLEL DO
          A_p(:, :) = A(:, :)
          do k = 1, omega
             A_p(k, k) = A_p(k, k) + lambda*A_p(k, k)
          end do

          info0 = 0
          info1 = 5*omega
          U(:, :) = A_p(:, :)

          call dsyev('v', 'u', omega, U, omega, eigvals, work, info1, info0)
          k1 = matmul(b, U)
          eig_inv(:, :) = 0.0E0_realk
          do k = 1, omega
             x0 = eigvals(k)
             if (abs(x0) > tolerance) then
                x0 = 1.0E0_realk/x0
             else
                x0 = 0.0E0_realk
             end if
             eig_inv(k, k) = x0
          end do
          k2(:) = 0.0E0_realk
          k2 = matmul(k1, eig_inv)
          delta(:) = 0.0E0_realk
          delta = matmul(U, k2)
          ! if (sum(delta(:)**2) < tolerance) then
          !    converged_p = .true.
          ! end if

          t_tmp_m(:) = t(:) - delta(:)
          t_tmp_p(:) = t(:) + delta(:)
          call q_error(q1m, q2m, w, t_tmp_m, omega, x, xsize)
          call q_error(q1p, q2p, w, t_tmp_p, omega, x, xsize)
          ! if ((q1m < q1_prev) .or. (q2m < q2_prev)) then
          if (q1m < q1_prev) then
             t(:) = t_tmp_m(:)
             q1_prev = q1m
             q2_prev = q2m
             lambda = lambda/2
             if (lambda < lambda_tolerance) then
                ! write (*,*) 'lambda too small'
                converged_p = .true.
                ! or kosher_input_p = .false. ?
             end if
             ! write (*,*) '- accepted t', itr, q1_prev, q2_prev!, lambda, 'delta, t', delta, t
             rejections = 0
          ! else if ((q1p < q1_prev) .or. (q2p < q2_prev)) then
          else if (q1p < q1_prev) then
             t(:) = t_tmp_p(:)
             q1_prev = q1p
             q2_prev = q2p
             lambda = lambda/2
             if (lambda < lambda_tolerance) then
                ! write (*,*) 'lambda too small'
                converged_p = .true.
                ! or kosher_input_p = .false. ?
             end if
             ! write (*,*) '+ accepted t', itr, q1_prev, q2_prev!, lambda, 'delta, t', delta, t
             rejections = 0
          else
             lambda = lambda*2
             if (lambda > 1.0E12_realk) then
                ! write (*,*) 'lambda too large'
                ! kosher_input_p = .false.
                converged_p = .true.
             end if
             ! write (*,*) 'rejected  ', itr, q1_prev, q2_prev, lambda, 'delta, t', delta, t
             rejections = rejections + 1
             IF(rejections.GT.10)converged_p = .true.
          end if
          if ((q1_prev < tolerance) .and. (q2_prev < tolerance)) then
             converged_p = .true.
          end if
          itr = itr + 1
       end if
    end do

    ! Final sanity check
    if (kosher_input_p) then
       t_out(:) = t(:)
    else
       t_out(:) = t_in(:)
    end if
  end subroutine lmqt

!   ! ========================================================================== !
!   ! Get t for a given set of w
!   ! ========================================================================== !
!   subroutine get_t_given_w(w, t_out, t_in, omega, x, xsize)
!     implicit none

!     integer, intent(in) :: omega
!     integer, intent(in) :: xsize
!     real(realk), intent(in) :: w(omega)
!     real(realk), intent(in) :: t_in(omega)
!     real(realk), intent(in) :: x(xsize)
!     real(realk), intent(inout) :: t_out(omega)

!     integer :: k, l, m, info0, info1, itr
!     real(realk) :: h_lm, x0, q1, q2, q1_prev, q2_prev
!     real(realk) :: work(5*omega)
!     real(realk),pointer :: diff_y_fx(:), g(:)
!     real(realk) :: gradient(omega), eigvals(omega), k1(omega), k2(omega)
!     real(realk) :: t(omega), delta(omega), t_tmp(omega)
!     real(realk) :: hessian(omega, omega), U(omega, omega), eig_inv(omega, omega)
!     real(realk), parameter :: tolerance = 1.0e-008_realk
!     real(realk), parameter :: tolerance2 = 1.0e-004_realk
!     integer, parameter :: maxitr = 512
!     logical :: converged_p, kosher_input_p
!     call mem_alloc(diff_y_fx,xsize,'LT:diff_y_fx')
!     call mem_alloc(g,xsize,'LT:g2')
!     kosher_input_p = .true.
!     t(:) = t_in(:)
!     itr = 1
!     converged_p = .false.

! !    call q_error(q1_prev, q2_prev, w, t_in, omega, x, xsize)
!     q1_prev = 2.0E9_realk
!     q2_prev = 2.0E9_realk

!     do while ((.not. converged_p) .and. (itr < maxitr) .and. (kosher_input_p))
!        do k = 1, omega
!           if (t(k) /= t(k)) then
!              kosher_input_p = .false.
!           end if
!        end do

!        do k = 1, xsize
!           diff_y_fx(k) = 1.0E0_realk/x(k)
!        enddo
!        do k = 1, omega
!           do l = 1, xsize
!              diff_y_fx(l) = diff_y_fx(l) - w(k)*exp(-t(k)*x(l))
!           enddo
!        end do
!        do l = 1, omega
!           ! g(xs) may be: 1 or xs^2

!           ! if g(xs) = 1 then
!           ! gradient(l) = -2.0E0_realk*sum((x(:)*w(l)*exp(-t(l)*x(:)))*diff_y_fx)

!           ! else if g(xs) = xs**2 then
!           gradient(l) = -2.0E0_realk*sum((x(:)**3*w(l)*exp(-t(l)*x(:)))*diff_y_fx(:))
!           if (gradient(l) /= gradient(l)) then
!              kosher_input_p = .false.
!           end if
!           do m = l, omega
!              ! g(xs) = 1 or xs**2 here as well
!              ! (The latter definition is active her too)
!              if (l == m) then
!                 ! h_lm = 2.0E0_realk*sum(x(:)**2*(w(l)**2*exp(-2*(t(l))*x(:)) + w(l)*exp(-t(l)*x(:))*diff_y_fx(:)))
!                 h_lm = 2.0E0_realk*sum(x(:)**4*(w(l)**2*exp(-2*(t(l))*x(:)) + w(l)*exp(-t(l)*x(:))*diff_y_fx(:)))
!              else
!                 ! h_lm = 2.0E0_realk*sum(x(:)**2*(w(l)*w(m)*exp(-(t(l) + t(m))*x(:))))
!                 h_lm = 2.0E0_realk*sum(x(:)**4*(w(l)*w(m)*exp(-(t(l) + t(m))*x(:))))
!              end if
!              if (h_lm /= h_lm) then
!                 kosher_input_p = .false.
!              end if
!              hessian(l, m) = h_lm
!              hessian(m, l) = h_lm
!           end do
!        end do
!        if (kosher_input_p) then
!           info0 = 0
!           info1 = 5*omega
!           U(:, :) = Hessian(:, :)

!           call dsyev('v', 'u', omega, U, omega, eigvals, work, info1, info0)
!           k1 = matmul(gradient, U)
!           eig_inv(:, :) = 0.0E0_realk

!           do k = 1, omega
!              x0 = eigvals(k)
!              if (abs(x0) > tolerance) then
!                 x0 = 1.0E0_realk/x0
!              else
!                 x0 = 0.0E0_realk
!              end if
!              eig_inv(k, k) = x0
!           end do
!           k2(:) = 0.0E0_realk
!           k2 = matmul(k1, eig_inv)
!           delta(:) = 0.0E0_realk
!           delta = matmul(U, k2)
!           if (sum(delta(:)**2) < tolerance2) then
!              converged_p = .true.
!           end if

!           t_tmp(:) = t(:) + delta(:)
!           call q_error(q1, q2, w, t_tmp, omega, x, xsize)
!           if ((q1 <= q1_prev) .and. (q2 <= q2_prev)) then
!              t(:) = t_tmp(:)
!           else
!              converged_p = .true.
!           end if
!           itr = itr + 1
!        else
!           t_out(:) = t_in(:)
!        end if
!     end do

!     ! Final sanity check
!     if (kosher_input_p) then
!        t_out(:) = t(:)
!     else
!        t_out(:) = t_in(:)
!     end if
!     call mem_dealloc(diff_y_fx)
!     call mem_dealloc(g)

!   end subroutine get_t_given_w



  ! ============================================================================ !
  ! q_error
  ! ============================================================================ !
  subroutine q_error(lsq_q1, lsq_q2, w, t, omega, x, xsize)
    implicit none

    real(realk), intent(inout) :: lsq_q1, lsq_q2
    integer, intent(in) :: omega
    integer, intent(in) :: xsize
    real(realk), intent(in) :: t(omega)
    real(realk), intent(in) :: w(omega)
    real(realk), intent(in) :: x(xsize)
    real(realk) :: q
    integer :: s

    lsq_q1 = 0.0E0_realk
    lsq_q2 = 0.0E0_realk
    !$OMP PARALLEL DO DEFAULT(none) PRIVATE(s,q) SHARED(xsize,&
    !$OMP w,t,x) REDUCTION(+:lsq_q1,lsq_q2)
    do s = 1, xsize
       q = (sum(w(:)*exp(-t(:)*x(s))) - 1.0E0_realk/x(s))**2
       lsq_q1 = lsq_q1 + q
       lsq_q2 = lsq_q2 + q*x(s)**2
    end do
    !$OMP END PARALLEL DO
    lsq_q1 = sqrt(lsq_q1/xsize)
    lsq_q2 = sqrt(lsq_q2/xsize)

  end subroutine q_error

  !> \brief wrapper routine to Remez, JCP 129, 044112 (2008)]
  !> 
  !> The Remez (Minimax quadrature) algorithm was courteously provided by 
  !> Seiichiro Ten-no. This algorithm was published in 
  !> "Minimax approximation for the decomposition of energy denominators in 
  !> Laplace transformed Møller–Plesset perturbation theories" 
  !> J.Chem.Phys., 129, 044112 (2008)
  !> Author of the Minimax quadrature algorithm: Akio Takatsuka (2007)     
  !> 
  !> This wrapper yield optimal quadrature points for a given Fixed number 
  !> of quadrature points
  !> 
  !> \author Thomas Kjaergaard
  !> \date 2016
  !> \param Emin 
  !> \param Emax
  !> \param omega The desired number of laplace quadrature point
  !> \param w_out The laplace quadrature weights
  !> \param t_out The laplace quadrature point exponents
  subroutine get_minimax_fixed_laplace_quadrature_points(Emin,Emax, omega, w_out, t_out)
    implicit none
    real(realk), intent(in) :: Emin,Emax
    integer, intent(in) :: omega
    real(realk), intent(inout) :: t_out(omega), w_out(omega)
    !local variables
    real(realk) ::  Coeff(40)
    character(len=8) :: Demand     
    logical :: Inf
    integer :: K_Lap,idx,i
    Demand = ' '
    Inf = .FALSE.
    K_Lap = omega ! The number of quadrature points (K_Lap=1,...,20)
    call Remez(K_Lap,EMin,EMax,Coeff,Demand,Inf)

    !Laplace-transformed MP2 energies are calculated using 
    !the scaled parameters 
    !weightBar = weight/Emin 
    !amplitudeBar = amplitude/Emin

!    CALL DSCAL(2*K_Lap,EMin,Coeff,1)

    DO I=1,K_Lap
       Idx = 2*I-1
       w_out(I) = Coeff(Idx)
       t_out(I) = Coeff(Idx+1)
    ENDDO
!    WRITE(*,*)'MINIA: t = ',t_out 
!    WRITE(*,*)'MINIA: w = ',w_out 
!    WRITE(51,*)'MINIA: t = ',t_out 
!    WRITE(51,*)'MINIA: w = ',w_out 

  end subroutine get_minimax_fixed_laplace_quadrature_points

  !> \brief wrapper routine to Remez, JCP 129, 044112 (2008)]
  !> 
  !> The Remez (Minimax quardature) algorithm was courteously provided by 
  !> Seiichiro Ten-no. This algorithm was published in 
  !> The Minimax quardature was introduced into Laplace transformed   *
  !> electronic structure by Takatsuka, Ten-no, and Hackbush.         *
  !> "Minimax approximation for the decomposition of energy denominators in 
  !> Laplace transformed Møller–Plesset perturbation theories" 
  !> J.Chem.Phys., 129, 044112 (2008)
  !> Author of the Minimax quardature algorithm: Akio Takatsuka (2007)     
  !> 
  !> This wrapper yield a number of quadrature points automatically determined 
  !> based on requested accuracy 
  !> 
  !> \author Thomas Kjaergaard
  !> \date 2016
  !> \param Emin 
  !> \param Emax
  !> \param omega The desired number of laplace quadrature point
  !> \param w_out The laplace quadrature weights
  !> \param t_out The laplace quadrature point exponents
  subroutine get_minimax_laplace_quadrature_points(Emin,Emax,omega,w_out,t_out,Thresh)
    implicit none
    real(realk), intent(in) :: Emin,Emax,Thresh
    integer, intent(inout) :: omega
    real(realk), intent(inout) :: t_out(20), w_out(20)
    !local variables
    real(realk) ::  Coeff(40)
    character(len=8) :: Demand 
    integer :: K_Lap,i,idx
    logical :: Inf
    Inf = .FALSE.
    K_Lap = 0 !K_Lap is automatically determined based on
              !requested accuracy 

    !Demand : Select the accuracy which you want
    !        = MILLI ... 10E-03 accuracy (K_Lap = 2-)
    !        = MICRO ... 10E-06 accuracy (K_Lap = 4-)
    !        = NANO  ... 10E-09 accuracy (K_Lap = 7-)
    !        = PICO  ... 10E-12 accuracy (K_Lap =11-)
    Demand = 'MILLI'
    IF(Thresh.LE.10E-03_realk)Demand = 'MICRO'
    IF(Thresh.LE.10E-06_realk)Demand = 'NANO'
    IF(Thresh.LE.10E-09_realk)Demand = 'PICO'
    
    call Remez(K_Lap,EMin,EMax,Coeff,Demand,Inf)
    omega = K_Lap

    !Laplace-transformed MP2 energies are calculated using 
    !the scaled parameters 
    !weightBar = weight/Emin 
    !amplitudeBar = amplitude/Emin

!    CALL DSCAL(2*K_Lap,EMin,Coeff,1)

    DO I=1,K_Lap
       Idx = 2*I-1
       w_out(I) = Coeff(Idx)
       t_out(I) = Coeff(Idx+1)
    ENDDO
    DO I=K_Lap+1,20
       t_out(I) = 0.0E0_realk
       w_out(I) = 0.0E0_realk
    ENDDO
!    WRITE(*,*)'MINIB: t = ',t_out 
!    WRITE(*,*)'MINIB: w = ',w_out 
!    WRITE(51,*)'MINIB: t = ',t_out 
!    WRITE(51,*)'MINIB: w = ',w_out 
    
  end subroutine get_minimax_laplace_quadrature_points

end MODULE LAPLACETRANSFORMMOD
