module wannier_localisation
  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use molecule_type, only: &
       copy_molecule, &
       free_moleculeinfo, &
       init_moleculeinfo
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use typedef, only: &
       typedef_setmolecules
  use wlattice_domains, only: ltdom_init, &
       ltdom_free, &
       lattice_domains_type, &
       ltdom_getdomain
  use lattice_storage
  use matrix_module, only: &
       matrix
  use timings_module, only: &
       timings_start, &
       timings_stop, &
       timings_type
  use wannier_utils, only: &
       wu_indxtolatvec, &
       wcopy_atompositions, &
       wtranslate_molecule
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_assign, &
       mat_print, &
       mat_tr
  use configurationType
  use davidson_settings
  use decompMod, only: orbspread_data
  use orbspread_utilMod
  use optimlocMOD
  use integralinterfacemod, only: ii_get_carmom

  implicit none
  
contains
  
  !> @brief Localize the orbitals within the space spanned by the virtual and occupied orbitals 
  !> separately. The orbitals will be reorganized in the mo - matrix after the subroutine is finalized.
  !
  ! note:
  ! - orbloc_info%m(1) = power of variance for occupied
  !   orbloc_info%m(2) = power of variance for virtual 
  ! - if you want to use carmoms in local MO basis for something,
  !   you will have to do that before you call orbspread_free which
  !   dealloc. the cartesian moment tensors are stored as orbloc_info%orbspread_inp%r(1:3)
  !   (x, y, z )and orbloc_info%orbspread_inp%q (x^2 + y^2 + z^2).
  subroutine w_localisation( &
       & mos, nbast, nocc, daltoninp, wconf, lsset, orbloc_info, timings, lupri, luerr, print_info)
    type(lmatrixp),intent(inout)        :: mos
    type(wannier_config), intent(inout) :: wconf
    type(RedSpaceItem)                  :: orbloc_info
    type(daltoninput), intent(inout)    :: daltoninp
    type(lssetting), intent(inout)      :: lsset
    type(timings_type), intent(inout)   :: timings
    integer, intent(in)                 :: lupri, luerr, nbast, nocc
    logical, intent(in), optional       :: print_info
    
    integer :: ncell, i, ndim, nvirt
    type(matrix) :: fullmos
    real(realk), pointer :: x(:), y(:), z(:)
    real(realk) :: xtot, ytot, ztot
    logical do_print

    if (present(print_info)) then
       do_print=print_info
    else
       do_print=.true.
    endif

    nvirt = mos%ncol-nocc
    ncell = 1
    do i=1, mos%periodicdims
       ncell = ncell * (2*mos%cutoffs(i)+1)
    enddo
    ndim = ncell * nbast
    call mat_init(fullmos, ndim, mos%ncol)
    call lts_tofull_mos(mos, fullmos, maxval(mos%cutoffs))

    write(lupri,'(/,a)') '********** Localisation of occupied and virtual Wannier orbitals **********'
    write(lupri,'(a)') 'Localisation functional: powers of the second central moment'
    write(lupri,'(a,i5,a,i5,a)') 'Total number of basis functions:',ndim,' (per unit cell:',nbast,')'
    write(lupri,'(a,i5,3x,a,i5)') 'Number of occupied orbitals:',nocc,'Number of virtual orbitals:',nvirt
    if (orbloc_info%m(1)==1) then
       write(lupri,'(a,i3,a)') 'Power for occupied space, m=',orbloc_info%m(1),' (Boys localisation)'
    else
       write(lupri,'(a,i3)')   'Power for occupied space, m=',orbloc_info%m(1)
    endif
    if (orbloc_info%m(2)==1) then
       write(lupri,'(a,i3,a)') 'Power for virtual space,  m=',orbloc_info%m(1),' (Boys localisation)'
    else
       write(lupri,'(a,i3)')   'Power for virtual space,  m=',orbloc_info%m(1)
    endif

    if (do_print) write(lupri,'(a)') '********** Localisation of occupied Wannier orbitals **********'
    call timings_start(timings, 'Localization: occupied')
    call orbspread_init(orbloc_info%orbspread_inp, orbloc_info%m(1), nocc)
    call w_calc_cartesian_moments( &
         & mos, 1, nocc, daltoninp%molecule, wconf%blat, lsset, &
         & orbloc_info%orbspread_inp, lupri, luerr)
    if (do_print) then
       write(lupri,*) 'Initial occupied orbitals:'
       call mem_alloc(x,nocc)
       call mem_alloc(y,nocc)
       call mem_alloc(z,nocc)
       call mat_extract_diagonal(x,orbloc_info%orbspread_inp%R(1))
       call mat_extract_diagonal(y,orbloc_info%orbspread_inp%R(2))
       call mat_extract_diagonal(z,orbloc_info%orbspread_inp%R(3))
       do i=1,nocc
          write(lupri,'(i3,a,f15.8,a,3f15.8)') &
          i,' Spread=',sqrt(orbloc_info%orbspread_inp%spread2(i)),' Center=',x(i),y(i),z(i)
       enddo
       xtot=mat_tr(orbloc_info%orbspread_inp%R(1))
       ytot=mat_tr(orbloc_info%orbspread_inp%R(2))
       ztot=mat_tr(orbloc_info%orbspread_inp%R(3))
    endif
    call w_localize_section( &
         & ndim, 1, nocc, fullmos, orbloc_info,  orbloc_info%m(1), lupri)
    if (do_print) then
       write(lupri,*) 'Final occupied orbitals:'
       call mat_extract_diagonal(x,orbloc_info%orbspread_inp%R(1))
       call mat_extract_diagonal(y,orbloc_info%orbspread_inp%R(2))
       call mat_extract_diagonal(z,orbloc_info%orbspread_inp%R(3))
       do i=1,nocc
          write(lupri,'(i3,a,f15.8,a,3f15.8)') &
          i,' Spread=',sqrt(orbloc_info%orbspread_inp%spread2(i)),' Center=',x(i),y(i),z(i)
       enddo
       xtot=abs(xtot-mat_tr(orbloc_info%orbspread_inp%R(1)))
       ytot=abs(ytot-mat_tr(orbloc_info%orbspread_inp%R(2)))
       ztot=abs(ztot-mat_tr(orbloc_info%orbspread_inp%R(3)))
       if (xtot>1.0e-8_realk .or. ytot>1.0e-8_realk .or. ztot>1.0e-8_realk) then
          call lsquit('w_localisation: orbital rotation not unitary',lupri)
       endif
       call mem_dealloc(x)
       call mem_dealloc(y)
       call mem_dealloc(z)
    endif
    call orbspread_free(orbloc_info%orbspread_inp) ! w.centers are diag(R(i)), i = 1, 2, 3 
    call timings_stop(timings, 'Localization: occupied')
    
    if (nvirt>0) then
       if (do_print) write(lupri,'(a)') '********** Localisation of virtual Wannier orbitals **********'
       call timings_start(timings, 'Localization: virtual')
       call orbspread_init(orbloc_info%orbspread_inp, orbloc_info%m(2), nvirt)
       call w_calc_cartesian_moments( &
            & mos, nocc + 1, mos%ncol, daltoninp%molecule, wconf%blat, lsset, &
            & orbloc_info%orbspread_inp, lupri, luerr)
       if (do_print) then
          write(lupri,*) 'Initial virtual orbitals:'
          call mem_alloc(x,nvirt)
          call mem_alloc(y,nvirt)
          call mem_alloc(z,nvirt)
          call mat_extract_diagonal(x,orbloc_info%orbspread_inp%R(1))
          call mat_extract_diagonal(y,orbloc_info%orbspread_inp%R(2))
          call mat_extract_diagonal(z,orbloc_info%orbspread_inp%R(3))
          do i=1,nvirt
             write(lupri,'(i3,a,f15.8,a,3f15.8)') &
             i,' Spread=',sqrt(orbloc_info%orbspread_inp%spread2(i)),' Center=',x(i),y(i),z(i)
          enddo
          xtot=mat_tr(orbloc_info%orbspread_inp%R(1))
          ytot=mat_tr(orbloc_info%orbspread_inp%R(2))
          ztot=mat_tr(orbloc_info%orbspread_inp%R(3))
       endif
       call w_localize_section( &
            & ndim, nocc+1, mos%ncol, fullmos, orbloc_info,  orbloc_info%m(2), lupri)
       !call mat_print(fullmos,1,ndim,1,nbast,6)
       if (do_print) then
          write(lupri,*) 'Final virtual orbitals:'
          call mat_extract_diagonal(x,orbloc_info%orbspread_inp%R(1))
          call mat_extract_diagonal(y,orbloc_info%orbspread_inp%R(2))
          call mat_extract_diagonal(z,orbloc_info%orbspread_inp%R(3))
          do i=1,nvirt
             write(lupri,'(i3,a,f15.8,a,3f15.8)') &
             i,' Spread=',sqrt(orbloc_info%orbspread_inp%spread2(i)),' Center=',x(i),y(i),z(i)
          enddo
          xtot=abs(xtot-mat_tr(orbloc_info%orbspread_inp%R(1)))
          ytot=abs(ytot-mat_tr(orbloc_info%orbspread_inp%R(2)))
          ztot=abs(ztot-mat_tr(orbloc_info%orbspread_inp%R(3)))
          if (xtot>1.0e-8_realk .or. ytot>1.0e-8_realk .or. ztot>1.0e-8_realk) then
             call lsquit('w_localisation: orbital rotation not unitary',lupri)
          endif
          call mem_dealloc(x)
          call mem_dealloc(y)
          call mem_dealloc(z)
       endif
       call orbspread_free(orbloc_info%orbspread_inp)
       call timings_stop(timings, 'Localization: virtual')
    endif
       
    call lts_fromfull_mos(fullmos,mos,mos%ncol,mos%nrow) 
    if (.false.) then
       write(luerr,*) 'w_localisation: final fullmos:'
       do i=1,ncell
          write(luerr,*) 'block ',i
          call mat_print(fullmos,nbast*(i-1)+1,nbast*i,1,nocc+nvirt,luerr)
       enddo
       write(luerr,*) 'w_localisation: final mos:'
       call lts_print(mos,luerr)
       call orbspread_init(orbloc_info%orbspread_inp, orbloc_info%m(2), mos%ncol)
       call w_calc_cartesian_moments( &
            & mos, 1, mos%ncol, daltoninp%molecule, wconf%blat, lsset, &
            & orbloc_info%orbspread_inp, lupri, luerr)
       write(lupri,*) 'w_localisation: check: all final orbital spreads'
       call mem_alloc(x,mos%ncol)
       call mem_alloc(y,mos%ncol)
       call mem_alloc(z,mos%ncol)
       call mat_extract_diagonal(x,orbloc_info%orbspread_inp%R(1))
       call mat_extract_diagonal(y,orbloc_info%orbspread_inp%R(2))
       call mat_extract_diagonal(z,orbloc_info%orbspread_inp%R(3))
       do i=1,mos%ncol
          write(lupri,'(i3,a,f15.8,a,3f15.8)') &
          i,' Spread=',sqrt(orbloc_info%orbspread_inp%spread2(i)),' Center=',x(i),y(i),z(i)
       enddo
       call mem_dealloc(x)
       call mem_dealloc(y)
       call mem_dealloc(z)
       call orbspread_free(orbloc_info%orbspread_inp)
    endif
    call mat_free(fullmos)

! debug

    ! Debug : compare with get_wannier_overlap
    ! call w_print_s00(mol_mos, aoint%smat, wconf%blat, nbast, mos)

end subroutine w_localisation

!> @brief Localize within a subspace spanned by the orbitals #cstart to #cend.
! todo Not optimal memorywise
subroutine w_localize_section(nbast, cstart, cend, fullmos, orbloc_info, m, lupri)
  
  type(matrix), intent(inout) :: fullmos
  type(RedSpaceItem) :: orbloc_info
  integer, intent(in) :: cstart, cend, nbast, m, lupri
  
  type(matrix) :: mos_section

  if (nbast /= fullmos%nrow) then
     write(lupri,*) 'w_localize_section: nbast=',nbast,' fullmos%nrow=',fullmos%nrow,' -- must be the same'
     call lsquit('dimension mismatch in w_localize_section',lupri)
  endif

  call mat_init(mos_section, nbast, cend-cstart+1)
  call mat_section(fullmos, 1, nbast, cstart, cend, mos_section) 
  call wannier_localization_drv(mos_section, m, orbloc_info, lupri)
  call mat_insert_section(mos_section, 1, nbast, cstart, cend, fullmos)
  call mat_free(mos_section)
  
end subroutine w_localize_section


  !> @brief Get the mo basis cartesian moments <0p| \hat o |0q> for o = x, y, z, x^2+y^2+z^2.
  !>   O_{lm} = sum_{l,m\in L_{AO}} C_{l,0}^\dagger o_{l,m} C_{m,0},
  !> where 
  !>   [o_{l,m}]_{\mu,\nu} = <l\mu| \hat o |m\nu>,
  !>   [C_{l,0}]_{\mu, \nu} = C_{l\mu,0\nu}
  !>
  !> @param
  !> @param cmofirst start at this cmo p, q \in {cmofirst, cmolast}
  !> @param cmolast stop at this cmo p, q \in {cmofirst, cmolast}
  !> @param 
  !> @param
  !> @param
  !> @param
  !> @param
  !>
  !> todo This s.r. probably needs a little optimizaiton. 
  !> Eg:
  !>   - x_{l, m} = x_{0, m-l}
  !>   - lts_get instead of lts_copy
  !>   - make slower but memory efficient version with
  !>     II_get_single_carmom(LUPRI,LUERR,SETTING,carmom,imat,nderiv,X,Y,Z)
  !>   - ...
 subroutine w_calc_cartesian_moments( & 
      & cmo, cmofirst, cmolast, refcell, blat, setting, orbspread_inp, lupri, luerr)

   ! todo are all of these imports are necc??
   
   type(lmatrixp), intent(inout) :: cmo
   type(moleculeinfo), intent(in) :: refcell
   type(bravais_lattice), intent(in) :: blat
   type(lssetting), intent(inout) :: setting
   type(orbspread_data), intent(inout) :: orbspread_inp
   integer, intent(in) :: lupri, luerr, cmofirst, cmolast

   real(realk), pointer :: tmpv(:)
   type(matrix) :: carmom_tmp(10) 
   type(matrix) :: tmp, tmp2, cmo1, cmo2
   type(matrix), pointer :: w
   type(lattice_domains_type) :: dom
   integer :: i1, indx1, i2, indx2, n, m
   logical :: lstat
   type(moleculeinfo), target :: mol1, mol2
   character :: str

   m = cmolast - cmofirst + 1
   n = cmo%nrow

#ifdef assertions
   str = 'mod: wannier_localization, s.r: w_calc_cartesian_moments: &
        & values of cmofirst, cmolast or carmom matrix dimensions.'
   call ls_assert((cmofirst > 0 .and. cmofirst < cmolast), str)
   call ls_assert((cmolast > cmofirst .and. <= mos%ncol), str)
   do i1 = 1, 3
      call ls_assert((orbspread_inp%R(i1)%nrow == m), str)
      call ls_assert((orbspread_inp%R(i1)%col == m), str)
   enddo
   call ls_assert((orbspread_inp%q%nrow == m), str)
   call ls_assert((orbspread_inp%q%col == m), str)
#endif

   ! init stuff
   do i1 = 1, 10
      call mat_init(carmom_tmp(i1), n, n)
   enddo
   allocate(w)
   call mat_init(w, n, m)
   call mat_init(tmp, m, n)
   call mat_init(tmp2, n, n)
   call mat_init(cmo1, n, m)
   call mat_init(cmo2, n, m)
   call copy_molecule(refcell, mol1, lupri)
   call copy_molecule(refcell, mol2, lupri)
   call ltdom_init(dom, blat%dims, maxval(cmo%cutoffs))
   call ltdom_getdomain(dom, cmo%cutoffs, incl_redundant=.true.)

   ! calculate cartesian moments
   do i1 = 1, 3
      call mat_zero(orbspread_inp%R(i1))
   enddo
   call mat_zero(orbspread_inp%Q)

   do i1 = 1, dom%nelms; indx1 = dom%indcs(i1)  ! loop over AO - basis
      call lts_section(cmo, indx1, lstat, cmo1, 1, n, cmofirst, cmolast, work=w)

      if (.not. lstat) cycle
      call wcopy_atompositions(mol1, refcell)
      call wtranslate_molecule(mol1, wu_indxtolatvec(blat, -indx1))

      do i2 = 1, dom%nelms; indx2 = dom%indcs(i2)

         call lts_section(cmo, indx2, lstat, cmo2, 1, n, cmofirst, cmolast, work=w)
         if (.not. lstat) cycle
         call wcopy_atompositions(mol2, refcell)
         call wtranslate_molecule(mol2, wu_indxtolatvec(blat, -indx2))
         call typedef_setmolecules(setting, mol1, 1, mol2, 2)

         ! get X_12 
         call ii_get_carmom(lupri, luerr, setting, carmom_tmp, 10, 2, &
              & 0.0_realk, 0.0_realk, 0.0_realk)
         ! <0p|x|0q> matrix = carmom(1),  x_12 = carmom_tmp(2)
         call mat_mul(cmo1, carmom_tmp(2), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
         call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, orbspread_inp%R(1))
         ! <0p|y|0q> matrix = carmom(2),  x_12 = carmom_tmp(3)
         call mat_mul(cmo1, carmom_tmp(3), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
         call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, orbspread_inp%R(2))
         ! <0p|z|0q> matrix = carmom(3),  x_12 = carmom_tmp(4)
         call mat_mul(cmo1, carmom_tmp(4), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
         call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, orbspread_inp%R(3))
         ! tmp2 <- x^2 + y^2 + z^2
         call mat_assign(tmp2, carmom_tmp(5))  ! 
         call mat_daxpy(1.0_realk, carmom_tmp(8), tmp2)  !
         call mat_daxpy(1.0_realk, carmom_tmp(10), tmp2)  !
         ! <0p|x^2 + y^2 + z^2|0q> matrix = carmom(1),  x_12 = carmom_tmp(2)
         call mat_mul(cmo1, tmp2, 'T', 'N', 1.0_realk, 0.0_realk, tmp)
         call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, orbspread_inp%Q)
      enddo
   enddo

   do i1 = 1, 10
      call mat_free(carmom_tmp(i1))
   enddo
   call mat_free(w)
   deallocate(w)
   call mat_free(tmp)
   call mat_free(tmp2)
   call mat_free(cmo1)
   call mat_free(cmo2)
   call free_moleculeinfo(mol1)
   call free_moleculeinfo(mol2)
   call ltdom_free(dom)

   ! spread2
   call mem_alloc(tmpv,m)
   call mat_extract_diagonal(orbspread_inp%spread2,orbspread_inp%Q)
   do i1=1,3
      call mat_extract_diagonal(tmpv,orbspread_inp%R(i1))
      tmpv=tmpv**2
      call daxpy(m,-1.0E0_realk,tmpv,1,orbspread_inp%spread2,1)
   enddo
   call mem_dealloc(tmpv)

 end subroutine w_calc_cartesian_moments

  subroutine ls_assert(assertcond, str)
	  logical, intent(in) :: assertcond
	  character, intent(in) :: str

	  if (.not. assertcond) then
		  call lsquit('str', -1)
	  endif
  end subroutine







!
!  TO BE DELETED at a later stage
!     |
!     |
!     V   	
	









  !> @brief Get the mo basis cartesian moments <0p| \hat o |0q> for o = x, y, z, x^2+y^2+z^2.
  !>   O_{lm} = sum_{l,m\in L_{AO}} C_{l,0}^\dagger o_{l,m} C_{m,0},
  !> where 
  !>   [o_{l,m}]_{\mu,\nu} = <l\mu| \hat o |m\nu>,
  !>   [C_{l,0}]_{\mu, \nu} = C_{l\mu,0\nu}
  !>
  !> @param
  !> @param
  !> @param
  !> @param
  !> @param
  !> @param
  !> @param
  !>
  !> todo This s.r. probably needs a little optimizaiton. 
  !> Eg:
  !>   - x_{l, m} = x_{0, m-l}
  !>   - lts_get instead of lts_copy
  !>   - ...
  subroutine w_calc_cartesian_moments_full(& 
       & cmo, refcell, blat, setting, carmom, lupri, luerr)

    type(lmatrixp), intent(inout) :: cmo
    type(moleculeinfo), intent(in) :: refcell
    type(bravais_lattice), intent(in) :: blat
    type(lssetting), intent(inout) :: setting
    type(matrix), intent(inout) :: carmom(4)
    integer :: lupri, luerr
    
    type(matrix) :: carmom_tmp(10), tmp, tmp2, cmo1, cmo2
    type(lattice_domains_type) :: dom
    integer :: i1, indx1, i2, indx2, n, m
    logical :: lstat
    type(moleculeinfo), target :: mol1, mol2

	  call copy_molecule(refcell, mol1, lupri)
	  call copy_molecule(refcell, mol2, lupri)
	  
	  m = cmo%nrow
	  n = cmo%ncol

	  do i1 = 1, 4
		  call mat_init(carmom(i1), m, m)
		  call mat_zero(carmom(i1))
	  enddo

	  do i1 = 1, 10
		  call mat_init(carmom_tmp(i1), m, m)
	  enddo
	  call mat_init(tmp, n, m)
	  call mat_init(tmp2, n, n)
	  call mat_init(cmo1, m, n)
	  call mat_init(cmo2, m, n)
	  
	  call ltdom_init(dom, blat%dims, maxval(cmo%cutoffs))
	  call ltdom_getdomain(dom, cmo%cutoffs, incl_redundant=.true.)

	  do i1 = 1, dom%nelms; indx1 = dom%indcs(i1)
		  call lts_copy(cmo, indx1, lstat, cmo1)
		  if (.not. lstat) cycle
        call wcopy_atompositions(mol1, refcell)
		  call wtranslate_molecule(mol1, wu_indxtolatvec(blat, -indx1))
		  
		  do i2 = 1, dom%nelms; indx2 = dom%indcs(i2)
			  call lts_copy(cmo, indx2, lstat, cmo2)
			  if (.not. lstat) cycle
			  call wcopy_atompositions(mol2, refcell)
			  call wtranslate_molecule(mol2, wu_indxtolatvec(blat, -indx2))
			  ! call typedef_setmolecules(setting, refcell, 1, 3)
			  !call typedef_setmolecules(setting, mol1, 2, mol2, 4)
			  !call typedef_setmolecules(setting, mol1, 1, mol2, 3)
			  call typedef_setmolecules(setting, mol1, 1, mol2, 2)
			  !call typedef_setmolecules(setting, mol1, 1, mol1, 1, mol2, 2, mol2, 2)
			  
			  ! get X_12 
			  call ii_get_carmom(lupri, luerr, setting, carmom_tmp, 10, 2, &
				  & 0.0_realk, 0.0_realk, 0.0_realk)

			  ! <0p|x|0q> matrix = carmom(1),  x_12 = carmom_tmp(2)
			  call mat_mul(cmo1, carmom_tmp(2), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
			  ! test: this line will give the overlap matrix 
			  ! call mat_mul(cmo1, carmom_tmp(1), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
			  call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, carmom(1))
			  ! <0p|y|0q> matrix = carmom(2),  x_12 = carmom_tmp(3)
			  call mat_mul(cmo1, carmom_tmp(3), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
			  call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, carmom(2))
			  ! <0p|z|0q> matrix = carmom(3),  x_12 = carmom_tmp(4)
			  call mat_mul(cmo1, carmom_tmp(4), 'T', 'N', 1.0_realk, 0.0_realk, tmp)
			  call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, carmom(3))
			  ! tmp2 <- x^2 + y^2 + z^2
			  call mat_assign(tmp2, carmom_tmp(5))  ! 
			  call mat_daxpy(1.0_realk, carmom_tmp(8), tmp2)  !
			  call mat_daxpy(1.0_realk, carmom_tmp(10), tmp2)  !
			  ! <0p|x^2 + y^2 + z^2|0q> matrix = carmom(1),  x_12 = carmom_tmp(2)
			  call mat_mul(cmo1, tmp2, 'T', 'N', 1.0_realk, 0.0_realk, tmp)
			  call mat_mul(tmp, cmo2, 'N', 'N', 1.0_realk, 1.0_realk, carmom(4))
		  enddo
	  enddo

	  do i1 = 1, 10
		  call mat_free(carmom_tmp(i1))
	  enddo
	  call mat_free(tmp)
	  call mat_free(tmp2)
	  call mat_free(cmo1)
	  call mat_free(cmo2)
	  call free_moleculeinfo(mol1)
	  call free_moleculeinfo(mol2)
	  call ltdom_free(dom)
  
  end subroutine

 
  ! tmp for debugging. Print the matrix[S_{00}] in the mo basis
  subroutine w_print_s00(mos, smat, blat, nbast)
    
    type(lmatrixp), intent(inout) :: mos, smat
    type(bravais_lattice), intent(in) :: blat
    integer, intent(in) :: nbast
    
    integer :: cutoff
    type(lattice_domains_type) :: dom_d0
    type(lmatrixp) :: smat_wannier
    type(matrix) :: mo, tmp
    logical :: lstat
    
	 call mat_init(tmp, mos%nrow, mos%ncol)

    cutoff = maxval(mos%cutoffs)

    call lts_init(smat_wannier, blat%dims, 2*mos%cutoffs, smat%nrow, smat%ncol, .false.)
    call lts_zero(smat_wannier)

    call mat_init(mo, mos%nrow, mos%ncol)
!    call mat_zero(mo)
    call lts_copy(mos, 0, lstat, mo)
	 if (.not. lstat) then
		 call lsquit('matrix not initiated. mod: wanniermain, sr. w_print_s00.', -1)
	 endif
    
    call ltdom_init(dom_d0, blat%dims,cutoff)
    call ltdom_getdomain(dom_d0, mos%cutoffs, incl_redundant=.true.)
    call get_wannier_overlap(smat_wannier, dom_d0, smat, mo)

    call lts_copy(smat_wannier, 0, lstat, tmp)
	 if (.not. lstat) then
		 call lsquit('matrix not initiated. mod: wanniermain, sr. w_print_s00.',-1)
	 endif
	 write (*, *) 'The overlap matrix in MO basis'
	 call mat_print(tmp, 1, tmp%nrow, 1, tmp%ncol, 6)
  
  end subroutine

  
  subroutine get_wannier_overlap(smat_wannier,dom_d0,smat,MO)

    type(lmatrixp),intent(out) :: smat_wannier
    type(lmatrixp),intent(in) :: smat
    type(lattice_domains_type),intent(in) :: dom_d0
    type(matrix),intent(in) :: MO

    !Local variables
    integer :: l1, l2, indx_l1, indx_l2
    type(matrix) :: SL1L2, tmp, tmp2
    logical :: lstat_SL1L2
    type(matrix), pointer :: ptr

!    write (*,*) 'Cutoffs overlap MO',smat_wannier%cutoffs
    
    loop_l1: do l1=1,dom_d0%nelms
       indx_l1 = dom_d0%indcs(l1)
       loop_l2: do l2=l1,dom_d0%nelms
          indx_l2 = dom_d0%indcs(l2)

          if (abs(indx_l2 - indx_l1) > maxval(smat_wannier%cutoffs)) cycle 
          
          call mat_init(SL1L2,smat%nrow,smat%ncol)
          call mat_zero(SL1L2)
!          write(*,*) 'get_SL1l2',indx_l1, indx_l2
          call lts_copy(Smat,(indx_l2 - indx_l1),lstat_SL1L2,SL1L2)
          if (.not. lstat_SL1L2) cycle

          call mat_init(tmp,MO%nrow,smat%ncol)
          call mat_init(tmp2,smat%ncol,MO%ncol)
          call mat_mul(MO,SL1L2,'T','N',1.0_realk,0.0_realk,tmp)
          call mat_mul(tmp,MO,'N','N',1.0_realk,0.0_realk,tmp2)

          !Fill Smat_Wannier
          
          ptr => lts_get(smat_wannier,indx_l2-indx_l1)
          if (.not. associated(ptr)) then
             call lts_mat_init(smat_wannier,indx_l2-indx_l1)
             ptr => lts_get(smat_wannier,indx_l2-indx_l1)
          endif
          call mat_assign(ptr, tmp2)

          call mat_free(tmp)
          call mat_free(tmp2)
          call mat_free(SL1L2)
       enddo loop_l2
    enddo loop_l1


    !write(*,*) 'Overlap on the Wannier basis'
    !call lts_print(smat_wannier,6)
    
  end subroutine get_wannier_overlap

  
end module wannier_localisation
