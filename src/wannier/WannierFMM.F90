module wannier_fmm

  use precision
  use wannier_utils !, only:
  use wannier_types, only: bravais_lattice
  use matrix_operations, only: &
       mat_init, &
       mat_abs_max_elm, &
       mat_print, &
       mat_zero, &
       mat_free
  use typedef, only: typedef_setmolecules
  use molecule_type, only: copy_molecule, free_moleculeinfo
  use molecule_typetype, only: moleculeinfo
  use integralinterfacemod, only: ii_get_sphmom, ii_carmom_to_shermom
  use typedeftype, only: lssetting
  use wlattice_domains, only: ltdom_init, ltdom_free, ltdom_getlayer, &
       & lattice_domains_type, ltdom_getdomain
  use lattice_storage!, only: lmatrixp, lts_init, lts_free, lts_get, lts_mat
  use matrix_module, only: matrix
  use timings_module
  use memory_handling, only: mem_alloc, mem_dealloc
#ifdef RUN_WANNIER_TESTS
  use wannier_test_module
  use mm_T_worker, only: pbc_restore_T_matrix
#endif
  use wannier_test_module, only: wtest_print_lmatarray_stored

implicit none
private
public :: wcalc_nucmom, wfmm_multipole_expansion, wfmm_form_Tlattice, &
  pbc_mat_redefine_q, pbc_multipl_moment_matorder
  ! todo remode the two last from the list of public functions

contains
	
!> @brief 
!> Modification of s.r. from JR's PBC2 folder
!> @author Johannes rekkedal
!> @author Karl Leikanger
function  wcalc_nucmom(refcell, maxmultmom, timings, lupri) result(nucmom)

	type(moleculeinfo), intent(in) :: refcell
	integer,intent(in) :: lupri
	integer, intent(in) :: maxmultmom
	type(timings_type), intent(inout) :: timings
	real(realk), pointer :: nucmom(:)

	real(realk), pointer :: tmp(:)
	type(matrix), pointer :: carnucmom(:), sphnucmom(:)
	logical :: reset_flag
	real(realk) :: q, pos(3)
	integer :: i, ncarmom, nsphmom, iprint

	write (*, *) 'Calculating nuclear moments...'
	call timings_start(timings, 'Nuclear moments')

	ncarmom = (maxmultmom + 1)*(maxmultmom + 2)*(maxmultmom + 3)/6
	nsphmom = (maxmultmom + 1)**2

	!alloc mem returnvar
	call mem_alloc(nucmom, nsphmom) 
	! Alloc mem for temporary matrices
	call mem_alloc(tmp, ncarmom) 
	call mem_alloc(carnucmom, ncarmom)
	do i = 1, ncarmom
		call mat_init(carnucmom(i), 1, 1)
		call mat_zero(carnucmom(i))
	enddo
	call mem_alloc(sphnucmom, nsphmom)
	do i = 1, nsphmom
		call mat_init(sphnucmom(i), 1, 1)
		call mat_zero(sphnucmom(i))
	enddo

	! Calculate cartesian moments
	reset_flag = .true.
	do i = 1, refcell%natoms
		q = refcell%atom(i)%charge
		pos(1:3) = refcell%atom(i)%center(1:3)
		call calc_pcharge_mom(tmp, q, pos, maxmultmom, reset_flag, &
			& ncarmom, lupri)
		reset_flag = .false.
	enddo
	do i=1, ncarmom
		carnucmom(i)%elms(1) = tmp(i)
	enddo

	! Transform to spherical moments here
	iprint = 2
	call ii_carmom_to_shermom(sphnucmom, carnucmom, nsphmom, ncarmom, &
		& maxmultmom, lupri, iprint)

	! ...
	do i = 1, nsphmom
		nucmom(i) = sphnucmom(i)%elms(1)
	enddo
	call pbc_multipl_moment_order(nucmom, maxmultmom)
	call pbc_redefine_q(nucmom, maxmultmom)

	! Dealloc tmp matrices
	call mem_dealloc(tmp)
	do i = 1, ncarmom
		call mat_free(carnucmom(i))
	enddo
	call mem_dealloc(carnucmom)
	do i = 1, nsphmom
		call mat_free(sphnucmom(i))
	enddo
	call mem_dealloc(sphnucmom)
	
	call timings_stop(timings, 'Nuclear moments')
end function

!> @brief (JohRekkedal:) This subroutine redefines the multipole moments.
!> It is a copy of the FMM code routine mm_renormalise_qlm
!> that has been modified slightly.
!> @author ?
!> @author Johannes Rekkedal
subroutine pbc_redefine_q(qlm, lmax)
	integer, intent(in) :: lmax
	real(realk), intent(inout) :: qlm((1+lmax)**2)

	integer :: l,m,p,pp
	real(realk) :: pref, s

	! prefactor to symmetrize T-matrix
	do l = 0, lmax
		if (mod(l,2) .eq. 1) then
			s = -1.0_realk
		else
			s = 1.0_realk
		end if
		pp = l*(l+1) +1
		do m = -l, -1
			pref = -1.0_realk / (sqrt(2.0_realk * factorial(l-m)*factorial(l+m)))
			p = pp+m
			qlm(p) = pref*qlm(p) * s
		end do
		pref = 1.0_realk / factorial(l)
		p = pp  ! m = 0
		qlm(p) = pref*qlm(p) * s
		do m = 1, l
			pref = ((-1)**m) / sqrt(2.0_realk * factorial(l-m)*factorial(l+m))
			p = pp+M
			qlm(p) = pref*qlm(p) * s
		end do
	end do

contains
	real(realk) function factorial(n)
		integer, intent(in) :: n
		integer :: i
		factorial = 1
		do i = n, 2, -1
			factorial = factorial*i
		end do
	end function
end subroutine 

!> @brief (JohRekkedal:) This subroutine redefines the multipole moments.
!> It is a copy of the FMM code routine mm_(?)_renormalise_qlm
!> that has been modified slightly.
!> @author ?
!> @author Johannes Rekkedal
subroutine pbc_mat_redefine_q(qlm, lmax, nbast)
  integer, intent(in) :: lmax, nbast
  TYPE(matrix), intent(inout) :: qlm((1+lmax)**2)

  integer :: l,m,p,pp,ab
  real(realk) :: pref, s

  ! prefactor to symmetrize T-matrix
  do l = 0, lmax
     if (mod(l,2) .eq. 1) then
        s = -1.0_realk
     else
        s = 1.0_realk
     end if
     pp = l*(l+1) +1
     do m = -l, -1
        pref = -1.0_realk / (sqrt(2.0_realk * factorial(l-m)*factorial(l+m)))
        p = pp+m
        do ab=1,nbast*nbast
           qlm(p)%elms(ab) = pref*qlm(p)%elms(ab) * s
        enddo
     end do
     pref = 1.0_realk / factorial(l)
     p = pp  ! m = 0
     do ab=1,nbast*nbast
        qlm(p)%elms(ab) = pref*qlm(p)%elms(ab) * s
     enddo
     do m = 1, l
        pref = ((-1)**m) / sqrt(2.0_realk * factorial(l-m)*factorial(l+m))
        p = pp+M
        do ab=1,nbast*nbast
           qlm(p)%elms(ab) = pref*qlm(p)%elms(ab) * s
        enddo
     end do
  end do

contains

  real(realk) function factorial(n)
    implicit none
    integer, intent(in) :: n
    integer :: i
    factorial = 1
    do i = n, 2, -1
       factorial = factorial*i
    end do
  end function factorial

end subroutine


!FMM related.
!> @brief 
!> author ?
subroutine pbc_multipl_moment_order(cell_mulmom, lmax)
	integer,intent(in) :: lmax
	real(realk),intent(inout) :: cell_mulmom((lmax+1)**2)
	!local variables
	integer :: je, m, khe, khe0, jmindex
	real(realk) :: tmp_mulmom((lmax+1)**2)
	real(realk) :: sphint

	do je=1, (lmax+1)**2
		tmp_mulmom(je)=cell_mulmom(je)
		cell_mulmom(je)=0.0_realk
	enddo
	khe0=0
	jmindex=1
	do je=0,lmax
		do m=-je,je
			! columns are ordered as 0, 1, -1, 2, -2, etc.
			if(m.gt.0) then
				khe = khe0 + 2*m
			else
				khe = khe0 - 2*m + 1
			end if
			! the nuclear charges must already be included in the
			! moments, so here we just add up.
			sphint = tmp_mulmom(khe)
			! add to the cell's moments
			cell_mulmom(jmindex) = sphint
			jmindex = jmindex + 1
		end do
		khe0 = khe0 + ( 2*je + 1 ) 
	end do
end subroutine

!FMM related.
!> @brief 
!> author ?
subroutine pbc_multipl_moment_matorder(sphnucmom,lmax,nbast)
	implicit none
	integer,intent(in) :: lmax,nbast
	!local variables
	integer :: je,m,khe,khe0,jmindex,ab
	type(matrix),intent(inout) :: sphnucmom((1+lmax)**2)
	real(realk) :: tmp_mulmom(nbast*nbast,(lmax+1)**2)
	real(realk) :: sphint


	do je=1, (lmax+1)**2
		tmp_mulmom(:,je)= sphnucmom(je)%elms(:)
		call mat_zero(sphnucmom(je))
	enddo
	khe0=0
	jmindex=1
	do je=0,lmax
		do m=-je,je
			! columns are ordered as 0, 1, -1, 2, -2, etc.
			if(m.gt.0) then
				khe = khe0 + 2*m
			else
				khe = khe0 -2*m+1
			end if
			! the nuclear charges must already be included in the
			! moments, so here we just add up.
			do ab=1,nbast*nbast
				sphint = tmp_mulmom(ab,khe)
				! add to the cell's moments
				sphnucmom(jmindex)%elms(ab)= sphint
			enddo
			jmindex = jmindex + 1
		end do
		khe0 = khe0 + ( 2*je+1 ) 
	end do

end subroutine pbc_multipl_moment_matorder

!FMM related.
!> @brief 
!> author ?
subroutine calc_pcharge_mom(cmm,q,pos,lmax,reset_flag,ncarmom,lupri)
	integer,intent(in) :: lupri, ncarmom
	real(realk),intent(in) :: q, pos(3)
	real(realk),intent(inout) :: cmm(ncarmom)
	integer,intent(in) :: lmax

	integer, parameter :: max_qlmax_pbc=21
	logical :: reset_flag
	real(realk) :: pos_pow(3,max_qlmax_pbc+1)
	integer :: i,j,k,ltot,index_ijk,icomp

	write(LUPRI,'(A,F10.4,A,3F10.4,A)') 'calc_pcharge_mom: q = ',q, &
		& ' at pos = (',pos(1),pos(2),pos(3),')'

	if (lmax .lt. 0) then
		call lsquit('mod:wannier_main, sr:calc_pcharge_mom: negative lmax.', &
			& lupri)
	else if (lmax .gt. max_qlmax_pbc) then
		call lsquit('mod:wannier_main, sr:calc_pcharge_mom: lmax too large.', &
			& lupri)
	end if

	! Compute x^n, y^n, z^n for n = 0,...,lmax
	pos_pow(1,1) = 1.0_realk
	pos_pow(2,1) = 1.0_realk
	pos_pow(3,1) = 1.0_realk
	do i = 1,lmax
		do icomp = 1,3
			pos_pow(icomp,i+1) = pos_pow(icomp,i) * pos(icomp)
		end do
	end do

	! The components are generated in the order (0 0 0), (1 0 0),
	! (0 1 0), (0 0 1), (2 0 0), (1 1 0), (1 0 1), (0 2 0), ...
	! Loop over Cartesian total "ang. mom."
	index_ijk = 1
	do ltot = 0, lmax, 1
		do i = ltot, 0, -1
			do j = ltot - i, 0, -1
				k = ltot - i - j
				if (reset_flag) then
					cmm(index_ijk) = q * pos_pow(1,i+1) & 
						& * pos_pow(2,j+1) * pos_pow(3,k+1)
				else
					cmm(index_ijk) = cmm(index_ijk) + q * pos_pow(1,i+1) &
						& * pos_pow(2,j+1) * pos_pow(3,k+1)
				end if
				index_ijk = index_ijk + 1
			end do
		end do
	end do
end subroutine 

!> @brief Do the multipole expansion for the reference cell and write the 
!> result to file. (Omega_ol(S)).
!> @author Karl Leikanger
!> @date 2015
!> @param 
!> @param 
!> @param 
!> @param 
!> @param 
!> @param maxmom Highest order of the multipole expansion.
!> @param 
!> @param
function wfmm_multipole_expansion(blat, lsset, maxgab, nbast, refcell, &
		& 	maxmultmom, timings, screening_thr, lupri, luerr) result(sphmom)
	integer,intent(in) :: lupri, luerr, nbast, maxmultmom
	type(lssetting), intent(inout) :: lsset 
	type(moleculeinfo), intent(in) :: refcell
	type(linteger), intent(in) :: maxgab
	type(bravais_lattice), intent(in):: blat
	type(timings_type), intent(inout) :: timings
	type(lmatrixarrayp) :: sphmom
	type(matrix), pointer :: sphmom_ptr(:)
	real(realk), intent(in) :: screening_thr

	integer :: i, j, indx, nsphmom  ! , n, d
	type(lattice_domains_type) :: dom
	type(moleculeinfo) :: moltmp
	integer, pointer :: maxgab_p

	integer :: realthr

	write (*, *) 'Calculating FMM spherical moments ...'
	call timings_start(timings, 'multipole moments')

	realthr = int(log(screening_thr))

	nsphmom = (maxmultmom + 1)**2

	call ltdom_init(dom, blat%dims, maxval(maxgab%cutoffs))

	! use maxgab%cutoffs as cutoff. Note, we only calculate sphmom for cells
	! where gab is small.
	call lts_init(sphmom, blat%dims, maxgab%cutoffs, &
		& nsphmom, nbast, nbast, .false., & 
		& write2disk=.true., fname='sphmom')
	!call set_lstime_print(.false.)
	! iterate over all cells where maxgab >= realthr. Is this a good cutoffthr?
	! TODO see expr. for omega. Is the maximal overlap more relevant to use??
!	n = 0
	call ltdom_getdomain(dom, maxgab%cutoffs, incl_redundant=.false.)
!	do i = 1, dom%nelms; indx = dom%indcs(i)
!		if (lts_get(maxgab, indx) >= realthr) n = n + 1
!	enddo
!	d = 0
	
	call copy_molecule(refcell, moltmp, lupri)
	do i = 1, dom%nelms; indx = dom%indcs(i)
		if (lts_get(maxgab, indx) < realthr) cycle
		call lts_mat_init(sphmom, indx)

		!call wtranslate_molecule(moltmp, wu_indxtolatvec(blat, indx))
    	do j = 1, moltmp%natoms
			moltmp%atom(j)%center(:) = &
            & refcell%atom(j)%center(:) + wu_indxtolatvec(blat, indx)
    	enddo
		call typedef_setmolecules(lsset, refcell, 1, moltmp, 2)
  
		sphmom_ptr => lts_get(sphmom, indx)
		call ii_get_sphmom(lupri, luerr, lsset, sphmom_ptr, nsphmom, &
		  & maxmultmom, 0.0_realk, 0.0_realk, 0.0_realk) !x, y, z
!	  	d = d + 1
!	  	if (mod(i, n/10) == 0) then
!			write (*, "(i3, A13)") int(100.0_realk * d/n), '% complete'
!		endif
	enddo
	call free_moleculeinfo(moltmp)
	call lts_store(sphmom)
	call timings_stop(timings, 'multipole moments')
 call ltdom_free(dom)

#ifdef RUN_WANNIER_TESTS
	call timings_start(timings, 'multipole moments test')
	call wtest_lmatrixarray_storage(sphmom)
	call timings_stop(timings, 'multipole moments test')
#endif
!	call wtest_print_lmatarray_stored(sphmom)

	write (lupri, *) ' ----- FMM spherical moments - matrix calculated ----- ' 
	write (lupri, *) 'Max. multipole moment =', maxmultmom
	write (lupri, *) 'Max log(sqrt(G_ab^0_cd^l)) cutoff thresh. =', realthr

	!call set_lstime_print(.true.)
end function 

#if 1
!> @brief Does the iterations in Eq. 24 J.Chem. Phys., Vol. 121 No. 7 (2004)
!> @author Johannes Rekkedal
!> @author Karl Leikanger
!> @date 2015
function wfmm_form_tlattice(num_its, lmax, square_intermed, blat, &
		& nfcutoffs, timings, lupri) result(tlat)

  integer,intent(in) :: lupri, num_its, lmax, nfcutoffs(3)
  real(realk), pointer :: tlat(:, :)
  type(bravais_lattice), intent(in) :: blat
  type(timings_type), intent(inout) :: timings
  logical, intent(in) :: square_intermed

  integer :: siz, i, row, col, l, qmin, qmax
  integer :: nfsiz, nfsiz1, nfsiz2, nfsiz3
  real(realk) :: layers_in_T
  real(realk), pointer :: W_NF(:,:), T_supNF(:,:), Tlat_aux(:,:)

  integer, parameter ::  debug_siz = 36
  integer, parameter :: debug_lmax = 5
  integer :: debug_nf
  real(realk) :: Tdebug(debug_siz,debug_siz), Tdebug2(debug_siz,debug_siz)
#ifdef DEBUGPBC
  real(realk) :: normsq
  INTEGER :: j
#endif
  
	write(*, *) 'Generating the FMM interaction matrix Tlat ...'
	call timings_start(timings, 'FMM Tlat')

	siz = (1 + lmax)**2
  	call mem_alloc(tlat, siz, siz)
	call mem_alloc(W_NF, siz, siz)
	call mem_alloc(T_supNF, siz, siz)
	call mem_alloc(Tlat_aux, siz, siz)

	if ((num_its < 0) .or. (num_its > 25)) then
		call lsquit('wfmm_form_Wlattice: Unreasonable number of iterations &
			& requested.', lupri)
	endif
	if (any(nfcutoffs /= nfcutoffs)) then !TODO necc??
		call lsquit('pbc_form_Tlattice: All NF sizes must be equal.', lupri)
	endif
	nfsiz = maxval(nfcutoffs) ! TODO contradicting the one above which says that all are equal
  ! get tensors representing the geometrical structure of
  ! the near-field and the near-field of the first supercell.
  call wnf_matrix(w_nf, lmax, siz, blat, lupri)
  call pbc_TsupNF_matrix(T_supNF,nfsiz,lmax,siz,square_intermed,blat,lupri)
#ifdef DEBUGPBC
	normsq = 0
	do i =1,siz
		do j=1,siz
			normsq=normsq+W_nf(i,j)**2
		enddo
	enddo
	write(*,*) 'Norm for WNF matrix = ', normsq
	write(lupri,*) 'Norm for WNF matrix = ', normsq
#endif
#ifdef DEBUGPBC
	normsq = 0
	do i =1,siz
		do j=1,siz
			normsq=normsq+T_supNF(i,j)**2
		enddo
	enddo
	write(*,*) 'Norm for T_supNF matrix = ', normsq
	write(lupri,*) 'Norm for WNF matrix = ', normsq
#endif

  Tlat_aux(:, :) = T_supNF(:, :)
  layers_in_T = 3.0_realk * nfsiz + 1.0_realk
  do i = 1, num_its
     write(LUPRI,*) 'pbc_form_Tlattice(): iteration ',i
     if (mod(i, 2) .eq. 1) then
        call pbc_iterate_Tlattice( &
			  & Tlat, Tlat_aux, W_NF, T_supNF, lmax, siz, square_intermed, lupri)
     else
        call pbc_iterate_Tlattice( &
			  & Tlat_aux, Tlat, W_NF, T_supNF, lmax, siz, square_intermed, lupri)
     end if
     ! count the number of cell layers included in the T tensor
     layers_in_T = 3.0_realk * layers_in_T + 1.0_realk
     ! some debug code
#ifdef DEBUGPBC
	if (i .eq. 1) then
		debug_nf = 3*nfsiz + 1
		call pbc_TsupNF_matrix( &
			& Tdebug, debug_nf, debug_lmax, debug_siz, &
			& square_intermed, blat, lupri)
		Tdebug(:, :) = Tdebug(:, :) + T_supNF(1:debug_siz, 1:debug_siz)
	else if (i .eq. 2) then
		debug_nf = 3*(3*nfsiz + 1) + 1
		call pbc_TsupNF_matrix(Tdebug2, debug_nf, debug_lmax, debug_siz, &
			& square_intermed, blat, lupri)
		Tdebug2(:, :) = Tdebug2(:, :) + Tdebug(:, :)
	end if
#endif
	end do
	if (mod(num_its,2) .eq. 0) then
		Tlat = Tlat_aux
	end if

	Tlat(1,1) = 0.0_realk

	! put in data in upper triangle
	if (.not. square_intermed) then
		write(LUPRI,*) 'pbc_form_Tlattice: Restoring some elements in upper &
			& triangle of Tlattice.'
		do l = 0, lmax
			qmin = l*(l+1) - l + 1
			qmax = l*(l+1) + l + 1
			do row = qmin, qmax
				do col = row + 1, qmax
					Tlat(row, col) = Tlat(col, row)
				end do
			end do
		end do
	end if

#ifdef DEBUGPBC
  normsq = 0
  do i =1,siz
   do j=1,siz
    normsq=normsq+Tlat(i,j)**2
   enddo
  enddo
  write(*,*) 'Norm for Tlat matrix = ', normsq
  write(lupri,*) 'Norm for Tlat matrix = ', normsq
#endif

  call pbc_T_enforce_invsym(Tlat,lmax,siz,lupri)

#ifdef DEBUGPBC
  normsq = 0
  do i =1,siz
   do j=1,siz
    normsq=normsq+Tlat(i,j)**2
   enddo
  enddo
  write(*,*) 'Norm for Tlat matrix invsym = ', normsq
  write(lupri,*) 'Norm for Tlat matrix invsym = ', normsq
#endif

!  call pbc_matlab_print(Tlat,256,256,'Tlattice before trans',lupri)

  call pbc_restore_T_matrix(lmax,Tlat)

#ifdef DEBUGPBC
  normsq = 0
  do i =1,siz
   do j=1,siz
    normsq=normsq+Tlat(i,j)**2
   enddo
  enddo
  write(*,*) 'Norm for Tlat matrix restore = ', normsq
  write(lupri,*) 'Norm for Tlat matrix invsym = ', normsq
#endif

  call timings_stop(timings, 'FMM Tlat')
  call mem_dealloc(W_NF)
  call mem_dealloc(T_supNF)
  call mem_dealloc(Tlat_aux)

end function

! TODO rewrite with domains!!

! This subroutine calculates the M2M translation operator T_NF
! that translates the moments of the central cell to cells in
! the near-field. The central cell is included.
!
! Note that the argument siz is there for convenience and must
! always have the value (1+lmax)**2
subroutine wnf_matrix(w_nf, lmax, siz, blat, lupri)
  integer, intent(in) :: siz, lmax, lupri
  real(realk), intent(inout) :: w_nf((1+lmax)**2,(1+lmax)**2)
  type(bravais_lattice),intent(in) :: blat

  integer :: mx, my, mz, p, m_max(3)
  real(realk) :: pos_std(3)
  real(realk), pointer :: w_aux(:,:)

  if (siz .ne. (1+lmax)**2) call lsquit('wnf_matrix: inconsistent sizes.' &
	  &, lupri)

  call mem_alloc(w_aux, siz, siz)
  w_nf(:, :) = 0.0_realk
  ! if the periodicity is turned off in some directions we need
  ! to constrain the loops accordingly
  m_max(:) = 1
  if (blat%dims <= 2) m_max(3) = 0
  if (blat%dims == 1) m_max(2) = 0
  ! loop over first super cell (it is the near-field when nfsiz = 1)
  do mx = - m_max(1), m_max(1)
	  do my = - m_max(2), m_max(2)
		  do mz = - m_max(3), m_max(3)
			  if (abs(mx) + abs(my) + abs(mz) .ne. 0) then
				  pos_std = wu_coortolatvec(blat, - [mx, my, mz])
				  w_aux(:,:) = 0.0_realk
				  call pbc_get_ltsqr_w_matrix(lmax, pos_std, w_aux)
				  w_nf = w_nf + w_aux
			  else
				  ! the fmm code routines that we call are picky and will
				  ! not accept the task of constructing a translation matrix
				  ! for a zero vector. fortunately, this is trivial so we add
				  ! the contribution directly here.
				  do p = 1,siz
					  w_nf(p,p) = w_nf(p,p) + 1.0_realk
				  end do
			  end if
		  end do
	  end do
  end do

  call mem_dealloc(w_aux)
end subroutine 

! TODO rewrite with domains!!

! Regard the NF of the central cell as a supercell. This subroutine
! calculates the M2L translation operator T_supNF that
! transforms the moments of the central cell into local moments of
! the near-field of this supercell.
subroutine pbc_tsupnf_matrix(t_supnf, nfsiz, lmax, siz, square_flag, blat, lupri)
  integer, intent(in) :: nfsiz, lmax, siz, lupri
  real(realk), intent(inout) :: t_supnf(siz, siz)
  type(bravais_lattice), intent(in) :: blat
  logical, intent(in) :: square_flag

  integer :: m_min, m_max, mx, my, mz, layer, fac(3)
  real(realk) :: pos_std(3), pos_lat(3)
  real(realk), pointer :: T_aux(:, :)

  if (siz .ne. (1+lmax)**2) then
	  call lsquit('pbc_tsupnf_matrix: inconsistent sizes.',lupri)
  endif
  if (nfsiz .lt. 1 .or. nfsiz .gt. 22) then
     call lsquit('pbc_tsupnf_matrix: huge nf size. bailing out.',lupri)
  end if

  call mem_alloc(T_aux, siz, siz)
  T_supNF(:,:) = 0.0_realk
  ! if the periodicity is turned off in some directions we need
  ! to constrain the loops accordingly
  fac = 1
  if (blat%dims <= 2) fac(3) = 0
  if (blat%dims == 1) fac(2) = 0
  ! loop over near-field of super-cell, but accept
  ! only points outside the near-field of the central cell
  m_min = nfsiz + 1
  m_max = 3 * nfsiz + 1
  do layer = m_max, m_min, -1
	  do mx = -layer*fac(1), layer*fac(1)
		  do my = -layer*fac(2), layer*fac(2)
			  do mz = -layer*fac(3), layer*fac(3)
				  if ((abs(mx) .eq. layer) &
					  .or. (abs(my) .eq. layer) &
					  .or. (abs(mz) .eq. layer)) then
					  pos_std = wu_coortolatvec(blat, [mx, my, mz])
					  T_aux(:, :) = 0.0_realk
					  call pbc_get_FLTSQ_T_matrix(lmax,pos_std,T_aux)
					  T_supNF = T_supNF + T_aux
				  end if
			  end do
		  end do
	  end do
  end do

  if (square_flag) then
     ! The FMM code gives a matrix T_{lm,jk} which is lower-triangular
     ! or, more precisely, T_{lm,jk} = 0 for j > l. Here we restore the
     ! full square matrix.
     call pbc_restore_T_matrix(lmax, T_supNF)
  end if

  call mem_dealloc(t_aux)
end subroutine pbc_TsupNF_matrix

! This subroutine does one iteration according to Eq. (24) in
! Kudin & Scuseria, J Chem Phys 121(7):2886-2890.
! That is, it computes
!
!    Wlat = m2l(scale(Wlat_old), T_NF) + W_supNF
!
subroutine pbc_iterate_Tlattice(Tlat, Tlat_old, W_NF, T_supNF, lmax, siz, &
		& square_flag, lupri)
	real(realk), parameter :: stmp = 10.0_realk
	integer, intent(in) :: lmax, siz,lupri
	real(realk), intent(in) :: W_NF(siz,siz), T_supNF(siz,siz)
	real(realk), intent(inout) :: Tlat_old(siz,siz)
	real(realk), intent(out) :: Tlat(siz,siz)
	logical, intent(in) :: square_flag

	real(realk) :: scale_factor

	if (siz .ne. (1+lmax)**2) call lsquit('pbc_iterate_Tlattice: Inconsistent sizes.',lupri)
	scale_factor = 3.0_realk

	call pbc_scale_T(Tlat_old,lmax,siz,scale_factor,lupri)
	call pbc_do_m2l(Tlat,Tlat_old,W_NF,lmax,siz,square_flag,lupri)
	Tlat = Tlat + T_supNF
end subroutine 

! This subroutine scales an interaction matrix so that the translations
! it represents become larger by a factor s.
subroutine pbc_scale_T(Tmatrix,lmax,siz,s,lupri)
  integer, intent(in) :: lmax, siz,lupri
  real(realk), intent(in) :: s
  real(realk), intent(inout) :: Tmatrix(siz,siz)

  integer :: l, mu, j, kappa, row, col, npow
  
  if (siz .ne. (1+lmax)**2) call lsquit('pbc_scale_T: Inconsistent sizes.',lupri)
  
  row = 0
  do l = 0, lmax
     do mu = -l, l
        ! update row index
        row = row + 1
        ! reset column index
        col = 0
        do j = 0, lmax
           do kappa = -j, j
              ! update column index
              col = col + 1
              ! which power of the scaling factor?
              npow = l + j + 1
              ! scale!
              Tmatrix(row,col) = Tmatrix(row,col) / s**npow
           end do
        end do
     end do
  end do
end subroutine pbc_scale_T

! This subroutine performs the M2L operation of T1 with W2 and
! stores the result in Tres.
subroutine pbc_do_m2l(Tres,T1,W2,lmax,siz,square_flag,lupri)
	logical :: square_flag
	integer, intent(in) :: lmax, siz,lupri
	real(realk), intent(in) :: T1(siz,siz), W2(siz,siz)
	real(realk), intent(out) :: Tres(siz,siz)

	real(realk) :: fac
	integer :: col, row
	integer :: l,m,j,k,p,q,kmax,qmin,lm,jk,pq

	if (siz .ne. (1+lmax)**2) call lsquit('pbc_do_m2l: Inconsistent sizes.',lupri)

	Tres(:,:) = 0.0_realk

	! compute M2L result
	if (square_flag) then
		do col = 1, siz
			do row = 1, siz
				Tres(row,col) = dot_product(T1(row,col:siz),W2(col:siz,col))
			end do
		end do
		return
	end if


	rows_l: do l = 0,lmax
		rows_m: do m = -l,l
			lm = l*(l+1)+1+m
			cols_j: do j = 0,l
				if (j .eq. l) then
					kmax = m
				else
					kmax = j
				end if
				cols_k: do k = -j, kmax
					jk = j*(j+1)+1+k
					! loop over pq >= jk
					sum_p: do p = j, lmax
						if (p .eq. j) then
							qmin = k
						else
							qmin = -p
						end if
						sum_q: do q = qmin,p
							pq = p*(p+1)+1+q
							! determine factor
							fac = 1.0_realk
							if (q .eq. 0) fac = fac / 2.0_realk
							!if (mod(p,2_long) .eq. 1) fac = -fac
							! add contribution
							Tres(lm,jk) = Tres(lm,jk) + T1(max(lm,pq),min(lm,pq)) &
								& * W2(pq,jk) * fac
						end do sum_q
					end do sum_p
					! put in the factor
					fac = 1.0_realk
					if (k .eq. 0) fac = fac / 2.0_realk
					!if (mod(j,2_long) .eq. 1) fac = -fac              
					Tres(lm,jk) = Tres(lm,jk) / fac
				end do cols_k
			end do cols_j
		end do rows_m
	end do rows_l
end subroutine pbc_do_m2l

! Even if the content of a cell lacks any symmetry the lattice
! itself will still have inversion symmetry. Therefore Tlattice
! must also have inversion symmetry. This subroutine resets elements
! in a T tensor that should be zero due inversion symmetry.
subroutine pbc_T_enforce_invsym(T,lmax,siz,lupri)

  integer, intent(in) :: lmax, siz
  real(realk), intent(inout) :: T(siz,siz)
  integer,intent(in) :: lupri

  integer :: l,j,pmin,pmax,qmin,qmax

  if (siz .ne. (1+lmax)**2) call lsquit('pbc_T_enforce_invsym: Inconsistent sizes.',lupri)

  write(LUPRI,*) 'pbc_T_enforce_invsym: Enforcing inversion symmetry in a T tensor.'

  do l = 0, lmax
     pmin = l*(l+1) - l + 1
     pmax = l*(l+1) + l + 1
     do j = 0, lmax
        if (mod(l+j,2) .eq. 1) then
           qmin = j*(j+1) - j + 1
           qmax = j*(j+1) + j + 1

           T(pmin:pmax,qmin:qmax) = 0.0_realk
        end if
     end do
  end do
end subroutine pbc_T_enforce_invsym

#endif

end module
