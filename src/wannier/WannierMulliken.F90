module Wannier_Mulliken


  use precision
  use wannier_utils
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use molecule_type, only: &
       init_moleculeinfo, &
       copy_molecule, &
       free_moleculeinfo
  use typedef
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage, only: &
       lmatrixp, &
       llmatrixp, &
       linteger, &
       lmatrixarrayp, &
       lts_mat_init, &
       lts_init, &
       lts_free, &
       lts_get, &
       lts_axpy, &
       lts_store, &
       lts_load, &
       lts_zero, &
       lts_print, &
       lts_copy
  use matrix_module, only: &
       matrix
  use timings_module
  use memory_handling, only: &
       mem_alloc, mem_dealloc
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_dotproduct, &
       mat_abs_max_elm, &
       mat_trans, &
       mat_print
  use integralinterfacemod, only: &
       II_get_nucel_mat

  implicit none
  private
  public :: w_calc_mulliken, w_calc_mulliken_pot, w_calc_mulliken_charge

contains

  subroutine w_calc_mulliken_pot(fockmat, dens, smat, wconf, lsset, refcell,  lupri, luerr)  
    type(lmatrixp), intent(in)       :: fockmat, dens, smat
    type(moleculeinfo),intent(in)    :: refcell
    type(wannier_config), intent(in) :: wconf
    type(lssetting), intent(inout)   :: lsset
    integer,intent(in)               ::  lupri, luerr

    type(lmatrixp)             :: zlrmat
    type(lattice_domains_type) :: dom, dom_ff
    integer                    :: nbas, natoms, fock_cutoff(3), nf_cutoff(3), ff_cutoff(3)
    integer                    :: i, l, indxl, m, indxm, layer
    type(moleculeinfo)         :: molL, molM
    real(realk),pointer        :: mulliken_charge(:)
    type(matrix),pointer       :: zlrmat_0L, F_0L
    type(matrix)               :: zlrmat_tmp, h1
    real(realk)                :: maxelm
    logical                    :: next
    
    natoms = refcell%natoms
    nbas = dens%nrow
    fock_cutoff(:) = fockmat%cutoffs(:)
    nf_cutoff(:) = wconf%nf_cutoff(:)
    ff_cutoff(:) = 100*wconf%nf_cutoff(:)
    
    !Initialisation of the Z matrix with the Fock matrix cutoffs
    !Redundancy = .false. so that only the block Z_0,L with L>=0 are computed
    call lts_init(zlrmat, wconf%blat%dims, fock_cutoff, nbas, nbas, wconf%no_redundant) 
        
    !Initialisation of the domain
    call ltdom_init(dom, wconf%blat%dims, maxval(fock_cutoff))
    call ltdom_getdomain(dom, fock_cutoff, incl_redundant = wconf%no_redundant)

    !Copy reference cell for integral call
    call copy_molecule(refcell, molL, lupri)
    call copy_molecule(refcell, molM, lupri)

    !Compute the Mulliken charges and assign them to the corresponding atoms in far-field cells
    call mem_alloc(mulliken_charge,natoms)
    call w_calc_mulliken_charge(mulliken_charge,dens,smat,refcell,wconf,lupri,luerr)
    do i=1,natoms
       molM%atom(i)%charge = mulliken_charge(i)
    enddo
    call mem_dealloc(mulliken_charge)

    !Initialisation of tmp matrices
    call mat_init(zlrmat_tmp, nbas, nbas)
    call mat_init(h1, nbas, nbas)
    
    loopl: do l = 1, dom%nelms; indxL = dom%indcs(l)
       F_0L => lts_get(fockmat,indxL)
       ! get Zmat of cell indxL
       call lts_mat_init(zlrmat, indxL)
       zlrmat_0L => lts_get(zlrmat, indxL)
       call mat_zero(zlrmat_0L)
       !Translate cell L
       call wcopy_atompositions(molL, refcell)
       call wtranslate_molecule(molL, wu_indxtolatvec(wconf%blat, indxL))

       call mat_zero(zlrmat_tmp)

       !Loop over cells in the farfield region layer by layer
       layer = maxval(nf_cutoff)
       next =.true.
       !loop over layers
       do while (next .and. (layer < maxval(ff_cutoff)))
          layer = layer+1
          call ltdom_init(dom_ff, wconf%blat%dims, maxval(ff_cutoff))
          call ltdom_getlayer(dom_ff, layer, .true.)
          loopm: do m = 1, dom_ff%nelms; indxM = dom_ff%indcs(m)
             write(*,*) 'M',indxM
             call wcopy_atompositions(molM, refcell)
             call wtranslate_molecule(molM, wu_indxtolatvec(wconf%blat, indxM))
             call typedef_setmolecules(lsset, refcell, 1, molL, 2, molM, 3)
             call II_get_nucel_mat(lupri, luerr, lsset, H1)
             call mat_daxpy(1._realk, H1, zlrmat_tmp)
          enddo loopm
          call ltdom_free(dom_ff)
          call mat_abs_max_elm(zlrmat_tmp,maxelm)
          if (maxelm < 1e-8_realk) then
             next = .false.
             write( *,*) layer
          endif
          call mat_daxpy(1._realk, zlrmat_tmp, zlrmat_0L)
          call mat_daxpy(1._realk, zlrmat_tmp, F_0L)
       enddo
    enddo loopl

    if (wconf%debug_level >= 6) then
       write(luerr,*) ''
       write(luerr,*) '***Fock - Farfield Mulliken' 
       call lts_print(zlrmat,luerr,1)
    endif

    call mat_free(zlrmat_tmp)
    call mat_free(h1)

    call lts_free(zlrmat)
    
    !Free domains
    call ltdom_free(dom)
    
    !Free moleculeinfo
    call free_moleculeinfo(molL)
    call free_moleculeinfo(molM)

    
  end subroutine  w_calc_mulliken_pot

  
  function w_calc_mulliken(dens,smat,refcell,wconf,lsset,lupri,luerr) result(elr)
    
    type(lmatrixp), intent(in)       :: dens, smat
    type(moleculeinfo),intent(in)    :: refcell
    type(wannier_config), intent(in) :: wconf
    type(lssetting), intent(inout)   :: lsset
    integer,intent(in)               :: lupri, luerr
    real(realk)                      :: elr
    
    real(realk),pointer        :: mulliken_charge(:)
    integer                    :: nbas, natoms, layer, l, indx_l
    integer                    :: nf_cutoff(3), ff_cutoff(3)
    type(lattice_domains_type) :: dom
    type(moleculeinfo)         :: moltmp
    integer                    :: i,j
    real(realk)                :: pq(3),distance,elayer
    logical                    :: next

    nbas = dens%nrow
    natoms = refcell%natoms

    nf_cutoff(:) = wconf%nf_cutoff(:)
    ff_cutoff(:) = 100*wconf%nf_cutoff(:)
    
    call mem_alloc(mulliken_charge,natoms)
    call w_calc_mulliken_charge(mulliken_charge,dens,smat,refcell,wconf,lupri,luerr)

    call copy_molecule(refcell, moltmp, lupri)

    elr = 0.0_realk
    layer = maxval(nf_cutoff)
    next =.true.
    !loop over layers
    do while (next .and. (layer < maxval(ff_cutoff)))
       layer = layer+1
       elayer = 0.0_realk
       call ltdom_init(dom, wconf%blat%dims, maxval(ff_cutoff))
       call ltdom_getlayer(dom, layer, .true.)
       !loop over cells in layer
       do l = 1,dom%nelms
          indx_l = dom%indcs(l)
          call wcopy_atompositions(moltmp, refcell)
          call wtranslate_molecule(moltmp, wu_indxtolatvec(wconf%blat, indx_l))
          call typedef_setmolecules(lsset, refcell, 1, moltmp, 3)
          !loop over atoms in refcell
          do i=1,natoms
             !loop over atoms in cell 
             do j=1,natoms
                pq(1:3) = refcell%atom(i)%center(1:3) - moltmp%atom(j)%center(1:3)
                distance = sqrt(pq(1)*pq(1) + pq(2)*pq(2) + pq(3)*pq(3))
                elayer = elayer + mulliken_charge(i)*mulliken_charge(j) / distance
             enddo
          enddo
       enddo
       !write (*,*) layer, elayer
       if (abs(elayer) < 1.0e-8_realk) next =.false.
       elr = elr+elayer
       call ltdom_free(dom)
    enddo

    write (lupri,*) 'Mulliken correction',elr,layer
    call free_moleculeinfo(moltmp)
    call mem_dealloc(mulliken_charge)
    
  end function w_calc_mulliken
  
  subroutine w_calc_mulliken_charge(mulliken_charge,dens,smat,refcell,wconf,lupri,luerr)

    real(realk),pointer,intent(out)  :: mulliken_charge(:)
    type(lmatrixp), intent(in)       :: dens, smat
    type(moleculeinfo),intent(in)    :: refcell
    type(wannier_config), intent(in) :: wconf
    integer,intent(in)               :: lupri, luerr

    real(realk),pointer :: gop(:)
    integer             :: nbas,i,j,natoms,iStartReg,iEndReg
    real(realk)         :: sm, thr 

    thr = 1e-8_realk

    nbas = dens%nrow
    natoms = refcell%natoms
    
    call mem_alloc(gop,nbas)
  
    call w_calc_mulliken_gop(gop,dens,smat,wconf,lupri,luerr)

    iStartReg = 1
    do i = 1,natoms
       mulliken_charge(i) = refcell%atom(i)%charge
       iEndReg = iStartReg + refcell%atom(i)%nContOrbREG - 1
       do j = iStartReg, iEndReg
          mulliken_charge(i) =  mulliken_charge(i) - gop(j)
       enddo
       iStartReg = iEndReg + 1
    enddo

    sm = 0.0e0_realk
    write(lupri,*) 'Mulliken charges'
    do i = 1, natoms
       sm = sm + mulliken_charge(i)
       write(lupri,*) refcell%atom(i)%name,mulliken_charge(i)
    enddo
    write(lupri,*) 'Total: ',sm
    if (wconf%debug_level >= 0) then
       if (abs(sm) > thr) then
          write (*,*) 'Sum of the Mulliken charges', sm
          call lsquit('Mulliken charges do not add up to zero',-1)
       endif
    endif
    
    call mem_dealloc(gop)
    
  end subroutine w_calc_mulliken_charge

  
  !> @brief Computes the Mulliken gross orbital product 
  !> @author E. Rebolini
  !> @date Sept 2016
  
  subroutine w_calc_mulliken_gop(gop,dens,smat,wconf,lupri,luerr)

    real(realk), pointer, intent(inout) :: gop(:)
    type(lmatrixp), intent(in)          :: dens, smat
    type(wannier_config), intent(in)    :: wconf
    integer,intent(in)                  :: lupri, luerr

    type(lattice_domains_type)  :: dom
    type(matrix)                :: D_0L, S_0L, tmp
    integer                     :: nbas, l, indx_l, i, j, ij
    logical                     :: lstat_S, lstat_D
    
    nbas = dens%nrow
    
    !Set up the domain d0
    call ltdom_init(dom, dens%periodicdims, maxval(dens%cutoffs))
    call ltdom_getdomain(dom, dens%cutoffs, incl_redundant=.true.)

    call mat_init(D_0L, nbas, nbas)
    call mat_init(S_0L, nbas, nbas)
    call mat_init(tmp, nbas, nbas)
    call mat_zero(tmp)
    loopl: do l = 1, dom%nelms
       indx_l = dom%indcs(l)
       call lts_copy(dens,indx_l,lstat_D,D_0L)
       if (lstat_D) then
          call lts_copy(smat,-indx_l,lstat_S,S_0L)
          if (lstat_S) then
             call mat_mul(D_0L,S_0L,'N','N',1.0_realk,1.0_realk,tmp)
          endif
       endif
    enddo loopl

    
    
    do i=1,nbas
       gop(i) = 2*tmp%elms( (i-1)*nbas + i)
    enddo

!!$    if (wconf%debug_level >= 2) then
!!$       write(luerr,*) 'Mulliken population'
!!$       call mat_print(tmp,1,nbas,1,nbas,luerr)
!!$       write(luerr,*) 'GOP'
!!$       write(luerr,*) gop
!!$    endif
    
    !Cleanup
    call mat_free(D_0L)
    call mat_free(S_0L)
    call mat_free(tmp)
    
    call ltdom_free(dom)

  end subroutine w_calc_mulliken_gop

  
end module Wannier_Mulliken
